using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using EventStoreBus.Node.Api;
using EventStoreBus.Node.Api.Representations;
using NuGet;
using NUnit.Framework;
using HttpClient = System.Net.Http.HttpClient;

// ReSharper disable InconsistentNaming
namespace EventStoreBus.Node.Tests
{
    [TestFixture]
    public class RestEndpointTests
    {
        //[Test]
        //public void It_works()
        //{
        //    var remoteRepo = new DataServicePackageRepository(new Uri("http://localhost/NuGetServer/nuget"));
        //    var packageStore = new FileSystemPackageStore(new PhysicalFileSystem("."), remoteRepo);
        //    var workerController = new WorkerController(new Uri("http://localhost:12345"), );
        //    var endpoint = new RestEndpoint("http://pn-spobiega-01:12645", packageStore, workerController);
        //    endpoint.Start();

        //    var client = new HttpClient();
        //    client.BaseAddress = new Uri("http://pn-spobiega-01:12645");
        //    HyperLink undeployLink = null;
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

        //    client.GetAsync("/")
        //        .ContinueWith(x => x.Result.Content.ReadAsAsync<Api.Representations.Node>().Result)
        //        .ContinueWith(x => Deploy(x.Result.Deploy.Url, client, "SomeReceptor"))
        //        .ContinueWith(x =>
        //                          {
        //                              undeployLink = x.Result.Undeploy;
        //                              return Start(x.Result.Start.Url, client);
        //                          })
        //        .ContinueWith(x =>
        //                          {
        //                              var stopLink = x.Result.Stop;
        //                              return client.DeleteAsync(stopLink.Url).Result;
        //                          })
        //        .ContinueWith(x => client.DeleteAsync(undeployLink.Url).Result)
        //        .Wait();
        //    endpoint.Stop();
        //}

        [Test]
        public void Run_all()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://pn-spobiega-01:12645");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

            client.GetAsync("/")
                .ContinueWith(x => x.Result.Content.ReadAsAsync<Api.Representations.Node>().Result)
                .ContinueWith(x => Deploy(x.Result.Deploy.Url, client, "SomeReceptor"))
                .ContinueWith(x => Start(x.Result.Start.Url, client))
                .Wait();

            client.GetAsync("/")
                .ContinueWith(x => x.Result.Content.ReadAsAsync<Api.Representations.Node>().Result)
                .ContinueWith(x => Deploy(x.Result.Deploy.Url, client, "SomeComponent"))
                .ContinueWith(x => Start(x.Result.Start.Url, client))
                .Wait();

            client.GetAsync("/")
                .ContinueWith(x => x.Result.Content.ReadAsAsync<Api.Representations.Node>().Result)
                .ContinueWith(x => Deploy(x.Result.Deploy.Url, client, "SomeViewGroup"))
                .ContinueWith(x => Start(x.Result.Start.Url, client))
                .Wait();
        }

        private static Instance Start(string startUrl, HttpClient client)
        {
            var instance = client.PutAsync(startUrl, new StringContent(""))
                .Result.Content.ReadAsAsync<Instance>().Result;
            return instance;
        }

        private static Deployment Deploy(string deployUrl, HttpClient client, string component)
        {
            var newDeployment = new NewDeployment
                                    {
                                        Component = component,
                                        Service = "ServiceA",
                                        Version = "1.0.0"
                                    };

            var deployment = client
                .PutAsXmlAsync(deployUrl, newDeployment)
                .Result.Content.ReadAsAsync<Deployment>().Result;
            return deployment;
        }
    }
}
// ReSharper restore InconsistentNaming

﻿using System;
using System.Collections.Generic;
using System.Linq;
using DurableSubscriber.Diagnostics;

namespace DurableSubscriber.Tests
{
    public class FakePullSource : IPullSource<object>
    {
        public int SliceNumber { get; private set; }

        public object NextEvent { get; private set; }

        public IEnumerable<Tuple<object ,object >> ReadSlice()
        {
            if (NextEvent != null)
            {
                SliceNumber++;
                var evnt = NextEvent;
                NextEvent = null;
                if (SliceReturning != null)
                {
                    SliceReturning(this, new EventArgs());
                }
                return new[] { new Tuple<object, object>(evnt, null)  };
            }
            if (NoSlicesLeft != null)
            {
                NoSlicesLeft(this, new EventArgs());
            }
            return Enumerable.Empty<Tuple<object, object>>();
        }

        public void Seek(object position)
        {
        }

        public void Reset()
        {
        }

        public void PrepareSlice(object evnt)
        {
            NextEvent = evnt;
        }

        public event EventHandler NoSlicesLeft;
        public event EventHandler SliceReturning;

        public State State
        {
            get { return null; }
        }
    }
}
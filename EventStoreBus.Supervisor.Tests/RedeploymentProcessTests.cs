﻿using System;
using System.Collections.Generic;
using EventStoreBus.Supervisor.DeploymentProcesses;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands;
using NUnit.Framework;
using PlannedDeployment = EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands.PlannedDeployment;

// ReSharper disable InconsistentNaming
namespace EventStoreBus.Supervisor.Tests
{
    [TestFixture]
    public class RedeploymentProcessTests : TestCase<RedeploymentProcess>
    {
        [Test]
        public void After_starting_it_tries_to_lock_the_service()
        {
            Given("id")
                .When(x => x.Create("svc", null, new FakeUniqueIdGenerator("lockId")))
                .Then(
                    Created(),
                    LockRequested());
        }

        [Test]
        public void After_locking_it_requests_a_deployment_plan()
        {
            Given("id",
                  Created(),
                  LockRequested()
                )
                .When(x => x.HandleLockSuccess())
                .Then(
                    Locked(),
                    DeploymentPlanRequested());
        }

        

        [Test]
        public void After_loading_plan_it_triggers_deployment_of_first_component_according_to_the_plan()
        {
            var deploymentPlan = new DeploymentPlan()
                                     {
                                         ServiceVersion = "1.0.0.0",
                                         ToDeploy = new List<PlannedDeployment>()
                                                        {
                                                            new PlannedDeployment()
                                                                {
                                                                    Component = "A",
                                                                    NodeId = "1"
                                                                },
                                                            new PlannedDeployment()
                                                                {
                                                                    Component = "B",
                                                                    NodeId = "2"
                                                                }
                                                        }
                                     };
            Given("id",
                  Created(),
                  LockRequested(),
                  Locked(),
                  DeploymentPlanRequested()
                )
                .When(x => x.LoadPlan(deploymentPlan))
                .Then(
                    VersionDetermined(),
                    DeploymentPlanned("A", "1"),
                    DeploymentPlanned("B", "2"),
                    DeploymentRequested("A", "1", "opId"));
        }

       

        [Test]
        public void After_successfully_deploying_a_component_it_triggers_its_start()
        {
            Given("id",
                  Created(),
                  LockRequested(),
                  Locked(),
                  VersionDetermined(),
                  DeploymentPlanned("A", "1"),
                  DeploymentPlanned("B", "2"),
                  DeploymentRequested("A", "1", "opId"))
                .When(x => x.HandleDeploySuccess("opId", "deplId"))
                .Then(
                    DeploymentSucceeded(),
                    StartRequested("1", "deplId", "id"));
        }
        
        [Test]
        public void After_successfully_starting_a_component_it_triggers_deployment_of_subsequent_component()
        {
            Given("id",
                  Created(),
                  LockRequested(),
                  Locked(),
                  VersionDetermined(),
                  DeploymentPlanned("A", "1"),
                  DeploymentPlanned("B", "2"),
                  DeploymentRequested("A", "1", "opId"),
                  DeploymentSucceeded(),
                  StartRequested("1", "deplId", "id"))
                .When(x => x.HandleStartSuccess("opId", "deplId"))
                .Then(
                    StartSucceeded(),
                    DeploymentRequested("B", "2", "opId"));
        } 
        
        [Test]
        public void After_successfully_starting_last_deployed_component_it_triggers_stop_of_first_undeployed_component()
        {
            Given("id",
                  Created(),
                  LockRequested(),
                  Locked(),
                  VersionDetermined(),
                  DeploymentPlanned("A", "1"),
                  UndeploymentPlanned("A", "3"),
                  UndeploymentPlanned("B", "4"),
                  DeploymentRequested("A", "1", "opId"),
                  DeploymentSucceeded(),
                  StartRequested("1", "deplId", "id"))
                .When(x => x.HandleStartSuccess("opId", "deplId"))
                .Then(
                    StartSucceeded(),
                    StopRequested("3"));
        }
        
        [Test]
        public void After_successfully_stopping_a_component_it_triggers_its_undeployment()
        {
            Given("id",
                  Created(),
                  LockRequested(),
                  Locked(),
                  VersionDetermined(),
                  DeploymentPlanned("A", "1"),
                  UndeploymentPlanned("A", "3"),
                  UndeploymentPlanned("B", "4"),
                  DeploymentRequested("A", "1", "opId"),
                  DeploymentSucceeded(),
                  StartRequested("1", "deplId", "id"),
                  StartSucceeded(),
                  StopRequested("3"))
                .When(x => x.HandleStopSuccess("opId", "deplId"))
                .Then(
                    StopSucceeded(),
                    UndeploymentRequested("3"));
        }
        
        [Test]
        public void After_successfully_undeploying_a_component_it_triggers_stopping_of_subsequent_component()
        {
            Given("id",
                  Created(),
                  LockRequested(),
                  Locked(),
                  VersionDetermined(),
                  UndeploymentPlanned("A", "3"),
                  UndeploymentPlanned("B", "4"),
                  StopRequested("3"),
                  StopSucceeded(),
                  UndeploymentRequested("3"))
                .When(x => x.HandleUndeploySuccess("opId", "deplId"))
                .Then(
                    UndeploymentSucceeded(),
                    StopRequested("4"));
        }
        
        [Test]
        public void After_successfully_undeploying_last_component_it_completes()
        {
            Given("id",
                  Created(),
                  LockRequested(),
                  Locked(),
                  VersionDetermined(),
                  UndeploymentPlanned("A", "3"),
                  StopRequested("3"),
                  StopSucceeded(),
                  UndeploymentRequested("3"))
                .When(x => x.HandleUndeploySuccess("opId", "deplId"))
                .Then(
                    UndeploymentSucceeded(),
                    UnlockRequested(),
                    Succeeded());
        }

        private static UnlockRequested UnlockRequested()
        {
            return new UnlockRequested()
                       {
                           LockId = "lockId",
                           ProcessId = "id",
                           Service = "svc"
                       };
        }

        private static Succeeded Succeeded()
        {
            return new Succeeded()
                       {
                           ProcessId = "id",
                           Service = "svc"
                       };
        }

        private static UndeploymentSucceeded UndeploymentSucceeded()
        {
            return new UndeploymentSucceeded
                       {
                           DeploymentId = "deplId",
                           OperationId = "opId",
                           ProcessId = "id"
                       };
        }

        private static UndeploymentRequested UndeploymentRequested(string nodeId)
        {
            return new UndeploymentRequested()
                       {
                           DeploymentId = "deplId",
                           NodeId = nodeId,
                           ProcessId = "id"
                       };
        }

        private static StopSucceeded StopSucceeded()
        {
            return new StopSucceeded
                       {
                           DeploymentId = "deplId",
                           OperationId = "opId",
                           ProcessId = "id"
                       };
        }

        private static StopRequested StopRequested(string nodeId)
        {
            return new StopRequested()
                       {
                           DeploymentId = "deplId",
                           NodeId = nodeId,
                           ProcessId = "id"
                       };
        }

        private static UndeploymentPlanned UndeploymentPlanned(string component, string nodeId)
        {
            return new UndeploymentPlanned()
                       {
                           Component = component,
                           NodeId = nodeId,
                           ExistingDeploymentId = "deplId"
                       };
        }

        private static StartSucceeded StartSucceeded()
        {
            return new StartSucceeded
                       {
                           DeploymentId = "deplId",
                           OperationId = "opId",
                           ProcessId = "id"
                       };
        }


        private static Created Created()
        {
            return new Created()
            {
                Service = "svc"
            };
        }

        private static LockRequested LockRequested()
        {
            return new LockRequested()
            {
                LockId = "lockId",
                ProcessId = "id",
                Service = "svc"
            };
        }

        private static Locked Locked()
        {
            return new Locked()
            {
                LockId = "lockId"
            };
        }

        private static DeploymentPlanRequested DeploymentPlanRequested()
        {
            return new DeploymentPlanRequested
            {
                LockId = "lockId",
                ProcessId = "id",
                Service = "svc"
            };
        }

        private static DeploymentPlanned DeploymentPlanned(string component, string nodeId)
        {
            return new DeploymentPlanned()
            {
                Component = component,
                NodeId = nodeId
            };
        }

        private static VersionDetermined VersionDetermined()
        {
            return new VersionDetermined()
            {
                Version = "1.0.0.0"
            };
        }

        private static DeploymentRequested DeploymentRequested(string component, string nodeId, string operationId)
        {
            return new DeploymentRequested()
            {
                ProcessId = "id",
                Service = "svc",
                Component = component,
                NodeId = nodeId,
                Version = "1.0.0.0"
            };
        }

        private static StartRequested StartRequested(string nodeId, string deploymentId, string operationId)
        {
            return new StartRequested()
            {
                DeploymentId = deploymentId,
                NodeId = nodeId,
                ProcessId = "id"
            };
        }

        private static DeploymentSucceeded DeploymentSucceeded()
        {
            return new DeploymentSucceeded()
            {
                DeploymentId = "deplId",
                OperationId = "opId",
                ProcessId = "id"
            };
        }
    }
}
// ReSharper restore InconsistentNaming

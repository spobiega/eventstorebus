﻿using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy;
using NUnit.Framework;

// ReSharper disable InconsistentNaming
namespace EventStoreBus.Supervisor.Tests
{
    public class UndeploymentProcessTests : TestCase<UndeploymentProcess>
    {
        [Test]
        public void When_starting_it_requests_undeployment_of_all_deployments_immediately()
        {
            Given("Self")
                .When(x => x.Start("Parent", new[]
                    {
                        new UndeploymentRequest("N1", "D1"),
                        new UndeploymentRequest("N2", "D2")
                    }))
                .Then(
                    new UndeploymentSequenceStarted("Self","Parent"),
                    new UndeploymentRequested("Self", "D1", "N1", 5),
                    new UndeploymentRequested("Self", "D2", "N2", 5)
                );
        }

        [Test]
        public void When_one_of_many_tasks_succeeds_process_is_not_yet_considered_complete()
        {
            Given("Self",
                  new UndeploymentSequenceStarted("Self", "Parent"),
                  new UndeploymentRequested("Self", "D1", "N1", 5),
                  new UndeploymentRequested("Self", "D2", "N2", 5)
                )
                .When(x => x.HandleUndeploySuccess("D1"))
                .Then(
                    new UndeploymentSucceeded("Self", "D1"));
        }

        [Test]
        public void When_one_of_many_tasks_fails_it_is_retried()
        {
            Given("Self",
                  new UndeploymentSequenceStarted("Self", "Parent"),
                  new UndeploymentRequested("Self", "D1", "N1", 5),
                  new UndeploymentRequested("Self", "D2", "N2", 5)
                )
                .When(x => x.HandleUndeployFailure("D1"))
                .Then(
                    new UndeploymentFailed("Self", "D1"),
                    new UndeploymentRequested("Self", "D1", "N1", 5));
        }

        [Test]
        public void When_all_tasks_complete_with_either_success_or_failure_process_is_considered_complete_with_failures()
        {
            Given("Self", new UndeploymentProcess(2),
                  new UndeploymentSequenceStarted("Self", "Parent"),
                  new UndeploymentRequested("Self", "D1", "N1", 2),
                  new UndeploymentRequested("Self", "D2", "N2", 2),
                  new UndeploymentSucceeded("Self", "D2"),
                  new UndeploymentFailed("Self", "D1"),
                  new UndeploymentRequested("Self", "D1", "N1", 2)
                )
                .When(x => x.HandleUndeployFailure("D1"))
                .Then(
                    new UndeploymentFailed("Self", "D1"),
                    new UndeploymentSequenceCompleted("Self", "Parent", new [] {"D2"}, new [] {"D1"}));
        }

        [Test]
        public void When_all_tasks_complete_successfully_process_is_considered_complete()
        {
            Given("Self",
                  new UndeploymentSequenceStarted("Self", "Parent"),
                  new UndeploymentRequested("Self", "D1", "N1", 5),
                  new UndeploymentRequested("Self", "D2", "N2", 5),
                  new UndeploymentSucceeded("Self", "D2")
                )
                .When(x => x.HandleUndeploySuccess("D1"))
                .Then(
                    new UndeploymentSucceeded("Self", "D1"),
                    new UndeploymentSequenceCompleted("Self", "Parent", new[] { "D1", "D2" }, new string[] { }));
        }
    }
}
// ReSharper restore InconsistentNaming

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Api;
using NUnit.Framework;

namespace EventStoreBus.Supervisor.Tests
{
    public abstract class TestCase<T> 
        where T : IAggregate, new()
    {
        protected IGiven Given(string id, params IEvent[] events)
        {
            var instance = new T();
            return Given(id, instance, events);
        }

        protected IGiven Given(string id, T instance, params IEvent[] events)
        {
            instance.LoadState(events);
            instance.SetId(id);
            return new TestHelper(instance);
        }

        private class TestHelper : IGiven, IWhen
        {
            private readonly T instance;
            private Exception exception;

            public TestHelper(T instance)
            {
                this.instance = instance;
            }

            public IWhen When(Action<T> action)
            {
                try
                {
                    action(instance);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
                return this;
            }

            public void Then(params IEvent[] events)
            {
                Assert.IsNull(exception, "Expected no exception");
                Assert.AreEqual(events.Length, instance.Changes.Count(), "Expected {0} events", events.Length);

                var changes = instance.Changes.ToList();

                for (var i = 0; i < events.Length; i++ )
                {
                    var matchingEvent = i < changes.Count() 
                        ? changes[i]
                        : null;
                    AssertAreEqual(events[i], matchingEvent);
                }
            }

            private static void AssertAreEqual(object expected, object actual)
            {
                if (actual == null)
                {
                    Assert.Fail("Expected\r\n{0}\r\nbut found nothing.", FormatValue(expected));
                }
                if (expected.GetType() != actual.GetType())
                {
                    Assert.Fail("Expected {0}\r\n{1}\r\nbut found {2}\r\n{3}.", expected.GetType().Name, FormatValue(expected), actual.GetType().Name, FormatValue(actual));
                }
                if (!CompareProperties(expected, actual))
                {
                    Assert.Fail("Expected\r\n{0}\r\nbut found\r\n{1}.", FormatValue(expected), FormatValue(actual));
                }
            }
            
            private static string FormatValue(object value)
            {
                if (value == null)
                {
                    return "<null>";
                }
                var payloadType = value.GetType();
                if (payloadType.IsPrimitive || payloadType.IsValueType || payloadType == typeof(string))
                {
                    return value.ToString();
                }
                var enumerable = value as IEnumerable;
                if (enumerable != null)
                {
                    return "[" + string.Join(", ", enumerable.Cast<object>().Select(FormatValue)) + "]";
                }
                var formattedProperties = payloadType.GetProperties().Select(x => FormatProperty(x.Name, x.GetValue(value, new object[0])));
                return payloadType.Name + " {" 
                    + System.Environment.NewLine 
                    + string.Join(System.Environment.NewLine, formattedProperties) 
                    + System.Environment.NewLine 
                    + "}";
            }

            private static string FormatProperty(string name, object value)
            {
                return name + ": " + FormatValue(value);
            }


            private static bool CompareProperties(object expected, object actual)
            {
                var type = expected.GetType();
                foreach (var propertyInfo in type.GetProperties())
                {
                    var expectedPropertyValue = propertyInfo.GetValue(expected, new object[0]);
                    var actualPropertyvalue = propertyInfo.GetValue(actual, new object[0]);
                    if (expectedPropertyValue == null && actualPropertyvalue == null)
                    {
                        continue;
                    }
                    if (expectedPropertyValue == null || actualPropertyvalue == null)
                    {
                        return false;
                    }
                    var expectedEnumerable = expectedPropertyValue as IEnumerable;
                    var actualEnumerbale = actualPropertyvalue as IEnumerable;
                    if (expectedEnumerable != null && actualEnumerbale != null)
                    {
                        if (!expectedEnumerable.Cast<object>().SequenceEqual(actualEnumerbale.Cast<object>()))
                        {
                            return false;
                        }
                    }
                    else if (!expectedPropertyValue.Equals(actualPropertyvalue))
                    {
                        return false;
                    }
                }
                return true;
            }

            public void ThenItThrows<T>()
            {
                Assert.IsNotNull(exception, "Expected an exception");
                Assert.AreEqual(typeof(T), exception.GetType());
            }
        }

        protected interface IGiven
        {
            IWhen When(Action<T> action);
        }

        protected interface IWhen
        {
            void Then(params IEvent[] events);
            void ThenItThrows<T>();
        }
    }
}

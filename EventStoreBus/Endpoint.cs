﻿using System;
using DurableSubscriber.Diagnostics;
using EventStoreBus.Logging;

namespace EventStoreBus
{
    public class Endpoint : IStartable
    {
        private readonly Type implementationType;
        private readonly ILogger logger;
        private readonly IEndpointListener listener;

        public Endpoint(Type implementationType, IEndpointListener listener)
        {
            this.implementationType = implementationType;
            this.listener = listener;
            logger = LogManager.GetLogger(typeof(Gateway).Name + "(" + implementationType.Name + ")");
        }

        public State State
        {
            get
            {
                return new State("Gateway (" + implementationType.Name + ")", GetType())
                  .WithProperty("Type", implementationType.Name);
            }
        }

        public void Start()
        {
            logger.Info("Starting...");
            listener.Start();
            logger.Info("Stared.");
        }

        public void Stop()
        {
            logger.Info("Stopping...");
            listener.Stop();
            logger.Info("Stopped.");
        }
    }
}
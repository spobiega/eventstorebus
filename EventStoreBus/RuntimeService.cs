﻿using System;
using System.Collections.Generic;
using System.Linq;
using DurableSubscriber.Diagnostics;
using EventStoreBus.Api;

namespace EventStoreBus
{
    public class RuntimeService : IStartable
    {
        private readonly string name;
        private readonly CompositionRoot compositionRoot;
        private readonly IEnumerable<IStartable> modules;

        public RuntimeService(string name, CompositionRoot compositionRoot, IEnumerable<IStartable> modules)
        {
            this.name = name;
            this.compositionRoot = compositionRoot;
            this.modules = modules.ToList();
        }

        public void Start()
        {
            compositionRoot.Starting();
            foreach (var module in modules)
            {
                module.Start();
            }
            compositionRoot.Started();
        }

        public void Stop()
        {
            compositionRoot.Stopping();
            foreach (var module in modules)
            {
                module.Stop();
            }
            compositionRoot.Stopped();
        }

        public State State
        {
            get
            {
                return new State("Service", GetType())
                    .WithProperty("Name", name)
                    .WithChildStates(modules);
            }
        }
    }
}
﻿using System.Threading.Tasks;

namespace EventStoreBus
{
    public interface IDispatcher
    {
        bool CanHandle(object payload);
        object Handle(object target, params object[] arguments);
    }
}
﻿namespace EventStoreBus
{
    public enum DatabaseType
    {
        Domain,
        Commands,
        Infrastructure
    }
}
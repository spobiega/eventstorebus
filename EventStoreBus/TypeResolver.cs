﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EventStoreBus.Api;

namespace EventStoreBus
{
    public class TypeResolver
    {
        private readonly IEnumerable<ITypeAbbreviationProvider> abbreviationProviders;
        private readonly Assembly mainAssembly;
        private readonly List<ITypeWrapper> typeWrappers;

        public TypeResolver(string mainAssemblyName, IEnumerable<ITypeWrapper> typeWrappers, IEnumerable<ITypeAbbreviationProvider> abbreviationProviders)
        {
            mainAssembly = Assembly.Load(mainAssemblyName);
            this.typeWrappers = typeWrappers.ToList();
            this.abbreviationProviders = abbreviationProviders.ToList();
            this.typeWrappers.Add(new StatelessComponentFactoryWrapper());
        }

        public Type ResolveType(string typeName)
        {
            var type = InstantiateType(typeName);
            return TryWrapType(type);
        }

        private Type TryWrapType(Type type)
        {
            var wrappedType = typeWrappers.Where(x => x.CanWrap(type)).Select(x => x.Wrap(type)).FirstOrDefault();
            return wrappedType ?? type;
        }

        private Type InstantiateType(string partialTypeName)
        {
            var expandedName = TryExpandName(partialTypeName);
            if (expandedName != null)
            {
                return Type.GetType(expandedName, true);
            }
            if (partialTypeName.Contains(","))
            {
                return Type.GetType(partialTypeName, true);
            }
            return mainAssembly.GetType(partialTypeName, true);
        }

        private string TryExpandName(string partialTypeName)
        {
            return abbreviationProviders.Where(x => x.KnowsAbbreviationFor(partialTypeName))
                                        .Select(x => x.GetFullName(partialTypeName))
                                        .FirstOrDefault();
        }

        public Type ResolveFactoryType(string factoryTypeName, string itemTypeName)
        {
            if (factoryTypeName != null)
            {
                return ResolveType(factoryTypeName);
            }
            var itemType = InstantiateType(itemTypeName);
            var candidateFactoryNames = new[]
                {
                    string.Format("{0}Factory", itemType.FullName),
                    string.Format("{0}+Factory", itemType.FullName)
                };
            var factoryType =
                candidateFactoryNames.Select(x => itemType.Assembly.GetType(x, false)).FirstOrDefault(x => x != null);
            if (factoryType == null)
            {
                throw new InvalidOperationException(string.Format("Can't find factory type for {0}. Tried {1}",
                                                                  itemTypeName, string.Join(", ", candidateFactoryNames)));
            }
            return TryWrapType(factoryType);
        }

        private class StatelessComponentFactoryWrapper : ITypeWrapper
        {
            public bool CanWrap(Type canidate)
            {
                return canidate.GetInterfaces().Contains(typeof(IStatelessComponentFactory))
                    && canidate.GetConstructor(Type.EmptyTypes) != null;
            }

            public Type Wrap(Type bareType)
            {
                return typeof (StatelessComponentFactory<>).MakeGenericType(bareType);
            }
        }
    }
}
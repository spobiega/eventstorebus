﻿using System;
using DurableSubscriber;
using EventStoreBus.Api;

namespace EventStoreBus
{
    public interface IAutonomousComponentFactory
    {
        object Create(Type componentImplementation, IEventStoreConnectionAccessor domainStoreConnection, ISerializer domainStoreEventSerializer, ICommandSender commandSender);
        void Release(object component);
    }

}
﻿using System;
using DurableSubscriber;
using EventStore.ClientAPI;
using EventStoreBus.Api;
using EventStoreBus.Logging;
using IEvent = EventStoreBus.Api.IEvent;
using ILogger = EventStoreBus.Logging.ILogger;

namespace EventStoreBus
{
    public class EventPublisher : IEventPublisher
    {
        private readonly IEventStoreConnectionAccessor connection;
        private readonly ISerializer serializer;
        private readonly ILogger logger = LogManager.GetLoggerFor<EventPublisher>();

        public EventPublisher(IEventStoreConnectionAccessor connection, ISerializer serializer)
        {
            this.connection = connection;
            this.serializer = serializer;
        }

        public void Publish(string streamId, IEvent evnt, string commandId)
        {
            logger.Trace("Publishing {0} event to {1}.", x => x.Args(evnt.GetType().FullName, streamId));
            var serializedEvent = serializer.Serialize(Guid.NewGuid(), evnt, commandId);
            try
            {
                connection.Invoke(x => x.AppendToStream(streamId, ExpectedVersion.Any, new[] { serializedEvent }));
                logger.Debug("Publishing {0} event to {1}.", x => x.Args(evnt.GetType().FullName, streamId));
            }
            catch (Exception ex)
            {
                logger.ErrorException(ex, "Error publishing {0} event to {1}", evnt.GetType().FullName, streamId);
                throw;
            }
           
        }
    }
}
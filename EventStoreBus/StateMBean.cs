using System;
using System.Linq;
using DurableSubscriber.Diagnostics;
using NetMX;
using NetMX.OpenMBean;

namespace EventStoreBus
{
    public class StateMBean : IDynamicMBean
    {
        private readonly State state;

        public StateMBean(State state)
        {
            this.state = state;
        }

        public MBeanInfo GetMBeanInfo()
        {
            return MBean.Info(state.Type.AssemblyQualifiedName, state.Name)
                .WithAttributes(state.Attributes.Select(attribute => MBean.ReadOnlyAttribute(attribute.Name, attribute.Name).TypedAs(SimpleType.CreateSimpleType(attribute.ValueType))).ToArray())
                .AndNothingElse()();
        }

        public object GetAttribute(string attributeName)
        {
            return state.Attributes.First(x => x.Name == attributeName).Value;
        }

        public void SetAttribute(string attributeName, object value)
        {
            throw new NotImplementedException();
        }

        public object Invoke(string operationName, object[] arguments)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using System;
using DurableSubscriber.Diagnostics;

namespace EventStoreBus
{
    public interface IEventHandler : IInstrumentable
    {
        bool CanHandle(object payload);
        void Handle(HandledEvent evnt);
        void Start();
        void Stop();
    }
}
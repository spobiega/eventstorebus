﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using DurableSubscriber;
using DurableSubscriber.Diagnostics;
using EventStore.ClientAPI;
using EventStoreBus.Logging;
using ILogger = EventStoreBus.Logging.ILogger;

namespace EventStoreBus
{
    public class AutonomousComponent : IStartable
    {
        private readonly ILogger logger;
        private readonly string name;
        private readonly CommandHandler commandHandler;
        private readonly ISerializer serializer;
        private readonly EventSourceSelector<int> commandSource;
        private readonly ICheckpointingStrategy<int> checkpointingStrategy;
        private readonly IFailureHandlingStrategy failureHandlingStrategy;
        private bool running;
        private bool stopping;
        private bool restarting;
        private Scheduler scheduler;
        private long sequence;
        private readonly Stopwatch watch = new Stopwatch();

        public AutonomousComponent(string name, 
            ISerializer serializer, 
            CommandHandler commandHandler, 
            EventSourceSelector<int> commandSource, 
            ICheckpointingStrategy<int> checkpointingStrategy,
            IFailureHandlingStrategy failureHandlingStrategy)
        {
            this.name = name;
            this.commandHandler = commandHandler;
            this.serializer = serializer;
            this.commandSource = commandSource;
            this.checkpointingStrategy = checkpointingStrategy;
            this.failureHandlingStrategy = failureHandlingStrategy;
            commandSource.Event += OnCommand;
            logger = LogManager.GetLogger(typeof(AutonomousComponent).Name + "(" + name + ")");
        }

        private void OnCommand(object sender, RecordedEventEventArgs<int> e)
        {
            if (stopping)
            {
                return;
            }
            if (sequence % 100 == 0)
            {
                if (sequence != 0)
                {
                    watch.Stop();
                    logger.Info("Processed 100 commands in {0} ms", watch.ElapsedMilliseconds);
                    watch.Reset();
                }
                watch.Start();
            }
            sequence++;
            try
            {
                scheduler.ScheduleExecution(sequence,
                                        () => HandleCommand((RecordedEvent)e.Event, null),
                                        () => checkpointingStrategy.NotifyEventsProcessed(e.Position));
            }
            catch (EscalateFailureException)
            {
                Pause();
            }
        }

        private void HandleCommand(RecordedEvent recordedEvent, IFailureHandlingResult failureHandlingResult)
        {
            try
            {
                var evnt = serializer.Deserialize(recordedEvent);
                if (evnt == null)
                {
                    logger.Warning("Not deserializable event type encountered: {0}.", recordedEvent.EventType);
                    return;
                }
                if (!commandHandler.CanHandle(evnt.Payload))
                {
                    logger.Warning("Command can not be handled by the handler: {0}.", recordedEvent.EventType);
                }

                logger.Debug("Handling command {0}/{1}.", x => x.Args(recordedEvent.EventType, recordedEvent.EventId));
                commandHandler.Handle(evnt.Payload);
                logger.Trace("Successfully handled command {0}/{1}", x => x.Args(recordedEvent.EventType, recordedEvent.EventId));
            }
            catch (Exception ex)
            {
                var result = failureHandlingStrategy.HandleFailure(ex, failureHandlingResult, recordedEvent);
                logger.Info("Error handling command {0}/{1}. Action: {2}", recordedEvent.EventType, recordedEvent.EventId, result.Action);
                if (result.Action == FailureHandlingAction.Retry)
                {
                    HandleCommand(recordedEvent, result);
                }
                else if (result.Action == FailureHandlingAction.Escalate)
                {
                    logger.ErrorException(ex, "Escalating failure of handling command {0}/{1}", recordedEvent.EventType, recordedEvent.EventId);
                    throw new EscalateFailureException();
                }
                else if (result.Action == FailureHandlingAction.Skip)
                {
                    //Do nothing
                    logger.ErrorException(ex, "Skipping command {0}/{1} due to persistent processing failure", recordedEvent.EventType,
                                 recordedEvent.EventId);
                }
            }
        }

        private void Pause()
        {
            if (restarting)
            {
                return;
            }
            logger.Info("Pausing...");
            restarting = true;
            Task.Factory.StartNew(() =>
                                      {
                                          Stop();
                                          Thread.Sleep(5000);
                                          restarting = false;
                                          Start();
                                      });

        }

        public void Start()
        {
            logger.Info("Starting...");
            sequence = 0;
            scheduler = new Scheduler();
            commandSource.Start();
            running = true;
            logger.Info("Started.");
        }

        public void Stop()
        {
            logger.Info("Stopping...");
            stopping = true;
            commandSource.Stop();
            scheduler.WaitToFinish();
            running = false;
            logger.Info("Stopped.");
            stopping = false;
        }

        public State State
        {
            get
            {
                return new State("Autonomous component " + name, GetType())
                    .WithDomainPrefix(name)
                    .WithAttribute("Running", () => running)
                    .WithChild(commandSource)
                    .WithChild(commandHandler);
            }
        }
    }
}
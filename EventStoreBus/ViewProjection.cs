﻿using System;
using DurableSubscriber.Diagnostics;
using EventStoreBus.Api;
using EventStoreBus.Logging;
using Raven.Client;

namespace EventStoreBus
{
    public class ViewProjection : IEventHandler
    {
        private readonly ILogger logger;
        private readonly Type implementationType;
        private readonly IViewProjectionFactory viewProjectionFactory;
        private readonly IDispatcher dispatcher;
        private readonly IViewEngine viewEngine;        

        public ViewProjection(Type implementationType, IViewProjectionFactory viewProjectionFactory, IDispatcher dispatcher, IViewEngine viewEngine)
        {
            this.implementationType = implementationType;
            this.viewProjectionFactory = viewProjectionFactory;
            this.dispatcher = dispatcher;
            this.viewEngine = viewEngine;
            logger = LogManager.GetLogger(typeof(ViewProjection).Name + "(" + implementationType.Name + ")");
        }

        public bool CanHandle(object payload)
        {
            return dispatcher.CanHandle(payload);
        }

        public void Handle(HandledEvent evnt)
        {
            logger.Trace("Creating instance to handle {0} event.", x => x.Args(evnt.Payload.GetType().FullName));
            var projection = viewProjectionFactory.Create(implementationType, viewEngine.CreateWriters());
            try
            {
                logger.Trace("Handling {0} event.", x => x.Args(evnt.Payload.GetType().FullName));
                dispatcher.Handle(projection, evnt.Payload, evnt.Store, evnt.StreamId, evnt.Sequence, evnt.EventId);
                logger.Debug("{0} event successfully handled.", x => x.Args(evnt.Payload.GetType().FullName));
            }
            finally
            {
                logger.Trace("Releasing instance after handling {0} event.", x => x.Args(evnt.Payload.GetType().FullName));
                viewProjectionFactory.Release(projection);
            }
        }

        public void Start()
        {
            viewEngine.Start();
        }

        public void Stop()
        {
            viewEngine.Stop();
        }

        public State State
        {
            get
            {
                return new State("View projection (" + implementationType.Name + ")", GetType())
                    .WithProperty("Type", implementationType.Name);
            }
        }
    }
}
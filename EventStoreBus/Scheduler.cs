﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventStoreBus
{
    public class Scheduler
    {
        private readonly int maxConcurrencyLevel;
        private readonly List<RunningTask> runningTasks;
        private long currentSequence;
        private bool stopping;

        public Scheduler()
            : this(10)
        {
        }

        public Scheduler(int maxConcurrencyLevel)
        {
            this.maxConcurrencyLevel = maxConcurrencyLevel;
            runningTasks = new List<RunningTask>(maxConcurrencyLevel);
        }

        public void ScheduleExecution(long sequence, Action action, Action completion)
        {
            CheckForFaultedTasks();
            RemoveCompletedTasks();
            while (!HasEmptySlot())
            {
                var awaitedTasks = runningTasks.Select(x => x.Task).ToArray();
                Task.WaitAny(awaitedTasks);
                CheckForFaultedTasks();
                RemoveCompletedTasks();
            }
            if (!stopping)
            {
                Start(sequence, action, completion);
            }
        }

        public void WaitToFinish()
        {
            stopping = true;
            Task.WaitAll(runningTasks.Select(x => x.Task).ToArray());
            RemoveCompletedTasks();
        }

        private void Start(long sequence, Action action, Action completion)
        {
            var task = Task.Factory.StartNew(action);
            var runningTask = new RunningTask(sequence, task, completion);
            runningTasks.Add(runningTask);
        }

        private void RemoveCompletedTasks()
        {
            var completedTasks = runningTasks.Where(x => x.Task.IsCompleted).OrderBy(x => x.Sequence).ToList();
            var completedTaskInSequence = completedTasks.TakeWhile(completedTask => completedTask.Sequence == currentSequence + 1l);
            foreach (var completedTask in completedTaskInSequence)
            {
                runningTasks.Remove(completedTask);
                completedTask.Completion();
                currentSequence++;
            }
        }

        private void CheckForFaultedTasks()
        {
            var faulted = runningTasks.FirstOrDefault(x => x.Task.IsFaulted);
            if (faulted != null)
            {
                throw faulted.Task.Exception.InnerExceptions[0]; //Escalate failure exception.
            }
        }

        private bool HasEmptySlot()
        {
            return runningTasks.Count < maxConcurrencyLevel;
        }

        private class RunningTask
        {
            private readonly long sequence;
            private readonly Task task;
            private readonly Action completion;

            public RunningTask(long sequence, Task task, Action completion)
            {
                this.sequence = sequence;
                this.completion = completion;
                this.task = task;
            }

            public long Sequence
            {
                get { return sequence; }
            }

            public Task Task
            {
                get { return task; }
            }

            public Action Completion
            {
                get { return completion; }
            }
        }
    }
}
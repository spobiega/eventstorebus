﻿using System;
using System.Collections.Generic;
using EventStoreBus.Api;
using EventStoreBus.Logging;
using EventStoreBus.Receptors;
using EventStoreBus.Receptors.Api;

namespace EventStoreBus
{
    public class CommandSender : ICommandSender
    {
        private static readonly ILogger logger = LogManager.GetLoggerFor(typeof (CommandSender));

        private readonly int maximumRetries;
        private readonly Dictionary<string, ICommandRouter> routersByName = new Dictionary<string, ICommandRouter>();
        private readonly Dictionary<string, string > routersByImplementationType = new Dictionary<string, string>();

        public CommandSender()
            : this(10)
        {
        }

        public CommandSender(int maximumRetries)
        {
            this.maximumRetries = maximumRetries;
        }

        public void SendTo(string componentName, object command)
        {
            ICommandRouter router;
            if (!routersByName.TryGetValue(componentName, out router))
            {
                throw new InvalidOperationException("Cannot find route to " + componentName);
            }

            bool success = false;
            int attempts = 0;
            while (!success)
            {
                if (attempts > maximumRetries)
                {
                    throw new InvalidOperationException("All routes to " + componentName + " are down.");
                }
                attempts++;
                var destination = router.DetermineTargetStream(command);                
                logger.Debug("Sending {0} command to {1} via {2}, attempt {3}", x => x.Args(command.GetType(), componentName, destination, attempts));
                success = destination.Send(command);
            }
        }

        public void SendTo<T>(object command)
        {
            string componentName;
            if (!routersByImplementationType.TryGetValue(typeof(T).FullName, out componentName))
            {
                throw new InvalidOperationException("Cannot find route to " + typeof(T).Name);
            }
            SendTo(componentName, command);
        }

        public void RegisterComponent(string componentName, Type componentImplementationType, ICommandRouter router)
        {
            routersByName[componentName] = router;
            if (componentImplementationType != null)
            {
                routersByImplementationType[componentImplementationType.FullName] = componentName;                
            }
        }
    }
}
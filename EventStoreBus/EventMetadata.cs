﻿namespace EventStoreBus
{
    public class EventMetadata
    {
        public readonly int Version;
        public readonly string RelatesTo;

        public EventMetadata(int version, string relatesTo)
        {
            Version = version;
            RelatesTo = relatesTo;
        }
    }
}
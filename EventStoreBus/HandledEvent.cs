﻿using System;

namespace EventStoreBus
{
    public class HandledEvent
    {
        private readonly string store;
        private readonly string streamId;
        private readonly Guid eventId;
        private readonly int sequence;
        private readonly object payload;

        public HandledEvent(string store, string streamId, Guid eventId, int sequence, object payload)
        {
            this.store = store;
            this.streamId = streamId;
            this.payload = payload;
            this.sequence = sequence;
            this.eventId = eventId;
        }

        public string StreamId
        {
            get { return streamId; }
        }

        public Guid EventId
        {
            get { return eventId; }
        }

        public int Sequence
        {
            get { return sequence; }
        }

        public object Payload
        {
            get { return payload; }
        }

        public string Store
        {
            get { return store; }
        }
    }
}
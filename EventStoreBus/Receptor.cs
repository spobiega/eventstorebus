﻿using System;
using DurableSubscriber.Diagnostics;
using EventStoreBus.Api;
using EventStoreBus.Logging;
using EventStoreBus.Receptors.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus
{
    public class Receptor : IEventHandler
    {
        private readonly ILogger logger;

        private readonly Type implementationType;
        private readonly IDispatcher dispatcher;
        private readonly ICommandSender commandSender;
        private readonly IViewEngine viewEngine;
        private readonly IReceptorFactory receptorFactory;

        public Receptor(Type implementationType, IReceptorFactory receptorFactory, IDispatcher dispatcher, ICommandSender commandSender, IViewEngine viewEngine)
        {
            this.implementationType = implementationType;
            this.dispatcher = dispatcher;
            this.receptorFactory = receptorFactory;
            this.commandSender = commandSender;
            this.viewEngine = viewEngine;
            logger = LogManager.GetLogger(typeof(Receptor).Name + "(" + implementationType.Name +")");
        }

        public bool CanHandle(object payload)
        {
            var result = dispatcher.CanHandle(payload);
            logger.Trace("{0} handle {1} event.", x => x.Args(result ? "can" : "can't", payload.GetType().FullName));
            return result;
        }

        public void Handle(HandledEvent evnt)
        {
            logger.Trace("Creating instance to handle {0} event.", x => x.Args(evnt.Payload.GetType().FullName));
            var receptor = receptorFactory.Create(implementationType, commandSender, viewEngine.CreateReaders());
            try
            {
                logger.Trace("Handling {0} event.", x => x.Args(evnt.Payload.GetType().FullName));
                dispatcher.Handle(receptor, evnt.Payload, evnt.EventId);
                logger.Debug("{0} event successfully handled.", x => x.Args(evnt.Payload.GetType().FullName));
            }
            finally
            {
                logger.Trace("Releasing instance after handling {0} event.", x => x.Args(evnt.Payload.GetType().FullName));
                receptorFactory.Release(receptor);
            }
        }

        public void Start()
        {
            viewEngine.Start();
        }

        public void Stop()
        {
            viewEngine.Stop();
        }

        public State State
        {
            get
            {
                return new State("Receptor (" + implementationType.Name + ")", GetType())
                    .WithProperty("Type", implementationType.Name);
            }
        }
    }
}
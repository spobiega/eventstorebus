﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DurableSubscriber;
using EventStore.ClientAPI;

namespace EventStoreBus
{
    public class MultiDatabaseEventStoreConnection : IEventStoreConnection
    {
        private readonly string databaseName;
        private readonly IEventStoreConnection innerConnection;

        public MultiDatabaseEventStoreConnection(string databaseName, IEventStoreConnection innerConnection)
        {
            this.databaseName = databaseName;
            this.innerConnection = innerConnection;
        }

        public void AppendToStream(string stream, int expectedVersion, params EventData[] events)
        {
            innerConnection.AppendToStream(GetFullStreamName(stream), expectedVersion, events);
        }

        public void AppendToStream(string stream, int expectedVersion, IEnumerable<EventData> events)
        {
            innerConnection.AppendToStream(GetFullStreamName(stream), expectedVersion, events);
        }

        public Task AppendToStreamAsync(string stream, int expectedVersion, params EventData[] events)
        {
            return innerConnection.AppendToStreamAsync(GetFullStreamName(stream), expectedVersion, events);
        }

        public Task AppendToStreamAsync(string stream, int expectedVersion, IEnumerable<EventData> events)
        {
            return innerConnection.AppendToStreamAsync(GetFullStreamName(stream), expectedVersion, events);
        }

        public StreamEventsSlice ReadStreamEventsForward(string stream, int start, int count, bool resolveLinkTos)
        {
            return innerConnection.ReadStreamEventsForward(GetFullStreamName(stream), start, count, resolveLinkTos);
        }

        public Task<StreamEventsSlice> ReadStreamEventsForwardAsync(string stream, int start, int count, bool resolveLinkTos)
        {
            return innerConnection.ReadStreamEventsForwardAsync(GetFullStreamName(stream), start, count, resolveLinkTos);
        }

        public StreamEventsSlice ReadStreamEventsBackward(string stream, int start, int count, bool resolveLinkTos)
        {
            return innerConnection.ReadStreamEventsBackward(GetFullStreamName(stream), start, count, resolveLinkTos);
        }

        public Task<StreamEventsSlice> ReadStreamEventsBackwardAsync(string stream, int start, int count, bool resolveLinkTos)
        {
            return innerConnection.ReadStreamEventsBackwardAsync(GetFullStreamName(stream), start, count, resolveLinkTos);
        }

        public AllEventsSlice ReadAllEventsForward(Position position, int maxCount, bool resolveLinkTos)
        {
            return innerConnection.ReadAllEventsForward(position, maxCount, resolveLinkTos);
        }

        public Task<AllEventsSlice> ReadAllEventsForwardAsync(Position position, int maxCount, bool resolveLinkTos)
        {
            return innerConnection.ReadAllEventsForwardAsync(position, maxCount, resolveLinkTos);
        }

        public EventStoreSubscription SubscribeToStream(string stream, bool resolveLinkTos, Action<ResolvedEvent> eventAppeared, Action subscriptionDropped)
        {
            return innerConnection.SubscribeToStream(GetFullStreamName(stream), resolveLinkTos, eventAppeared, subscriptionDropped);
        }

        public EventStoreSubscription SubscribeToAll(bool resolveLinkTos, Action<ResolvedEvent> eventAppeared, Action subscriptionDropped)
        {
            return innerConnection.SubscribeToAll(resolveLinkTos, eventAppeared, subscriptionDropped);
        }

        private string GetFullStreamName(string streamNameWithoutDatabasePrefix)
        {
            return string.IsNullOrEmpty(streamNameWithoutDatabasePrefix)
                       ? "$ce-" + databaseName
                       : databaseName + "-" + streamNameWithoutDatabasePrefix;
        }
    }
}
﻿using System;
using System.Runtime.Serialization;

namespace EventStoreBus
{
    [Serializable]
    public class ConfigurationException : Exception
    {
        public ConfigurationException()
        {
        }

        public ConfigurationException(string message) : base(message)
        {
        }
        
        public ConfigurationException(string message, params object[] formatArgs) 
            : base(string.Format(message, formatArgs))
        {
        }

        public ConfigurationException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ConfigurationException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
﻿using System;
using DurableSubscriber;
using DurableSubscriber.Diagnostics;
using EventStoreBus.Api;
using EventStoreBus.Logging;

namespace EventStoreBus
{    
    public class CommandHandler : IInstrumentable
    {
        public ISerializer DomainStoreSerializer { get; set; }
        private readonly ILogger logger;

        private readonly Type implementationType;
        private readonly IAutonomousComponentFactory componentFactory;
        private readonly IDispatcher dispatcher;
        private readonly IEventStoreConnectionAccessor domainStoreConnection;
        private readonly ISerializer domainStoreSerializer;
        private readonly ICommandSender commandSender;

        public CommandHandler(Type implementationType, IAutonomousComponentFactory componentFactory, IDispatcher dispatcher, IEventStoreConnectionAccessor domainStoreConnection, ISerializer domainStoreSerializer, ICommandSender commandSender)
        {
            DomainStoreSerializer = domainStoreSerializer;
            this.implementationType = implementationType;
            this.componentFactory = componentFactory;
            this.dispatcher = dispatcher;
            this.domainStoreConnection = domainStoreConnection;
            this.domainStoreSerializer = domainStoreSerializer;
            this.commandSender = commandSender;
            logger = LogManager.GetLogger(typeof(CommandHandler).Name + "(" + implementationType.Name + ")");
        }

        public bool CanHandle(object payload)
        {
            var result = dispatcher.CanHandle(payload);
            logger.Debug("{0} handle {1} command.", x => x.Args(result ? "can" : "can't", payload.GetType().FullName));
            return result;
        }

        public void Handle(object payload)
        {
            logger.Trace("Creating instance to handle {0} command.", x => x.Args(payload.GetType().FullName));
            var component = componentFactory.Create(implementationType, domainStoreConnection, domainStoreSerializer, commandSender);
            try
            {
                logger.Trace("Handling {0} command.", x => x.Args(payload.GetType().FullName));
                dispatcher.Handle(component, payload);
                logger.Debug("{0} command successfully handled.", x => x.Args(payload.GetType().FullName));
            }
            finally
            {
                logger.Trace("Releasing instance after handling {0} command.", x => x.Args(payload.GetType().FullName));
                componentFactory.Release(component);
            }
        }

        public State State
        {
            get
            {
                return new State("Command handler (" + implementationType.Name + ")", GetType())
                    .WithProperty("Type", implementationType.Name);
            }
        }
    }
}
﻿using System;
using System.Text;
using DurableSubscriber;
using EventStore.ClientAPI;

namespace EventStoreBus
{
    public class DefaultFailureHandlingStrategy : IFailureHandlingStrategy
    {
        private readonly int maxFailedAttempts;
        private readonly IEventStoreConnectionAccessor failedEventStore;
        private readonly string failedEventStream;

        public DefaultFailureHandlingStrategy(int maxFailedAttempts, IEventStoreConnectionAccessor failedEventStore, string componentName)
        {
            this.maxFailedAttempts = maxFailedAttempts;
            this.failedEventStream = string.Format("#{0}-failures", componentName);
            this.failedEventStore = failedEventStore;
        }

        public IFailureHandlingResult HandleFailure(Exception exception, IFailureHandlingResult previousResult, RecordedEvent evnt)
        {
            var typedResult = (DefaultFailureHandlingResult) previousResult ??
                              new DefaultFailureHandlingResult(FailureHandlingAction.Retry, 0);

            if (typedResult.FailedAttempts < maxFailedAttempts)
            {
                return typedResult.Increment(FailureHandlingAction.Retry);
            }
            if (AppendFailedEventLink(evnt))
            {
                return typedResult.Increment(FailureHandlingAction.Skip);
            }
            return typedResult.Increment(FailureHandlingAction.Retry);
        }

        private bool AppendFailedEventLink(RecordedEvent evnt)
        {
            var body = Encoding.UTF8.GetBytes(evnt.EventNumber + "@" + evnt.EventStreamId);
            EventData linkEvent = new EventData(Guid.NewGuid(), "$>", false, body, new byte[0]);
            return failedEventStore.TryInvoke(x => x.AppendToStream(failedEventStream, ExpectedVersion.Any, new [] {linkEvent}));
        }
    }
}
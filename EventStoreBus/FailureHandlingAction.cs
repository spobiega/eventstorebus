﻿namespace EventStoreBus
{
    public enum FailureHandlingAction
    {
        Retry,
        Skip,
        Escalate
    }
}
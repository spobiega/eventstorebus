﻿using System.Collections.Generic;
using System.Linq;
using DurableSubscriber.Diagnostics;

namespace EventStoreBus
{
    public class RuntimeModule : IStartable
    {
        private readonly string name;
        private readonly IEnumerable<IStartable> runtimeComponents;

        public RuntimeModule(string name, IEnumerable<IStartable> runtimeComponents)
        {
            this.name = name;
            this.runtimeComponents = runtimeComponents.ToList();
        }

        public void Start()
        {
            foreach (var module in runtimeComponents)
            {
                module.Start();
            }
        }

        public void Stop()
        {
            foreach (var module in runtimeComponents)
            {
                module.Stop();
            }
        }

        public State State
        {
            get
            {
                return new State("Module", GetType())
                    .WithProperty("Name",name)
                    .WithChildStates(runtimeComponents);
            }
        }
    }
}
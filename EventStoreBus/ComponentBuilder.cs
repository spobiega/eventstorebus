using System;
using System.Linq;
using DurableSubscriber;
using EventStore.ClientAPI;
using EventStoreBus.Api;
using EventStoreBus.Config;
using EventStoreBus.Receptors.Api;
using Environment = EventStoreBus.Config.Environment;

namespace EventStoreBus
{
    public class ComponentBuilder
    {
        private readonly string deploymentId;
        private readonly EventStoreConnectionManager connectionManager;
        private readonly CompositionRoot serviceObject;
        private readonly Module module;
        private readonly Service service;
        private readonly Common.Version version;
        private readonly Environment environment;
        private readonly TypeResolver typeResolver;

        public ComponentBuilder(string deploymentId, EventStoreConnectionManager connectionManager, CompositionRoot serviceObject, Module module, Service service, string version, Environment environment, TypeResolver typeResolver)
        {
            this.deploymentId = deploymentId;
            this.connectionManager = connectionManager;
            this.serviceObject = serviceObject;
            this.module = module;
            this.service = service;
            this.version = version;
            this.environment = environment;
            this.typeResolver = typeResolver;
        }

        public IStartable BuildEventProcessorComponent(EventProcessor eventProcessorDefinition)
        {
            var failureHandlingStrategy = BuildFailureHandlingStrategy(eventProcessorDefinition.Name, eventProcessorDefinition.FailureHandling);
            var processorHost = new EventHandlerHost(eventProcessorDefinition.Name, failureHandlingStrategy);

            var storeName = eventProcessorDefinition.DataStore;

            var viewEngine = GetViewEngine(storeName);
            var commandSender = BuildCommandSender();

            foreach (var stage in eventProcessorDefinition.Stages)
            {
                if (stage is View)
                {
                    var viewProjectionType = typeResolver.ResolveType(stage.Implementation);
                    var viewProjectionFactoryType = typeResolver.ResolveFactoryType(stage.Factory, stage.Implementation);
                    var viewProjectionFactory = (IViewProjectionFactory) serviceObject.CreateInstance(viewProjectionFactoryType);

                    processorHost.Add(new ViewProjection(viewProjectionType, viewProjectionFactory, new EnvelopeDispatcher(viewProjectionType), viewEngine));
                }
                else if (stage is Config.Gateway)
                {
                    var handlerType = typeResolver.ResolveType(stage.Implementation);
                    var handlerFactoryType = typeResolver.ResolveFactoryType(stage.Factory, stage.Implementation);
                    var handlerFactory = (IGatewayFactory)serviceObject.CreateInstance(handlerFactoryType);

                    processorHost.Add(new Gateway(handlerType, handlerFactory, new DynamicDispatcher(handlerType), commandSender, viewEngine));
                }
            }

            
            foreach (var subscription in eventProcessorDefinition.Subscriptions)
            {
                var checkpointingStrategy = BuildCheckpointingStrategy(eventProcessorDefinition.Name, subscription);
                var serializer = connectionManager.GetSerializer(subscription.SourceStore);
                var connection = connectionManager.GetConnection(subscription.SourceStore, DatabaseType.Domain);
                processorHost.SubscribeTo(subscription.SourceStore, connection, serializer, checkpointingStrategy);
            }

            return processorHost;
        }

        private IViewEngine GetViewEngine(string storeName)
        {
            if (storeName ==  null)
            {
                return new NullViewEngine();
            }
            var viewStore = service.ViewStores.FirstOrDefault(x => x.Name == storeName);
            if (viewStore == null)
            {
                throw new ConfigurationException("View store {0} not found in service definition.", storeName);
            }
            var viewStoreNode = environment.ViewStores.Single(x => x.Name == storeName);
            var viewEngineType = typeResolver.ResolveType(viewStore.EngineImplementation);
            return (IViewEngine) serviceObject.CreateInstance(viewEngineType, viewStore.Name, viewStoreNode.ConnectionString);
        }

        public IStartable BuildEndpointComponent(Config.Endpoint endpoint)
        {
            var commandSender = BuildCommandSender();
            var viewEngine = GetViewEngine(endpoint.ViewStore);

            var listenerFactoryType = typeResolver.ResolveFactoryType(endpoint.Factory, endpoint.Implementation);
            var listenerType = typeResolver.ResolveType(endpoint.Implementation);
            var listenerFactory = (IListenerFactory)serviceObject.CreateInstance(listenerFactoryType);
            var listener = listenerFactory.Create(listenerType, endpoint.Name, endpoint.Address, commandSender, viewEngine);

            return new Endpoint(listenerType, listener);
        }

        public IStartable BuildReceptorComponent(Config.Receptor receptorDefinition)
        {
            var failureHandlingStrategy = BuildFailureHandlingStrategy(receptorDefinition.Name, receptorDefinition.FailureHandling);
            var receptorHost = new EventHandlerHost(receptorDefinition.Name, failureHandlingStrategy);

            var receptorType = typeResolver.ResolveType(receptorDefinition.Implementation);
            var receptorFactoryType = typeResolver.ResolveFactoryType(receptorDefinition.Factory, receptorDefinition.Implementation);
            var receptorFactory = (IReceptorFactory) serviceObject.CreateInstance(receptorFactoryType);
            var commandSender = BuildCommandSender();
            var viewEngine = GetViewEngine(receptorDefinition.ViewStore);

            receptorHost.Add(new Receptor(receptorType, 
                receptorFactory, 
                new DynamicDispatcher(receptorType), 
                commandSender,
                viewEngine));

            foreach (var subscription in receptorDefinition.Subscriptions)
            {
                var checkpointingStrategy = BuildCheckpointingStrategy(receptorDefinition.Name, subscription);
                var connection = connectionManager.GetConnection(subscription.SourceStore, DatabaseType.Domain);
                var serializer = connectionManager.GetSerializer(subscription.SourceStore);
                receptorHost.SubscribeTo(subscription.SourceStore, connection, serializer, checkpointingStrategy);
            }

            return receptorHost;
        }

        private CommandSender BuildCommandSender()
        {
            var commandSender = new CommandSender();

            //Should receptor be able to send commands to all modules?
            foreach (var m in service.Modules)
            {
                foreach (var component in m.Components.OfType<Config.CommandProcessor>())
                {
                    var componentDeployments = environment.Deployments
                                                          .Where(x => component.Name == x.Component
                                                                      && service.Name == x.Service
                                                                      && version.IsCompatible(x.Version));

                    var commandDestinations = componentDeployments
                        .Select(x =>
                            {
                                var connection = connectionManager.GetConnection(m.DomainStore.Name, DatabaseType.Commands);
                                var serializer = connectionManager.GetSerializer(m.DomainStore.Name);
                                return new CommandDestination(connection, serializer, GetInputStreamNameForComponent(x));
                            });
                    var router = new RoundRobinRouter(commandDestinations);
                    commandSender.RegisterComponent(component.Name, typeResolver.ResolveType(component.Implementation), router);
                }
            }
            return commandSender;
        }

        private static string GetInputStreamNameForComponent(ComponentDeployment deployment)
        {
            return GetInputStreamNameForComponent(deployment.Component, deployment.DeploymentId, deployment.Version);
        }

        private static string GetInputStreamNameForComponent(string component, string deploymentId, Common.Version version)
        {
            return string.Format("{0}-{1}-{2}.{3}", component, deploymentId, version.Major, version.Minor);
        }

        public IStartable BuildCommandProcessorComponent(Config.CommandProcessor componentDefinition)
        {
            var commandStoreSerializer = connectionManager.GetSerializer(module.DomainStore.Name);
            var commandStoreConnection = connectionManager.GetConnection(module.DomainStore.Name, DatabaseType.Commands);

            var componentType = typeResolver.ResolveType(componentDefinition.Implementation);
            var componentFactoryType = typeResolver.ResolveFactoryType(componentDefinition.Factory, componentDefinition.Implementation);
            var componentFactory = (IAutonomousComponentFactory)serviceObject.CreateInstance(componentFactoryType);

            var streamName = GetInputStreamNameForComponent(componentDefinition.Name, deploymentId, version);

            var checkpointingStrategyImpl = new SingleStreamEventStoreCheckpointingStrategy(commandStoreConnection, streamName);
            var checkpointingStrategy = new BatchEventStoreCheckpointingStrategy<int>(checkpointingStrategyImpl,
                                                                                      componentDefinition.Checkpointing.
                                                                                          BatchSize);

            var pullSource = new SingleEventStreamPullSource(commandStoreConnection, streamName, checkpointingStrategy);
            var pushSource = new SingleEventStreamPushSource(commandStoreConnection, streamName);
            var selector = new EventSourceSelector<int>(pullSource, pushSource, new RecordedEventIdentityExtractor());

            var failureHandlingStrategy = BuildFailureHandlingStrategy(componentDefinition.Name, componentDefinition.FailureHandling);

            var domainStoreConnection = connectionManager.GetConnection(module.DomainStore.Name, DatabaseType.Domain);
            var domainStoreSerializer = connectionManager.GetSerializer(module.DomainStore.Name);

            var commandSender = BuildCommandSender();
            var commandHandler = new CommandHandler(componentType, componentFactory, new DynamicDispatcher(componentType), domainStoreConnection, domainStoreSerializer, commandSender);
            var component = new AutonomousComponent(componentDefinition.Name, commandStoreSerializer, commandHandler, selector, checkpointingStrategy, failureHandlingStrategy);

            return component;
        }

        private IFailureHandlingStrategy BuildFailureHandlingStrategy(string componentName, FailureHandling failureHandlingDefinition)
        {
            var databaseName = module.DomainStore != null
                                   ? module.DomainStore.Name
                                   : service.Name;

            if (failureHandlingDefinition.Default != null)
            {
                return new DefaultFailureHandlingStrategy(failureHandlingDefinition.Default.Retries,
                                                          connectionManager.GetConnection(databaseName, DatabaseType.Infrastructure),
                                                          componentName);
            }
            if (failureHandlingDefinition.NoSkip != null)
            {
                return new ViewProjectionFailureHandlingStrategy(failureHandlingDefinition.NoSkip.Retries);
            }
            throw new NotSupportedException("Failure handling strategy not defined.");
        }

        private ICheckpointingStrategy<int> BuildCheckpointingStrategy(string componentName, Config.Subscription subscription)
        {
            var databaseName = module.DomainStore != null
                                   ? module.DomainStore.Name
                                   : service.Name;

            var connection = connectionManager.GetConnection(databaseName, DatabaseType.Infrastructure);
            var streamName = componentName + "-" + subscription.SourceStore;
            var implementation = new SingleStreamEventStoreCheckpointingStrategy(connection, streamName);
            return new BatchEventStoreCheckpointingStrategy<int>(implementation,
                                                                     subscription.Checkpointing.BatchSize);
        }
    }
}
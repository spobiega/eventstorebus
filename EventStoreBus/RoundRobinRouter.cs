﻿using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Receptors;

namespace EventStoreBus
{
    public class RoundRobinRouter : ICommandRouter
    {
        private readonly IList<CommandDestination> targetStreams;
        private long counter;

        public RoundRobinRouter(IEnumerable<CommandDestination> targetStreams)
        {
            this.targetStreams = targetStreams.ToList();
        }

        public CommandDestination DetermineTargetStream(object command)
        {
            counter++;
            return targetStreams[(int)(counter%targetStreams.Count)];
        }
    }
}
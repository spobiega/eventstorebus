﻿using EventStoreBus.Views.Api;

namespace EventStoreBus
{
    public interface IViewEngine : IStartable
    {
        IViewReaders CreateReaders();
        IViewWriters CreateWriters();
    }
}
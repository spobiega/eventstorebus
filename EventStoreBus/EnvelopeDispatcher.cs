﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EventStoreBus.Api;

namespace EventStoreBus
{
    public class EnvelopeDispatcher : IDispatcher
    {
        private readonly Dictionary<Type, MethodInfo> handlingMethods;

        public EnvelopeDispatcher(Type targetType)
            : this(targetType, "When")
        {
        }

        public EnvelopeDispatcher(Type targetType, string handlerMethodName)
        {
            handlingMethods = targetType.GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => x.Name == handlerMethodName)
                .ToDictionary(InferPayloadType, x => x);
        }

        private static Type InferPayloadType(MethodInfo handlerMethod)
        {
            var firstParameterType = handlerMethod.GetParameters()[0].ParameterType;
            if (!firstParameterType.IsGenericType || firstParameterType.GetGenericTypeDefinition() != typeof(Envelope<>))
            {
                throw new InvalidOperationException("Handler methods have to accept an argument of type Envelope<T>");
            }
            return firstParameterType.GetGenericArguments()[0];
        }

        public bool CanHandle(object payload)
        {
            return handlingMethods.ContainsKey(payload.GetType());
        }

        public object Handle(object target, params object[] arguments)
        {
            var payloadType = arguments[0].GetType();
            var method = handlingMethods[payloadType];
            var envelope = Activator.CreateInstance(typeof (Envelope<>).MakeGenericType(payloadType), arguments);
            return method.Invoke(target, new[]{envelope});
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using DurableSubscriber;
using EventStore.ClientAPI;
using EventStoreBus.Api;
using EventStoreBus.Logging;
using ILogger = EventStoreBus.Logging.ILogger;

namespace EventStoreBus
{
    public class DefaultSerializer : ISerializer
    {
        private static readonly ILogger logger = LogManager.GetLoggerFor<DefaultSerializer>();

        private const string EventTypePrefix = "ESB:";
        private readonly List<Assembly> assemblies = new List<Assembly>();

        public EventData Serialize(Guid uniqueId, object payload, string relatesTo)
        {
            var payloadType = payload.GetType();
            logger.Debug("Serializing payload {0}: {1}", f => f.Args(payloadType.FullName, FormatValue(payload)));
            var fullName = payloadType.FullName;
            var startOfVersionNumber = fullName.LastIndexOf("_");
            string typeName;
            int version;
            if (startOfVersionNumber < 0)
            {
                typeName = fullName;
                var versionAttribute = payloadType.GetCustomAttributes(typeof (VersionAttribute), false)
                    .Cast<VersionAttribute>()
                    .FirstOrDefault();
                if (versionAttribute == null)
                {
                    throw new InvalidOperationException("Version attribute missing from type "+payloadType.FullName);
                }
                version = versionAttribute.VersionNumber;
            }
            else
            {
                typeName = fullName.Substring(0, startOfVersionNumber);
                version = int.Parse(fullName.Substring(startOfVersionNumber));
            }
            var serializer = new DataContractSerializer(payloadType);
            byte[] data;
            byte[] metadata;
            using (var stream = new MemoryStream())
            {
                serializer.WriteObject(stream, payload);
                stream.Flush();
                data = stream.ToArray();
            }
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(version);
                writer.Write(relatesTo != null);
                if (relatesTo != null)
                {
                    writer.Write(relatesTo);
                }
                writer.Flush();
                metadata = stream.ToArray();
            }

            return new EventData(uniqueId, EventTypePrefix + typeName, false, data, metadata);
        }

        public PersistentEvent Deserialize(RecordedEvent recordedEvent)
        {
            if (!recordedEvent.EventType.StartsWith(EventTypePrefix))
            {
                return null;
            }
            int version;
            string relatesTo = null;
            using (var stream = new MemoryStream(recordedEvent.Metadata))
            using (var reader = new BinaryReader(stream))
            {
                version = reader.ReadInt32();
                var hasRelated = reader.ReadBoolean();
                if (hasRelated)
                {
                    relatesTo = reader.ReadString();                    
                }
            }
            var eventType = recordedEvent.EventType.Substring(EventTypePrefix.Length);
            var explicitTypeName = eventType + "_" + version;
            Type payloadType = GetPayloadType(explicitTypeName);
            if (payloadType == null)
            {
                payloadType = GetPayloadType(eventType);
                if (payloadType == null)
                {
                    logger.Trace("Skipping unrecognized event type: "+eventType);
                    return null;
                }
            }
            var serializer = new DataContractSerializer(payloadType);
            object payload;
            using (var stream = new MemoryStream(recordedEvent.Data))
            {
                payload = serializer.ReadObject(stream);
            }
            logger.Trace("Deserialized payload {0}: {1}", f => f.Args(payloadType.FullName, FormatValue(payload)));
            return new PersistentEvent(payload, new EventMetadata(version, relatesTo),recordedEvent.EventId);
        }

        private Type GetPayloadType(string fullTypeName)
        {
            return assemblies.Select(x => x.GetType(fullTypeName, false)).Where(x => x != null).FirstOrDefault();
        }

        public void RegisterEvents(Assembly eventAssembly)
        {
            assemblies.Add(eventAssembly);
        }

        private static string FormatProperty(string name, object value)
        {
            return name + ": " + FormatValue(value);
        }

        private static string FormatValue(object value)
        {
            if (value == null)
            {
                return "<null>";
            }
            var payloadType = value.GetType();
            if (payloadType.IsPrimitive || payloadType.IsValueType || payloadType == typeof(string))
            {
                return value.ToString();
            }
            var enumerableValue = value as IEnumerable;
            if (enumerableValue != null)
            {
                return "[ " + string.Join(", ", enumerableValue.Cast<object>().Select(FormatValue)) + " ]";
            }
            var formattedProperties = payloadType.GetProperties().Select(x => FormatProperty(x.Name, x.GetValue(value, new object[0])));
            return "{ " + string.Join(", ", formattedProperties) + " }";
        }
    }
}
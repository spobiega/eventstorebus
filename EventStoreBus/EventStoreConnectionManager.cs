﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using DurableSubscriber;
using EventStore.ClientAPI;
using EventStoreBus.Config;
using Assembly = System.Reflection.Assembly;
using Environment = EventStoreBus.Config.Environment;

namespace EventStoreBus
{
    public class EventStoreConnectionManager
    {
        private readonly Environment environment;
        private readonly Dictionary<string, IEventStoreConnectionAccessor> connections = new Dictionary<string, IEventStoreConnectionAccessor>();
        private readonly Dictionary<string, ISerializer> serializers = new Dictionary<string, ISerializer>();
        private readonly IEnumerable<Config.EventStore> allEventStores; 

        public EventStoreConnectionManager(Service service, Environment environment)
        {
            this.environment = environment;
            allEventStores = service.ExternalEventStores.Concat(service.Modules.Select(x => x.DomainStore).Where(x => x != null));
        }

        public ISerializer GetSerializer(string eventStoreName)
        {
            ISerializer serializer;
            if (!serializers.TryGetValue(eventStoreName, out serializer))
            {
                var store = allEventStores.FirstOrDefault(x => x.Name == eventStoreName);
                if (store == null)
                {
                    throw new ConfigurationException("Event store {0} not found in service's description", eventStoreName);
                }                

                var serializerType = store.SerializerImplementation != null 
                    ? Type.GetType(store.SerializerImplementation, true)
                    : typeof(DefaultSerializer);

                serializer = (ISerializer)Activator.CreateInstance(serializerType);
                foreach (var assemblyRef in store.Events)
                {
                    var assemblyName = new AssemblyName(assemblyRef.Name);
                    var assembly = Assembly.Load(assemblyName);
                    serializer.RegisterEvents(assembly);
                }
                serializers[eventStoreName] = serializer;
            }
            return serializer;
        }
        
        public IEventStoreConnectionAccessor GetConnection(string eventStoreName, DatabaseType databaseType)
        {
            var node = environment.EventStores.FirstOrDefault(x => x.Databases.Any(db => db.Name == eventStoreName));
            if (node == null)
            {
                throw new ConfigurationException("No node hosting event store {0} was found.", eventStoreName);
            }

            var fullDatabaseName = eventStoreName + databaseType.ToString();
            IEventStoreConnectionAccessor connection;
            if (!connections.TryGetValue(fullDatabaseName, out connection))
            {
                var tcpEndPoint = GetTcpEndPoint(node);
                var httpEndPoint = GetHttpEndPoint(node, tcpEndPoint);
                connection = new PersistentEventStoreConnectionAccessor(tcpEndPoint, httpEndPoint, fullDatabaseName, ConnectionSettings.Create());
                connections[fullDatabaseName] = connection;
            }
            return connection;
        }

        private static IPEndPoint GetTcpEndPoint(EventStoreNode node)
        {
            var addresses = Dns.GetHostAddresses(node.Address);
            return new IPEndPoint(addresses[0], node.TcpPort != 0 ? node.TcpPort : 1113);
        }

        private static IPEndPoint GetHttpEndPoint(EventStoreNode node, IPEndPoint tcpEndpoint)
        {
            var addresses = Dns.GetHostAddresses(node.Address);
            return new IPEndPoint(addresses[0], node.HttpPort != 0 ? node.HttpPort : tcpEndpoint.Port + 1000);
        }
    }
}
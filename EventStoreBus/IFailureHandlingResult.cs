﻿namespace EventStoreBus
{
    public interface IFailureHandlingResult
    {
        FailureHandlingAction Action { get; }
    }
}
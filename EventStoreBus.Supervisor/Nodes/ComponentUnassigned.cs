﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes
{
    [DataContract]
    [Version(1)]
    public class ComponentUnassigned : IEvent
    {
        [DataMember]
        public string AssignmentId { get; set; }

        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string Component { get; set; }
    }
}
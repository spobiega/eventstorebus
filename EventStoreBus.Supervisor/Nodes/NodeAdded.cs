﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes
{
    [DataContract]
    [Version(1)]
    public class NodeAdded : IEvent
    {
        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public string ListenUrl { get; set; }
    }
}

﻿namespace EventStoreBus.Supervisor.Nodes.Gateway
{
    public enum Operation
    {
        Deploy,
        Undeploy,
        Start,
        Stop
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes.Gateway
{
    [DataContract]
    [Version(1)]
    public class DeploymentFailed : IEvent
    {
        [DataMember]
        public string OperationId { get; set; }

        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        [DataMember]
        public string Reason { get; set; }
    }
}
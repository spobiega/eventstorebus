using System;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes.Handlers;

namespace EventStoreBus.Supervisor.Nodes.Gateway
{
    public class NodeGateway
    {
        private const string StreamId = "NodeGateway";

        private readonly INodeController nodeController;
        private readonly IEventPublisher eventPublisher;

        public NodeGateway(INodeController nodeController, IEventPublisher eventPublisher)
        {
            this.nodeController = nodeController;
            this.eventPublisher = eventPublisher;
        }

        public void When(Deploy command)
        {
            try
            {
                var confirmation = nodeController.Deploy(command.NodeUrl, command.DeploymentUrl, command.Service, command.Component,
                                      command.Version);
                eventPublisher.Publish(StreamId, new DeploymentSucceeded()
                {
                    DeploymentId = command.DeploymentId,
                    NodeId = command.NodeId,
                    OperationId = command.OperationId,
                    WSManagementUrl = confirmation.WSManagementUrl,
                    WebManagementUrl = confirmation.WebManagementUrl
                }, command.Id);
            }
            catch (Exception ex)
            {
                eventPublisher.Publish(StreamId, new DeploymentFailed()
                {
                    DeploymentId = command.DeploymentId,
                    Reason = ex.Message,
                    NodeId = command.NodeId,
                    OperationId = command.OperationId
                }, command.Id);
            }
        }       

        public void When(Undeploy command)
        {
            Try(command.Id, command.OperationId,  command.NodeId, command.DeploymentId, Operation.Undeploy,
                () => nodeController.Undeploy(command.NodeUrl, command.DeploymentId));
        }
        
        public void When(Start command)
        {
            Try(command.Id, command.OperationId,  command.NodeId, command.DeploymentId, Operation.Start,
                () => nodeController.Start(command.NodeUrl, command.DeploymentId));
        }
        
        public void When(Stop command)
        {
            Try(command.Id, command.OperationId, command.NodeId, command.DeploymentId, Operation.Stop,
                () => nodeController.Stop(command.NodeUrl, command.DeploymentId));
        }

        private void Try(string commandId, string operationId, string nodeId, string deploymentId, Operation operation, Action guardedAction)
        {
            try
            {
                guardedAction();
                eventPublisher.Publish(StreamId, new OperationSucceeded()
                                                     {
                                                         DeploymentId = deploymentId,
                                                         NodeId = nodeId,
                                                         Operation = operation,
                                                         OperationId = operationId
                                                     }, commandId);
            }
            catch (Exception ex)
            {
                eventPublisher.Publish(StreamId, new OperationFailed()
                                                     {
                                                         DeploymentId = deploymentId,
                                                         Reason = ex.Message,
                                                         NodeId = nodeId,
                                                         Operation = operation,
                                                         OperationId = operationId
                                                     }, commandId);
            }
        }

        public class Factory : IStatelessComponentFactory
        {
            public object Create(Type componentImplementation, IEventPublisher eventPublisher)
            {
                return new NodeGateway(new NodeController(), eventPublisher);
            }

            public void Release(object component)
            {
            }
        }
    }

}
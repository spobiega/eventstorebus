﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes.Gateway
{
    [DataContract]
    [Version(1)]
    public class Deploy : Command
    {
        [DataMember]
        public string OperationId { get; set; }

        [DataMember]
        public string NodeId { get; set; }
        
        [DataMember]
        public string NodeUrl { get; set; }
        
        [DataMember]
        public string DeploymentId { get; set; }
        
        [DataMember]
        public string DeploymentUrl { get; set; }

        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string Component { get; set; }
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes
{
    [DataContract]
    [Version(1)]
    public class ComponentDeployed : IEvent
    {
        [DataMember]
        public string OperationId { get; set; }

        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }
        
        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string Component { get; set; }

        [DataMember]
        public string WSManagementUrl { get; set; }

        [DataMember]
        public string WebManagementUrl { get; set; }
    }
}
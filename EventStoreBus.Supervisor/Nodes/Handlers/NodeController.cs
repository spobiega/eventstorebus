﻿using System;
using System.Threading.Tasks;

namespace EventStoreBus.Supervisor.Nodes.Handlers
{
    public class NodeController : INodeController
    {
        public void UpdateEnvironment(string nodeUrl, Config.Environment environment)
        {
            try
            {
                var implementation = new EventStoreBus.Node.Client.NodeController(nodeUrl);
                implementation.UpdateEnvironmentAsync(environment).Wait();
            }
            catch (AggregateException ex)
            {
                throw ex.Flatten().InnerException;
            }
        }

        public DeploymentSlot ClaimDeploymentSlot(string nodeUrl)
        {
            try
            {
                var implementation = new EventStoreBus.Node.Client.NodeController(nodeUrl);
                return implementation.ClaimDeploymentSlotAsync()
                    .ContinueWith(x => new DeploymentSlot(x.Result.Item2, x.Result.Item1), TaskContinuationOptions.OnlyOnRanToCompletion)
                    .Result;
            }
            catch (AggregateException ex)
            {
                throw ex.Flatten().InnerException;
            }
        }

        public DeploymentConfirmation Deploy(string nodeUrl, string deploymentUrl, string service, string component, string version)
        {
            try
            {
                var implementation = new EventStoreBus.Node.Client.NodeController(nodeUrl);
                var deployment = implementation.DeployAsync(deploymentUrl, service, version, component).Result;
                return new DeploymentConfirmation(deployment.WSManagementUrl, deployment.WebManagementUrl);
            }
            catch (AggregateException ex)
            {
                throw ex.Flatten().InnerException;
            }
        }

        public void Undeploy(string nodeUrl, string deploymentId)
        {
            try
            {
                var implementation = new EventStoreBus.Node.Client.NodeController(nodeUrl);
                implementation.UndeployAsync(deploymentId).Wait();
            }
            catch (AggregateException ex)
            {
                throw ex.Flatten().InnerException;
            }
        }
        
        public void Start(string nodeUrl, string deploymentId)
        {
            try
            {
                var implementation = new EventStoreBus.Node.Client.NodeController(nodeUrl);
                implementation.StartAsync(deploymentId).Wait();
            }
            catch (AggregateException ex)
            {
                throw ex.Flatten().InnerException;
            }
        }
        
        public void Stop(string nodeUrl, string deploymentId)
        {
            try
            {
                var implementation = new EventStoreBus.Node.Client.NodeController(nodeUrl);
                implementation.StopAsync(deploymentId).Wait();
            }
            catch (AggregateException ex)
            {
                throw ex.Flatten().InnerException;
            }
        }

    }
}
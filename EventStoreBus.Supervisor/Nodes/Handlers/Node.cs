﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders;

namespace EventStoreBus.Supervisor.Nodes.Handlers
{
    public class Node : Aggregate<NodeState>
    {
        public void Add(string listenUrl)
        {
            Apply(new NodeAdded
                      {
                          NodeId = Id,
                          ListenUrl = listenUrl
                      });
        }

        public void Remove()
        {
            Apply(new NodeRemoved()
                      {
                          NodeId = Id
                      });
        }

        public void Assign(string service, string component, string assignmentId)
        {
            if (!State.Active)
            {
                return; //Do nothing
            }
            Apply(new ComponentAssigned
                      {
                          AssignmentId = assignmentId,
                          Component = component,
                          Service = service,
                          NodeId = Id
                      });
        }
        
        public void Unassign(string assignmentId)
        {
            if (!State.Active)
            {
                return; //Do nothing
            }
            var assignment = State.GetAssignment(assignmentId);
            if (assignment == null)
            {
                return; //No assignment
            }
            Apply(new ComponentUnassigned
                      {
                          AssignmentId = assignment.Id,
                          Component = assignment.Component,
                          Service = assignment.Service,
                          NodeId = Id
                      });
        }

        public void Deploy(string operationId, DeploymentSlot deploymentSlot, string service, string component, string version, INodeController nodeController)
        {
            if (!State.Active)
            {
                return; //Do nothing
            }
            Apply(new ComponentDeploymentRequested
            {
                OperationId = operationId,
                DeploymentId = deploymentSlot.Id,
                DeploymentUrl = deploymentSlot.Url,
                NodeId = Id,
                NodeUrl = State.ListenUrl,
                Service = service,
                Component = component,
                Version = version,
            });
        }

        public void Deploy(string operationId, string service, string component, string version, INodeController nodeController)
        {
            var deploymentSlot = nodeController.ClaimDeploymentSlot(State.ListenUrl);
            Deploy(operationId, deploymentSlot, service, component, version, nodeController);
        }

        public void OnDeploySucceeded(string deploymentId, string operationId, string wsManagementUrl, string webManagementUrl)
        {
            var deployment = State.Deployments.First(x => x.Id == deploymentId);
            Apply(ComponentDeployedBuilder.Build(Id, operationId, deployment, wsManagementUrl, webManagementUrl));
        }
        
        public void OnDeployFailed(string deploymentId, string operationId)
        {
            var deployment = State.Deployments.First(x => x.Id == deploymentId);
            Apply(ComponentDeploymentFailedBuilder.Build(Id, operationId, deployment));
        }

        public void Start(string operationId, string deploymentId)
        {
            if (!State.Active)
            {
                return;
            }
            var deployment = State.Deployments.FirstOrDefault(x => x.Id == deploymentId);
            if (deployment == null)
            {
                return; //Do nothing
            }
            if (deployment.State != DeploymentState.Stopped)
            {
                return; //Invalid state
            }
            Apply(ComponentStartRequestedBuilder.Build(Id, operationId, State, deployment));
        }

        public void OnStartSucceeded(string deploymentId, string operationId)
        {
            var deployment = State.Deployments.First(x => x.Id == deploymentId);
            Apply(ComponentStartedBuilder.Build(Id, operationId, deployment));
        }
        
        public void OnStartFailed(string deploymentId, string operationId)
        {
            var deployment = State.Deployments.First(x => x.Id == deploymentId);
            Apply(ComponentStartFailedBuilder.Build(Id, operationId, deployment));
        }

        public void Stop(string operationId, string deploymentId)
        {
            if (!State.Active)
            {
                return;
            }
            var deployment = State.Deployments.FirstOrDefault(x => x.Id == deploymentId);
            if (deployment == null)
            {
                return; //Do nothing
            }
            if (deployment.State != DeploymentState.Running)
            {
                return; //Invalid state
            }
            Apply(ComponentStopRequestedBuilder.Build(Id, operationId, State, deployment));
        }

        public void OnStopSucceeded(string deploymentId, string operationId)
        {
            var deployment = State.Deployments.First(x => x.Id == deploymentId);
            Apply(ComponentStoppedBuilder.Build(Id, operationId, deployment));
        }
        
        public void OnStopFailed(string deploymentId, string operationId)
        {
            var deployment = State.Deployments.First(x => x.Id == deploymentId);
            Apply(ComponentStopFailedBuilder.Build(Id, operationId, deployment));
        }

        public void Undeploy(string operationId, string deploymentId)
        {
            if (!State.Active)
            {
                return; //Do nothing
            }
            var deployment = State.Deployments.FirstOrDefault(x => x.Id == deploymentId);
            if (deployment == null)
            {
                return; //Do nothing
            }
            if (deployment.State != DeploymentState.Stopped)
            {
                return; //Invalid state
            }
            Apply(ComponentUndeploymentRequestedBuilder.Build(Id, operationId, State, deployment));
        }

        public void OnUndeploySucceeded(string deploymentId, string operationId)
        {
            var deployment = State.Deployments.First(x => x.Id == deploymentId);
            Apply(ComponentUndeployedBuilder.Build(Id, operationId, deployment));
        }
        
        public void OnUndeployFailed(string deploymentId, string operationId)
        {
            var deployment = State.Deployments.First(x => x.Id == deploymentId);
            Apply(ComponentUndeploymentFailedBuilder.Build(Id, operationId, deployment));
        }
    }
}
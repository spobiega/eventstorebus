﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentDeployedBuilder
    {
        public static ComponentDeployed Build(string nodeId, string operationId, Deployment deployment, string wsManagementUrl, string webManagementUrl)
        {
            return new ComponentDeployed()
                       {
                           OperationId = operationId,
                           Service = deployment.Service,
                           Component = deployment.Component,
                           Version = deployment.Version,
                           DeploymentId = deployment.Id,
                           NodeId = nodeId,
                           WSManagementUrl = wsManagementUrl,
                           WebManagementUrl = webManagementUrl
                       };
        }
    }
}
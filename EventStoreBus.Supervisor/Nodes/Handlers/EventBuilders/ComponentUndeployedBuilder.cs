﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentUndeployedBuilder
    {
        public static ComponentUndeployed Build(string nodeId, string operationId, Deployment deployment)
        {
            return new ComponentUndeployed()
                       {
                           OperationId = operationId,
                           Service = deployment.Service,
                           Component = deployment.Component,
                           Version = deployment.Version,
                           DeploymentId = deployment.Id,
                           NodeId = nodeId
                       };
        }
    }
}
﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentStoppedBuilder
    {
        public static ComponentStopped Build(string nodeId, string operationId, Deployment deployment)
        {
            return new ComponentStopped()
                       {
                           OperationId = operationId,
                           Component = deployment.Component,
                           DeploymentId = deployment.Id,
                           NodeId = nodeId,
                           Service = deployment.Service,
                           Version = deployment.Version
                       };
        }
    }
}
﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentDeploymentFailedBuilder
    {
        public static ComponentDeploymentFailed Build(string nodeId, string operationId, Deployment deployment)
        {
            return new ComponentDeploymentFailed()
                       {
                           OperationId = operationId,
                           Service = deployment.Service,
                           Component = deployment.Component,
                           Version = deployment.Version,
                           NodeId = nodeId,
                           DeploymentId = deployment.Id
                       };
        }
    }
}
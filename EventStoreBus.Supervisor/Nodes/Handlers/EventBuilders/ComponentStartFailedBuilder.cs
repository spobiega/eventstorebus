﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentStartFailedBuilder
    {
        public static ComponentStartFailed Build(string nodeId, string operationId, Deployment deployment)
        {
            return new ComponentStartFailed()
                       {
                           OperationId = operationId,
                           NodeId = nodeId,
                           DeploymentId = deployment.Id
                       };
        }
    }
}
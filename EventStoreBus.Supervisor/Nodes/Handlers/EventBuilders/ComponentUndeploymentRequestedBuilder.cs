﻿namespace EventStoreBus.Supervisor.Nodes.Handlers.EventBuilders
{
    public class ComponentUndeploymentRequestedBuilder
    {
        public static ComponentUndeploymentRequested Build(string nodeId, string operationId, NodeState nodeState, Deployment deployment)
        {
            return new ComponentUndeploymentRequested()
                       {
                           OperationId = operationId,
                           DeploymentId = deployment.Id,
                           NodeId = nodeId,
                           NodeUrl = nodeState.ListenUrl
                       };
        }
    }
}
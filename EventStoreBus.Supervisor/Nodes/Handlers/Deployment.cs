﻿namespace EventStoreBus.Supervisor.Nodes.Handlers
{
    public class Deployment
    {
        private readonly string id;
        private readonly string service;
        private readonly string component;
        private readonly string version;
        private readonly string deploymentOperationId;
        private readonly string url;
        private DeploymentState state;

        public Deployment(string id, string url, string deploymentOperationId, string service, string component, string version)
        {
            this.id = id;
            this.url = url;
            this.deploymentOperationId = deploymentOperationId;
            this.service = service;
            this.component = component;
            this.version = version;
            state = DeploymentState.DeployPending;
        }

        public void On(ComponentDeployed evnt)
        {
            state = DeploymentState.Stopped;
        } 
        
        public void On(ComponentStartRequested evnt)
        {
            state = DeploymentState.StartPending;
        }
        
        public void On(ComponentStarted evnt)
        {
            state = DeploymentState.Running;
        }
        
        public void On(ComponentStopRequested evnt)
        {
            state = DeploymentState.StopPending;
        }
        
        public void On(ComponentStopped evnt)
        {
            state = DeploymentState.Stopped;
        }

        public void On(ComponentUndeploymentRequested evnt)
        {
            state = DeploymentState.UndeployPending;
        }

        public string Id
        {
            get { return id; }
        }

        public string Service
        {
            get { return service; }
        }

        public string Component
        {
            get { return component; }
        }

        public string Version
        {
            get { return version; }
        }

        public string DeploymentOperationId
        {
            get { return deploymentOperationId; }
        }

        public DeploymentState State
        {
            get { return state; }
        }
    }
}
﻿using System;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Supervisor.Nodes.Gateway;
using EventStoreBus.Supervisor.Nodes.Handlers.Commands;

namespace EventStoreBus.Supervisor.Nodes.Handlers
{
    public class NodesHandler
    {
        private readonly INodeController nodeController;
        private readonly IRepository<Node> nodes;
        private readonly IUniqueIdGenerator uniqueIdGenerator;

        public NodesHandler(IUniqueIdGenerator uniqueIdGenerator, INodeController nodeController, IRepository<Node> nodes)
        {
            this.nodeController = nodeController;
            this.nodes = nodes;
            this.uniqueIdGenerator = uniqueIdGenerator;
        }

        public void When(HandleDeploymentSuccess command)
        {
            nodes.InvokeEnsuringExists(command.NodeId, command.Id,
                                       node => node.OnDeploySucceeded(command.DeploymentId, command.OperationId, command.WSManagementUrl, command.WebManagementUrl));
        }

        public void When(HandleOperationResult command)
        {
            nodes.InvokeEnsuringExists(command.NodeId, command.Id,
                                    node =>
                                        {
                                            switch (command.Operation)
                                            {
                                                case Operation.Deploy:
                                                    if (command.Success)
                                                    {
                                                        throw new NotSupportedException("Deployment success is communicated via separate message.");
                                                    }
                                                    node.OnDeployFailed(command.DeploymentId, command.OperationId);
                                                    break;
                                                case Operation.Undeploy:
                                                    if (command.Success)
                                                    {
                                                        node.OnUndeploySucceeded(command.DeploymentId, command.OperationId);
                                                    }
                                                    else
                                                    {
                                                        node.OnUndeployFailed(command.DeploymentId, command.OperationId);
                                                    }
                                                    break;
                                                case Operation.Start:
                                                    if (command.Success)
                                                    {
                                                        node.OnStartSucceeded(command.DeploymentId, command.OperationId);
                                                    }
                                                    else
                                                    {
                                                        node.OnStartFailed(command.DeploymentId, command.OperationId);
                                                    }
                                                    break;
                                                case Operation.Stop:
                                                    if (command.Success)
                                                    {
                                                        node.OnStopSucceeded(command.DeploymentId, command.OperationId);
                                                    }
                                                    else
                                                    {
                                                        node.OnStopFailed(command.DeploymentId, command.OperationId);
                                                    }
                                                    break;

                                            }
                                        });
        }

        public void When(AddNode command)
        {
            nodes.Invoke(command.NodeId, command.Id, 
                                    node => node.Add(command.ListenUrl));
        }

        public void When(RemoveNode command)
        {
            nodes.InvokeEnsuringExists(command.NodeId, command.Id,
                                     node => node.Remove());
        }

        public void When(AssignComponent command)
        {
            nodes.InvokeEnsuringExists(command.NodeId, command.Id,
                node => node.Assign(command.Service, command.Component, command.Id));
        }

        public void When(UnassignComponent command)
        {
            nodes.InvokeEnsuringExists(command.NodeId, command.Id,
                node => node.Unassign(command.AssignmentId));
        }

        public void When(DeployComponent command)
        {
            if (command.DeploymentId != null)
            {
                var slot = new DeploymentSlot(command.DeploymentId, command.DeploymentUrl);
                nodes.InvokeEnsuringExists(command.NodeId, command.Id,
                                         node => node.Deploy(command.Id, slot, command.Service, command.Component, command.Version, nodeController));    
            }
            else
            {
                nodes.InvokeEnsuringExists(command.NodeId, command.Id,
                                         node => node.Deploy(command.Id, command.Service, command.Component, command.Version, nodeController));    
            }            
        }

        public void When(UndeployComponent command)
        {
            nodes.InvokeEnsuringExists(command.NodeId, command.Id,
                                     node => node.Undeploy(command.Id, command.DeploymentId));
        }
        
        public void When(StartComponent command)
        {
            nodes.InvokeEnsuringExists(command.NodeId, command.Id,
                                     node => node.Start(command.Id, command.DeploymentId));
        }
        
        public void When(StopComponent command)
        {
            nodes.InvokeEnsuringExists(command.NodeId, command.Id,
                                     node => node.Stop(command.Id, command.DeploymentId));
        }

        public class Factory : IAggregateComponentFactory
        {
            public object Create(Type componentImplementation, IRepositories repositories, IFactories factories)
            {
                return new NodesHandler(new DefaultUniqueIdGenerator(), new NodeController(), repositories.Get<Node>());
            }

            public void Release(object component)
            {
            }
        }
    }
}
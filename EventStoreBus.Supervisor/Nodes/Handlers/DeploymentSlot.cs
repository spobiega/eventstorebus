﻿namespace EventStoreBus.Supervisor.Nodes.Handlers
{
    public class DeploymentSlot
    {
        private readonly string id;
        private readonly string url;

        public DeploymentSlot(string id, string url)
        {
            this.id = id;
            this.url = url;
        }

        public string Id
        {
            get { return id; }
        }

        public string Url
        {
            get { return url; }
        }
    }
}
﻿namespace EventStoreBus.Supervisor.Nodes.Handlers
{
    public class DeploymentConfirmation
    {
        private readonly string wsManagementUrl;
        private readonly string webManagementUrl;

        public DeploymentConfirmation(string wsManagementUrl, string webManagementUrl)
        {
            this.wsManagementUrl = wsManagementUrl;
            this.webManagementUrl = webManagementUrl;
        }

        public string WSManagementUrl
        {
            get { return wsManagementUrl; }
        }

        public string WebManagementUrl
        {
            get { return webManagementUrl; }
        }
    }
}
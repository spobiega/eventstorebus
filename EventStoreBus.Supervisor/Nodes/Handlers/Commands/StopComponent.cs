﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes.Handlers.Commands
{
    [DataContract]
    [Version(1)]
    public class StopComponent : Command
    {
        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }
    }
}
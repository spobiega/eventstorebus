﻿namespace EventStoreBus.Supervisor.Nodes
{
    public enum DeploymentState
    {
        DeployPending,
        Stopped,
        StartPending,
        Running,
        StopPending,
        UndeployPending
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Nodes
{
    [DataContract]
    [Version(1)]
    public class ComponentStopRequested : IEvent
    {
        [DataMember]
        public string OperationId { get; set; }

        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public string NodeUrl { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }
    }
}
using System.Collections.Generic;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.Nodes.Views
{
    public class NodeView : ConcurrentViewBase
    {
        public string NodeId { get; set; }
        public string Url { get; set; }
        public List<Deployment> Deployments { get; private set; }
        public List<Assignment> Assignments { get; private set; }

        public NodeView()
        {
            Deployments = new List<Deployment>();
            Assignments = new List<Assignment>();
        }        
    }
}
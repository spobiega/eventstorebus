﻿namespace EventStoreBus.Supervisor.Nodes.Views
{
    public class Assignment
    {
        public string AssignmentId { get; set; }
        public string Service { get; set; }
        public string Component { get; set; }
    }
}
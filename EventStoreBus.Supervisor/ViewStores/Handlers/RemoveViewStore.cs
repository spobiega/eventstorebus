﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.ViewStores.Handlers
{
    [DataContract]
    [Version(1)]
    public class RemoveViewStore : Command
    {
        [DataMember]
        public string StoreId { get; set; }
    }
}
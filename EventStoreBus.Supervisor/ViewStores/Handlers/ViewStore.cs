﻿using System.Collections.Generic;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.EventStores.Handlers;

namespace EventStoreBus.Supervisor.ViewStores.Handlers
{
    public class ViewStore : Aggregate<ViewStoreState>
    {
        public void Create(string connectionString)
        {
            Apply(new ViewStoreAdded()
                      {
                          ConnectionString = connectionString,
                          StoreId = Id
                      });
        }

        public void Remove()
        {
            Apply(new ViewStoreRemoved()
                      {
                          StoreId = Id
                      });
        }
    }
}
﻿using System;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.ViewStores.Handlers
{
    public class ViewStoresHandler
    {
        private readonly IRepository<ViewStore> stores;

        public ViewStoresHandler(IRepository<ViewStore> stores)
        {
            this.stores = stores;
        }

        public void When(AddViewStore command)
        {
            stores.Invoke(command.StoreId, command.Id, 
                                    node => node.Create(command.ConnectionString));
        }

        public void When(RemoveViewStore command)
        {
            stores.InvokeEnsuringExists(command.StoreId, command.Id,
                                     node => node.Remove());
        }

        public class Factory : IAggregateComponentFactory
        {
            public object Create(Type componentImplementation, IRepositories repositories, IFactories factories)
            {
                return new ViewStoresHandler(repositories.Get<ViewStore>());
            }

            public void Release(object component)
            {
            }
        }
    }
}
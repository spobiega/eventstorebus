﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.ViewStores
{
    [DataContract]
    [Version(1)]
    public class ViewStoreRemoved : IEvent
    {
        [DataMember]
        public string StoreId { get; set; }
    }
}
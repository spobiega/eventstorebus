﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Services
{
    [DataContract]
    [Version(1)]
    public class ServiceRegistered : IEvent
    {
        [DataMember]
        public string Name { get; set; }
    }
}
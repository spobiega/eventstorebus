﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Services
{
    [DataContract]
    [Version(1)]
    public class LockFailed : IEvent
    {
        [DataMember]
        public string LockId { get; set; }
    }
}
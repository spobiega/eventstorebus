﻿namespace EventStoreBus.Supervisor.Services.Views
{
    public class ComponentDeployment
    {
        public string NodeId { get; set; }
        public string DeploymentId { get; set; }
        public string Version { get; set; }
        public string Component { get; set; }
    }
}
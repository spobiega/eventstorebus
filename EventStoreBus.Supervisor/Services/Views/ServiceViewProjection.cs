﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.Services.Views
{
    public class ServiceViewProjection
    {
        private readonly IViewWriter<ServiceView> writer;

        public ServiceViewProjection(IViewWriter<ServiceView> writer)
        {
            this.writer = writer;
        }

        public void When(Envelope<ServiceRegistered> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Create(evnt.Payload.Name, evnt, new ServiceView()
                                                            {
                                                                Name = evnt.Payload.Name
                                                            });
            }
        }

        public void When(Envelope<ServiceVersionSet> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.ServiceName, evnt,
                               service => service.InstalledVersion = evnt.Payload.Version);
            }
        }

        public void When(Envelope<Locked> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.ServiceName, evnt,
                               service => service.LockId = evnt.Payload.LockId);
            }
        }
        
        public void When(Envelope<Unlocked> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.ServiceName, evnt,
                               service => service.LockId = null);
            }
        }

        public void When(Envelope<ServiceVersionAdded> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.ServiceName, evnt,
                               v => v.AvailableVersions.Add(evnt.Payload.Version));
            }
        }

        public void When(Envelope<ServiceComponentDeployed> evnt)
        {
            var deployment = new ComponentDeployment()
                                 {
                                     Component = evnt.Payload.Component,
                                     DeploymentId = evnt.Payload.DeploymentId,
                                     NodeId = evnt.Payload.NodeId,
                                     Version = evnt.Payload.Version
                                 };
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.Service, evnt,
                               v => v.DeployedComponents.Add(deployment));
            }
        }

        public void When(Envelope<ServiceComponentUndeployed> evnt)
        {
            using (var session = writer.OpenSession())
            {
                session.Update(evnt.Payload.Service, evnt,
                               v => v.DeployedComponents.RemoveAll(x => x.DeploymentId == evnt.Payload.DeploymentId));
            }
        }

        public class Storage : IViewStorage<ServiceView>
        {
            public string CollectionName
            {
                get { return "services"; }
            }
        }

        public class Factory : IViewProjectionFactory
        {
            public object Create(Type viewProjectionImplementation, IViewWriters viewWriters)
            {
                return new ServiceViewProjection(viewWriters.GetWriter<ServiceView, Storage>());
            }

            public void Release(object projection)
            {
            }
        }
    }
}
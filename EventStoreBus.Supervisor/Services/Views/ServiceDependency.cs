﻿namespace EventStoreBus.Supervisor.Services.Views
{
    public class ServiceDependency
    {
        public string Name { get; set; }
        public string VersionSpec { get; set; }
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Services.Handlers
{
    [DataContract]
    [Version(1)]
    public class RegisterService : Command
    {
        [DataMember]
        public string Name { get; set; }
    }
}
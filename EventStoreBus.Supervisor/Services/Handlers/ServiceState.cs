﻿using System.Collections.Generic;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.Services.Handlers
{
    public class ServiceState : AggregateState
    {
        public string LockId { get; private set; }
        private readonly List<ServiceComponentDeployment> componentDeployments = new List<ServiceComponentDeployment>();

        public IEnumerable<ServiceComponentDeployment> ComponentDeployments
        {
            get { return componentDeployments; }
        }

        public void When(ServiceComponentDeployed evnt)
        {
            var componentDeployment = new ServiceComponentDeployment(evnt.DeploymentId, evnt.NodeId, evnt.Component,
                                                                     evnt.Version);
            componentDeployments.Add(componentDeployment);
        }

        public void When(ServiceComponentUndeployed evnt)
        {
            componentDeployments.RemoveAll(x => x.Id == evnt.DeploymentId);
        }

        public void When(Locked evnt)
        {
            LockId = evnt.LockId;
        }

        public void When(Unlocked evnt)
        {
            LockId = null;
        }
    }
}
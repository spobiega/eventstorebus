﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Services.Handlers
{
    [DataContract]
    [Version(1)]
    public class UnregisterDeployment : Command
    {
        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        [DataMember]
        public string Service { get; set; }
    }
}
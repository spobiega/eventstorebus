﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.Services.Handlers
{
    [DataContract]
    [Version(1)]
    public class SetCurrentVersion : Command
    {
        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string Version { get; set; }
    }
}
﻿using System;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.EventStores.Handlers
{
    public class EventStoresHandler
    {
        private readonly IRepository<EventStore> stores;

        public EventStoresHandler(IRepository<EventStore> stores)
        {
            this.stores = stores;
        }

        public void When(AddEventStore command)
        {
            stores.Invoke(command.StoreId, command.Id, 
                                    store => store.Create(command.Address, command.Port));
        }

        public void When(CreateDatabase command)
        {
            stores.InvokeEnsuringExists(command.StoreId, command.Id,
                                        store => store.CreateDatabase(command.DatabaseName));
        }

        public void When(RemoveEventStore command)
        {
            stores.InvokeEnsuringExists(command.StoreId, command.Id,
                                        store => store.Remove());
        }

        public class Factory : IAggregateComponentFactory
        {
            public object Create(Type componentImplementation, IRepositories repositories, IFactories factories)
            {
                return new EventStoresHandler(repositories.Get<EventStore>());
            }

            public void Release(object component)
            {
            }
        }
    }
}
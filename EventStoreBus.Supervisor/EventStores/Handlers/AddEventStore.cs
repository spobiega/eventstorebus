﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.EventStores.Handlers
{
    [DataContract]
    [Version(1)]
    public class AddEventStore : Command
    {
        [DataMember]
        public string StoreId { get; set; }

        [DataMember]
        public string Address { get; set; }
        
        [DataMember]
        public int Port { get; set; }
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.EventStores.Handlers
{
    [DataContract]
    [Version(1)]
    public class CreateDatabase : Command
    {
        [DataMember]
        public string StoreId { get; set; }

        [DataMember]
        public string DatabaseName { get; set; }
    }
}
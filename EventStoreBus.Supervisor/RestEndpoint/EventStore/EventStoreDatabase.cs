﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.RestEndpoint.EventStore
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class EventStoreDatabase
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public HyperLink Events { get; set; }
    }
}
﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes;
using EventStoreBus.Supervisor.Nodes.Handlers;
using EventStoreBus.Supervisor.Nodes.Handlers.Commands;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus.Supervisor.RestEndpoint.Instance
{
    public class InstanceController : BaseController
    {
        private readonly IViewReader<NodeView> nodeReader;

        public InstanceController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender) 
            : base(baseUrl, commandSender)
        {
            nodeReader = viewStore.GetReader<NodeView, NodeViewProjection.Storage>();
        }

        public HttpResponseMessage Put(string nodeId, string deploymentId)
        {
            NodeView nodeView;
            using (var session = nodeReader.OpenSession())
            {
                nodeView = session.Load(nodeId);
            }
            if (nodeView == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var deployment = nodeView.Deployments.FirstOrDefault(x => x.Id == deploymentId);
            if (deployment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            if (deployment.State != DeploymentState.Stopped)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            CommandSender.SendTo<NodesHandler>(new StartComponent()
                                                   {
                                                       Id = Guid.NewGuid().ToString(),
                                                       DeploymentId = deploymentId,
                                                       NodeId = nodeId
                                                   });
            return Request.CreateResponse(HttpStatusCode.Accepted);
        }

        public HttpResponseMessage Get(string nodeId, string deploymentId)
        {
            NodeView nodeView;
            using (var session = nodeReader.OpenSession())
            {
                nodeView = session.Load(nodeId);
            }
            if (nodeView == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var deployment = nodeView.Deployments.FirstOrDefault(x => x.Id == deploymentId);
            if (deployment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var instance = new Instance(deployment.State, deploymentId, Url);
            return Request.CreateResponse(HttpStatusCode.OK, instance);
        }

        public HttpResponseMessage Delete(string nodeId, string deploymentId)
        {
            NodeView nodeView;
            using (var session = nodeReader.OpenSession())
            {
                nodeView = session.Load(nodeId);
            }
            if (nodeView == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var deployment = nodeView.Deployments.FirstOrDefault(x => x.Id == deploymentId);
            if (deployment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            if (deployment.State != DeploymentState.Running)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            CommandSender.SendTo<NodesHandler>(new StopComponent()
                                                   {
                                                       Id = Guid.NewGuid().ToString(),
                                                       DeploymentId = deploymentId,
                                                       NodeId = nodeId
                                                   });
            return Request.CreateResponse(HttpStatusCode.Accepted);
        }
    }
}
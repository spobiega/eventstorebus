﻿using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Supervisor.Nodes;
using EventStoreBus.Supervisor.RestEndpoint.Deployment;

namespace EventStoreBus.Supervisor.RestEndpoint.Instance
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Instance
    {
        [DataMember]
        public DeploymentState State { get; set; }
        [DataMember]
        public HyperLink Deployment { get; set; }
        [DataMember]
        public HyperLink Start { get; set; }
        [DataMember]
        public HyperLink Stop { get; set; }
        [DataMember]
        public HyperLink Manage { get; set; }
        
        public Instance(DeploymentState state, string deploymentId, UrlHelper url)
        {
            State = state;
            var routeValues = new {id = deploymentId};
            Deployment = url.LinkToGet<DeploymentController>("parent", routeValues);
            if (State == DeploymentState.Running)
            {
                Stop = url.LinkToDelete<InstanceController>("stop", routeValues);
            }
            else
            {
                Start = url.LinkToPut<InstanceController>("start", routeValues);
            }
            Manage = new HyperLink("manage", string.Format("http://localhost:12345/hosts/{0}/rest", deploymentId), HttpMethod.Get);
        }
    }
}
using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.RestEndpoint.Assignment;
using EventStoreBus.Supervisor.RestEndpoint.Deployment;
using EventStoreBus.Supervisor.RestEndpoint.Environment;
using EventStoreBus.Supervisor.RestEndpoint.EventStore;
using EventStoreBus.Supervisor.RestEndpoint.Instance;
using EventStoreBus.Supervisor.RestEndpoint.Node;
using EventStoreBus.Supervisor.RestEndpoint.Service;
using EventStoreBus.Supervisor.RestEndpoint.ViewStore;
using EventStoreBus.Views.Api;
using EventStoreBus.Web.Api;
using Raven.Client;

namespace EventStoreBus.Supervisor.RestEndpoint
{
    public class WebFactory : IWebFactory
    {
        public IHttpController Create(Type controllerType, string endpointUrl, IViewReaders viewStoreReaders, ICommandSender commandSender)
        {
            return (IHttpController)Activator.CreateInstance(controllerType, new Uri(endpointUrl), viewStoreReaders, commandSender);
        }

        public void Release(IHttpController instance)
        {
        }

        public void Configure(HttpConfiguration config)
        {
            var routes = config.Routes;
            routes.MapHttpRoute<InstanceController>("nodes/{nodeId}/deployments/{deploymentId}/instance", HttpMethod.Get, HttpMethod.Put, HttpMethod.Delete)
                .MapHttpRoute<DeployController>("nodes/{nodeId}/deploy/{operationId}", HttpMethod.Get, HttpMethod.Put)
                .MapHttpRoute<DeploymentController>("nodes/{nodeId}/deployments/{deploymentId}",  HttpMethod.Get, HttpMethod.Delete)
                .MapHttpRoute<AssignmentController>("nodes/{nodeId}/assignments/{assignmentId}", HttpMethod.Delete)
                .MapHttpRoute<AssignmentController>("nodes/{nodeId}/assign/{operationId}", HttpMethod.Put, HttpMethod.Get)
                .MapHttpRoute<NodeController>("nodes/add/{operationId}", HttpMethod.Put)
                .MapHttpRoute<NodeController>("nodes/{nodeId}", HttpMethod.Get)
                .MapHttpRoute<NodesController>("nodes", HttpMethod.Get)
                .MapHttpRoute<EventStoreController>("eventStores/add/{operationId}", HttpMethod.Put)
                .MapHttpRoute<EventStoreDatabaseController>("eventStores/{name}/createDatabase/{operationId}", HttpMethod.Put)
                .MapHttpRoute<EventStoreController>("eventStores/{name}", HttpMethod.Get)
                .MapHttpRoute<EventStoresController>("eventStores", HttpMethod.Get)
                .MapHttpRoute<ViewStoreController>("viewStores/add/{operationId}", HttpMethod.Put)
                .MapHttpRoute<ViewStoreController>("viewStores/{name}", HttpMethod.Get)
                .MapHttpRoute<ViewStoresController>("viewStores", HttpMethod.Get)
                .MapHttpRoute<ServiceController>("services/add/{operationId}", HttpMethod.Put)
                .MapHttpRoute<RedeployController>("services/{name}/redeploy/{operationId}", HttpMethod.Put)
                .MapHttpRoute<UpgradeController>("services/{name}/upgrade/{operationId}", HttpMethod.Put)
                .MapHttpRoute<VersionController>("services/{name}/setver/{operationId}", HttpMethod.Put)
                .MapHttpRoute<ServiceController>("services/{name}", HttpMethod.Get)
                .MapHttpRoute<ServicesController>("services", HttpMethod.Get)
                .MapHttpRoute<EnvironmentController>("", HttpMethod.Get);
        }
    }
}
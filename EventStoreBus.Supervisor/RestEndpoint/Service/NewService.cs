﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.RestEndpoint.Service
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NewService
    {
        [DataMember]
        public string Name { get; set; }
    }
}
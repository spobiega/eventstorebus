﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Supervisor.RestEndpoint.Environment;
using EventStoreBus.Supervisor.Services.Views;

namespace EventStoreBus.Supervisor.RestEndpoint.Service
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class ServiceRepository
    {
        [DataMember]
        public List<ServiceReference> Services { get; set; }

        [DataMember]
        public HyperLink Register { get; set; }

        [DataMember]
        public HyperLink Parent { get; set; }

        public ServiceRepository(IEnumerable< ServiceView> views, UrlHelper url)
        {
            Services = views.Select(x => new ServiceReference(x, url)).ToList();
            Register = url.LinkToPut<ServiceController>("register", new {operationId = Guid.NewGuid().ToString()});
            Parent = url.LinkToGet<EnvironmentController>("parent", new { });
        }
    }
}
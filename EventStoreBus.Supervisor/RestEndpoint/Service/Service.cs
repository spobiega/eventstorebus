﻿using System;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Web.Api;
using EventStoreBus.Supervisor.Services.Views;

namespace EventStoreBus.Supervisor.RestEndpoint.Service
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Service
    {
        public Service(ServiceView view, UrlHelper url)
        {
            Name = view.Name;
            CurrentVersion = view.InstalledVersion;
            Redeploy = url.LinkToPut<RedeployController>("redeploy", GetRouteValues(view));
            Upgrade = url.LinkToPut<UpgradeController>("upgrade", GetRouteValues(view));
            SetVersion = url.LinkToPut<VersionController>("setVersion", GetRouteValues(view));
            Parent = url.LinkToGet<ServicesController>("parent", new {});
        }

        private static object GetRouteValues(ServiceView view)
        {
            return new
                       {
                           operationId = Guid.NewGuid().ToString(),
                           name = view.Name
                       };
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string CurrentVersion { get; set; }
        
        [DataMember]
        public HyperLink Parent { get; set; }

        [DataMember]
        public HyperLink SetVersion { get; set; }

        [DataMember]
        public HyperLink Redeploy { get; set; }

        [DataMember]
        public HyperLink Upgrade { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Services.Views;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Service
{
    public class ServicesController : BaseController
    {
        private readonly IViewReader<ServiceView> serviceReader;

        public ServicesController(Uri baseUrl, IViewReaders viewReaders, ICommandSender commandSender) 
            : base(baseUrl, commandSender)
        {
            serviceReader = viewReaders.GetReader<ServiceView, ServiceViewProjection.Storage>();
        }

        public ServiceRepository Get()
        {
            using (var session = serviceReader.OpenSession())
            {
                return new ServiceRepository(session.Query(), Url);
            }
        }
    }
}
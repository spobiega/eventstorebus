﻿using System;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Supervisor.RestEndpoint.Instance;
using EventStoreBus.Supervisor.RestEndpoint.Node;

namespace EventStoreBus.Supervisor.RestEndpoint.Deployment
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Deployment
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Version { get; set; }
        [DataMember]
        public string Component { get; set; }
        [DataMember]
        public HyperLink Start { get; set; }
        [DataMember]
        public HyperLink Stop { get; set; }
        [DataMember]
        public HyperLink Status { get; set; }
        [DataMember]
        public HyperLink Undeploy { get; set; }
        [DataMember]
        public HyperLink Parent { get; set; }
        [DataMember]
        public HyperLink Self { get; set; }

        public Deployment()
        {
        }

        public Deployment(Nodes.Views.Deployment deployment, string nodeId, UrlHelper url)
        {
            Component = deployment.Component;
            Id = deployment.Id;
            Service = deployment.Service;
            Version = deployment.Version;

            Self = url.LinkTo<DeploymentController>("self", HttpMethod.Get, GetRouteValues(deployment, nodeId));
            Start = url.LinkTo<InstanceController>("start", HttpMethod.Put, GetRouteValues(deployment, nodeId));
            Stop = url.LinkTo<InstanceController>("stop", HttpMethod.Delete, GetRouteValues(deployment, nodeId));
            Status = url.LinkTo<InstanceController>("status", HttpMethod.Get, GetRouteValues(deployment, nodeId));
            Parent = url.LinkTo<NodeController>("parent", HttpMethod.Get, GetRouteValues(deployment, nodeId));
            Undeploy = url.LinkTo<DeploymentController>("undeploy", HttpMethod.Delete, GetRouteValues(deployment, nodeId));
        }

        private static object GetRouteValues(Nodes.Views.Deployment deployment, string nodeId)
        {
            return new
                       {
                           deploymentId = deployment.Id,
                           operationId = Guid.NewGuid().ToString(),
                           nodeId = nodeId
                       };
        }
    }
}
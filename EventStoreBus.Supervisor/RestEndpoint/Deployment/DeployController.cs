﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes.Handlers;
using EventStoreBus.Supervisor.Nodes.Handlers.Commands;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Views.Api;
using EventStoreBus.Web.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Deployment
{
    public class DeployController : BaseController
    {
        private readonly IViewReader<NodeView> nodeReader;

        public DeployController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
            nodeReader = viewStore.GetReader<NodeView, NodeViewProjection.Storage>();
        }

        public HttpResponseMessage Put(NewDeployment newDeployment)
        {
            var operationId = (string)ControllerContext.RouteData.Values["operationId"];
            var nodeId = (string)ControllerContext.RouteData.Values["nodeId"];

            CommandSender.SendTo<NodesHandler>(new DeployComponent
                                                   {
                                                       Id = operationId,
                                                       Service = newDeployment.Service,
                                                       Component = newDeployment.Component,
                                                       Version = newDeployment.Version,
                                                       NodeId = nodeId
                                                   });

            var response = Request.CreateResponse(HttpStatusCode.Accepted);
            response.Headers.Location = new Uri(BaseUrl, Url.Route<DeploymentController>(HttpMethod.Get, new
                                                                        {
                                                                            nodeId,
                                                                            operationId
                                                                        }));
            return response;
        }

        public HttpResponseMessage Get(string nodeId, string operationId)
        {
            NodeView nodeView;
            using (var session = nodeReader.OpenSession())
            {
                nodeView = session.Load(nodeId);
            }
            if (nodeView == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var deployment = nodeView.Deployments.FirstOrDefault(x => x.DeploymentOperationId == operationId);
            if (deployment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var response = Request.CreateResponse(HttpStatusCode.SeeOther);
            response.Headers.Location = new Uri(BaseUrl, Url.Route<DeploymentController>(HttpMethod.Get, new
                                                                                     {
                                                                                         nodeId,
                                                                                         deploymentId = deployment.Id
                                                                                     }));
            return response; 
        }
    }
}
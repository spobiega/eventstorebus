﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes.Handlers;
using EventStoreBus.Supervisor.Nodes.Handlers.Commands;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Views.Api;
using EventStoreBus.Web.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Deployment
{
    public class DeploymentController : BaseController
    {
        private readonly IViewReader<NodeView> nodeReader;

        public DeploymentController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender) 
            : base(baseUrl, commandSender)
        {
            nodeReader = viewStore.GetReader<NodeView, NodeViewProjection.Storage>();
        }

        public HttpResponseMessage Get(string nodeId, string deploymentId)
        {
            NodeView nodeView;
            using (var session = nodeReader.OpenSession())
            {
                nodeView = session.Load(nodeId);
            }
            if (nodeView == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var deployment = nodeView.Deployments.FirstOrDefault(x => x.Id == deploymentId);
            if (deployment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var result = new Deployment(deployment, nodeId, Url);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        public HttpResponseMessage Delete(string nodeId, string deploymentId)
        {
            NodeView nodeView;
            using (var session = nodeReader.OpenSession())
            {
                nodeView = session.Load(nodeId);
            }
            if (nodeView == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            CommandSender.SendTo<NodesHandler>(new UndeployComponent()
                                                   {
                                                       Id = Guid.NewGuid().ToString(),
                                                       DeploymentId = deploymentId,
                                                       NodeId = nodeId
                                                   });
            var response = Request.CreateResponse(HttpStatusCode.Accepted);
            response.Headers.Location = new Uri(BaseUrl, Url.Route<NodeController>(HttpMethod.Get, new {nodeId}));
            return response;
        }
    }
}
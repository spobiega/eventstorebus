﻿using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Config;

namespace EventStoreBus.Supervisor.RestEndpoint.ViewStore
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class ViewStore
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ConnectionString { get; set; }
        [DataMember]
        public HyperLink Remove { get; set; }

        public ViewStore(ViewStoreNode node, UrlHelper url)
        {
            Name = node.Name;
            ConnectionString = node.ConnectionString;
        }
    }
}
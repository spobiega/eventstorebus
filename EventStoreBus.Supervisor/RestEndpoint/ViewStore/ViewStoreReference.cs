﻿using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Config;

namespace EventStoreBus.Supervisor.RestEndpoint.ViewStore
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class ViewStoreReference
    {
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public HyperLink Details { get; set; }

        public ViewStoreReference(ViewStoreNode node, UrlHelper url)
        {
            Name = node.Name;
            Details = url.LinkToGet<ViewStoreController>("details", new {name = node.Name});
        }
    }
}
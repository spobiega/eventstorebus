﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.RestEndpoint.ViewStore
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NewViewStore
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ConnectionString { get; set; }
    }
}
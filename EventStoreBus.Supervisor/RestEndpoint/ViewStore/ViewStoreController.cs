using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Environment;
using EventStoreBus.Supervisor.EventStores.Handlers;
using EventStoreBus.Supervisor.RestEndpoint.EventStore;
using EventStoreBus.Supervisor.ViewStores.Handlers;
using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus.Supervisor.RestEndpoint.ViewStore
{
    public class ViewStoreController : BaseController
    {
        private readonly IViewReader<EnvironmentView> envReader;

        public ViewStoreController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
            envReader = viewStore.GetReader<EnvironmentView, EnvironmentViewProjection.Storage>();
        }

        public HttpResponseMessage Get(string name)
        {
            EnvironmentView view;
            using (var session = envReader.OpenSession())
            {
                view = session.LoadSingle();
            }
            var store = view.Data.ViewStores.FirstOrDefault(x => x.Name == name);
            if (store == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, new ViewStore(store, Url));
        }

        public HttpResponseMessage Put(NewViewStore store)
        {
            var operationId = (string)ControllerContext.RouteData.Values["operationId"];
            EnvironmentView view;
            using (var session = envReader.OpenSession())
            {
                view = session.LoadSingle();
            }
            var storeWithSameName = view.Data.ViewStores.FirstOrDefault(x => x.Name == store.Name);
            if (storeWithSameName != null)
            {
                if (storeWithSameName.ConnectionString != store.ConnectionString)
                {
                    return Request.CreateResponse(HttpStatusCode.Conflict);
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            CommandSender.SendTo<ViewStoresHandler>(new AddViewStore()
                                                         {
                                                             StoreId = store.Name,
                                                             ConnectionString = store.ConnectionString,
                                                             Id = operationId
                                                         });
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
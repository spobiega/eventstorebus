﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Supervisor.RestEndpoint.Environment;

namespace EventStoreBus.Supervisor.RestEndpoint.Node
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NodeCollection
    {
        [DataMember]
        public List<NodeReference> Nodes { get; set; }

        [DataMember]
        public HyperLink Register { get; set; }
        
        [DataMember]
        public HyperLink Parent { get; set; }

        public NodeCollection(IEnumerable< NodeView> views, UrlHelper url)
        {
            Nodes = views.Select(x => new NodeReference(x, url)).ToList();
            Register = url.LinkToPut<NodeController>("register", new {operationId = Guid.NewGuid().ToString()});
            Parent = url.LinkToGet<EnvironmentController>("parent", new {});
        }
    }
}
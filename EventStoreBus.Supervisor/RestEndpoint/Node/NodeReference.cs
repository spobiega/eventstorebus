﻿using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Supervisor.Nodes.Views;

namespace EventStoreBus.Supervisor.RestEndpoint.Node
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NodeReference
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public HyperLink Details { get; set; }

        public NodeReference(NodeView view, UrlHelper url)
        {
            Id = view.NodeId;
            Details = url.LinkToGet<NodeController>("details", new {nodeId = view.NodeId});
        }
    }
}
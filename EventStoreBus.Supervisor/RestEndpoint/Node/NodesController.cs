﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Node
{
    public class NodesController : BaseController
    {
        private readonly IViewReader<NodeView> nodeReader;

        public NodesController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
            nodeReader = viewStore.GetReader<NodeView, NodeViewProjection.Storage>();
        }

        public NodeCollection Get()
        {
            using (var session = nodeReader.OpenSession())
            {
                return new NodeCollection(session.Query(), Url);
            }
        }
    }
}
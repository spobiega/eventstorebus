﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http.Routing;
using EventStoreBus.Supervisor.RestEndpoint.EventStore;
using EventStoreBus.Supervisor.RestEndpoint.Node;
using EventStoreBus.Supervisor.RestEndpoint.Service;
using EventStoreBus.Supervisor.RestEndpoint.ViewStore;

namespace EventStoreBus.Supervisor.RestEndpoint.Environment
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Environment
    {
        public Environment(UrlHelper url)
        {
            Nodes = url.LinkToGet<NodesController>("nodes", new {});
            EventStores = url.LinkToGet<EventStoresController>("eventStores", new {});
            ViewStores = url.LinkToGet<ViewStoresController>("viewStores", new {});
            Services = url.LinkToGet<ServicesController>("services", new {});
            Manage = new HyperLink("manage", "http://localhost:12345/1/rest", HttpMethod.Get);
        }

        [DataMember]
        public HyperLink EventStores { get; set; }
        
        [DataMember]
        public HyperLink ViewStores { get; set; }

        [DataMember]
        public HyperLink Nodes { get; set; }

        [DataMember]
        public HyperLink Services { get; set; }
        
        [DataMember]
        public HyperLink Manage { get; set; }
    }
}
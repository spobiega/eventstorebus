﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes.Handlers;
using EventStoreBus.Supervisor.Nodes.Handlers.Commands;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Views.Api;
using EventStoreBus.Web.Api;

namespace EventStoreBus.Supervisor.RestEndpoint.Assignment
{
    public class AssignmentController : BaseController
    {
        private readonly IViewReader<NodeView> nodeReader;

        public AssignmentController(Uri baseUrl, IViewReaders viewStore, ICommandSender commandSender)
            : base(baseUrl, commandSender)
        {
            nodeReader = viewStore.GetReader<NodeView, NodeViewProjection.Storage>();
        }

        public HttpResponseMessage Put(NewAssignment newAssignment)
        {
            var operationId = (string)ControllerContext.RouteData.Values["operationId"];
            var nodeId = (string)ControllerContext.RouteData.Values["nodeId"];

            CommandSender.SendTo<NodesHandler>(new AssignComponent()
                                                   {
                                                       Id = operationId,
                                                       Service = newAssignment.Service,
                                                       Component = newAssignment.Component,
                                                       NodeId = nodeId
                                                   });

            var response = Request.CreateResponse(HttpStatusCode.Accepted);
            response.Headers.Location = new Uri(BaseUrl, Url.Route<NodeController>(HttpMethod.Get, new
                                                                        {
                                                                            nodeId,
                                                                        }));
            response.Content = new StringContent(operationId);
            return response;
        }

        public HttpResponseMessage Get(string nodeId, string operationId)
        {
            NodeView nodeView;
            using (var session = nodeReader.OpenSession())
            {
                nodeView = session.Load(nodeId);
            }
            if (nodeView == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var deployment = nodeView.Assignments.FirstOrDefault(x => x.AssignmentId == operationId);
            if (deployment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            var response = Request.CreateResponse(HttpStatusCode.SeeOther);
            response.Headers.Location = new Uri(BaseUrl, Url.Route<NodeController>(HttpMethod.Get, new
                                                                                                       {
                                                                                                           nodeId,
                                                                                                       }));
            return response;
        }
        
        public HttpResponseMessage Delete(string nodeId, string assignmentId)
        {
            CommandSender.SendTo<NodesHandler>(new UnassignComponent()
                                                   {
                                                       Id = Guid.NewGuid().ToString(),
                                                       AssignmentId = assignmentId,
                                                       NodeId = nodeId
                                                   });

            var response = Request.CreateResponse(HttpStatusCode.Accepted);
            response.Headers.Location = new Uri(BaseUrl, Url.Route<NodeController>(HttpMethod.Get, new
                                                                        {
                                                                            nodeId,
                                                                        }));
            return response;
        }

    }
}
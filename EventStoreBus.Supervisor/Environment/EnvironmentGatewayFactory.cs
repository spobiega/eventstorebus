﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.Nodes.Handlers;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.Environment
{
    public class EnvironmentGatewayFactory : IGatewayFactory
    {
        public object Create(Type gatewayImplementation, ICommandSender commandSender, IViewReaders documentStore)
        {
            return new EnvironmentGateway(new NodeController(),
                                          documentStore.GetReader<EnvironmentView, EnvironmentViewProjection.Storage>(),
                                          documentStore.GetReader<NodeView, NodeViewProjection.Storage>());
        }

        public void Release(object projection)
        {
        }
    }
}
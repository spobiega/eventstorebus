﻿using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.Environment
{
    public class EnvironmentView : ConcurrentViewBase
    {
        public Config.Environment Data { get; set; }

        public EnvironmentView()
        {
            Data = new Config.Environment();
        }
    }
}
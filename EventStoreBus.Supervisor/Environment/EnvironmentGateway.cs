using System;
using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Supervisor.EventStores;
using EventStoreBus.Supervisor.Nodes;
using EventStoreBus.Supervisor.Nodes.Handlers;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Supervisor.ViewStores;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.Environment
{
    public class EnvironmentGateway
    {
        private const string ViewId = "single";
        private readonly IViewReader<EnvironmentView> envViewReader;
        private readonly IViewReader<NodeView> nodeViewReader;
        private readonly NodeController nodeController;

        public EnvironmentGateway(NodeController nodeController, 
            IViewReader<EnvironmentView> envViewReader, 
            IViewReader<NodeView> nodeViewReader)
        {
            this.nodeController = nodeController;
            this.nodeViewReader = nodeViewReader;
            this.envViewReader = envViewReader;
        }

        public void When(EventStoreAdded evnt, Guid eventId)
        {
            UpdateEnvironment();
        }

        public void When(EventStoreRemoved evnt, Guid eventId)
        {
            UpdateEnvironment();
        }
        
        public void When(ViewStoreAdded evnt, Guid eventId)
        {
            UpdateEnvironment();
        }
        
        public void When(ViewStoreRemoved evnt, Guid eventId)
        {
            UpdateEnvironment();
        }
        
        public void When(ComponentDeployed evnt, Guid eventId)
        {
            UpdateEnvironment();
        }
        
        public void When(ComponentUndeployed evnt, Guid eventId)
        {
            UpdateEnvironment();
        }

        private void UpdateEnvironment()
        {
            var environment = LoadEnvironment();
            var nodes = LoadNodes();
            foreach (var node in nodes)
            {
                UpdateEnvironment(node, environment);
            }
        }

        private IEnumerable<NodeView> LoadNodes()
        {
            List<NodeView> nodes;
            using (var nodeSession = nodeViewReader.OpenSession())
            {
                nodes = nodeSession.Query().ToList();
            }
            return nodes;
        }

        private EnvironmentView LoadEnvironment()
        {
            EnvironmentView environment;
            using (var environmentSession = envViewReader.OpenSession())
            {
                environment = environmentSession.Load(ViewId);
            }
            return environment;
        }

        private void UpdateEnvironment(NodeView node, EnvironmentView environment)
        {
            nodeController.UpdateEnvironment(node.Url, environment.Data);
        }
    }
}
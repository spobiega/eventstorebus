﻿using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Supervisor.Services.Views;
using EventStoreBus.Views.Api;
using PlannedDeployment = EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands.PlannedDeployment;
using PlannedUndeployment = EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands.PlannedUndeployment;

namespace EventStoreBus.Supervisor.DeploymentProcesses
{
    public class DeploymentPlanService : IDeploymentPlanService
    {
        private readonly IViewReader<NodeView> nodeReader;
        private readonly IViewReader<ServiceView> serviceReader;

        public DeploymentPlanService(IViewReader<NodeView> nodeReader, IViewReader<ServiceView> serviceReader)
        {
            this.nodeReader = nodeReader;
            this.serviceReader = serviceReader;
        }

        public DeploymentPlan BuildRedeploymentPlan(string service, string lockId)
        {
            ServiceView serviceView;
            using (var serviceSession = serviceReader.OpenSession())
            {
                serviceView = serviceSession.Load(service);
            }
            //if (serviceView.LockId != lockId)
            //{
            //    return null; //Maybe throw?
            //}
            var plan = new DeploymentPlan();
            plan.ServiceVersion = serviceView.InstalledVersion;

            List<NodeView> nodes;
            using (var nodeSession = nodeReader.OpenSession())
            {
                nodes = nodeSession.Query().ToList();
            }
            foreach (var node in nodes)
            {
                var assignmensByComponent = node.Assignments
                    .Where(x => x.Service == service)
                    .GroupBy(x => x.Component);
                foreach (var group in assignmensByComponent)
                {
                    PlanComponentDeployment(service, plan, node, group);
                }
            }
            return plan;
        }

        private static void PlanComponentDeployment(string service, DeploymentPlan plan, NodeView node, IGrouping<string, Assignment> group)
        {
            var deployments = node.Deployments
                .Where(x => x.Service == service && x.Component == group.Key)
                .ToList();
            var assignmentCount = group.Count();
            var delta = assignmentCount - deployments.Count;
            if (delta > 0)
            {
                Deploy(plan, node, group.Key, delta);
            }
            else if (delta < 0)
            {
                Undeploy(plan, node, deployments.Take(-delta));
            }
        }

        private static void Undeploy(DeploymentPlan plan, NodeView node, IEnumerable<Nodes.Views.Deployment> toUndeploy)
        {
            plan.ToUndeploy.AddRange(
                toUndeploy.Select(x => new PlannedUndeployment
                                           {
                                               Component = x.Component,
                                               ExistingDeploymentId = x.Id,
                                               NodeId = node.NodeId
                                           }));
        }

        private static void Deploy(DeploymentPlan plan, NodeView node, string component, int instanceCount)
        {
            plan.ToDeploy.AddRange(
                Enumerable.Range(0, instanceCount).Select(x => new PlannedDeployment
                                                                   {
                                                                       Component = component,
                                                                       NodeId = node.NodeId
                                                                   }));
        }
    }
}
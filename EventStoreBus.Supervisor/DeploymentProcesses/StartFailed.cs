﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses
{
    [Version(1)]
    [DataContract]
    public class StartFailed : IEvent
    {
        
    }
}
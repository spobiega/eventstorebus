﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers
{
    public class PlannedUndeployment
    {
        private readonly string nodeId;
        private readonly string component;
        private readonly string existingDeploymentId;

        public PlannedUndeployment(string nodeId, string component, string existingDeploymentId)
        {
            this.nodeId = nodeId;
            this.existingDeploymentId = existingDeploymentId;
            this.component = component;
        }

        public string NodeId
        {
            get { return nodeId; }
        }

        public string Component
        {
            get { return component; }
        }

        public string ExistingDeploymentId
        {
            get { return existingDeploymentId; }
        }
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Manage.Events
{
    [DataContract]
    [Version(1)]
    public class ManagementSequenceSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }
        [DataMember]
        public string ParentProcessId { get; set; }

        public ManagementSequenceSucceeded(string processId, string parentProcessId)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
        }

        public ManagementSequenceSucceeded()
        {
        }
    }
}
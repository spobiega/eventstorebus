﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start
{
    [DataContract]
    [Version(1)]
    public class StartSequenceFailed : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<string> FailedRequests { get; set; }

        public StartSequenceFailed(string processId, string parentProcessId, IEnumerable<string> failedRequests)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
            FailedRequests = failedRequests.ToList();
        }

        public StartSequenceFailed()
        {
            FailedRequests = new List<string>();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start.Commands
{
    [DataContract]
    [Version(1)]
    public class BeginStartSequence : Command
    {
        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<StartRequest> Requests { get; set; }

        public BeginStartSequence(string commandId, string parentProcessId, IEnumerable<StartRequest> requests)
            : base(commandId)
        {
            ParentProcessId = parentProcessId;
            Requests = requests.ToList();
        }

        public BeginStartSequence()
        {
        } 
    }
}
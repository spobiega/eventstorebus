﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start
{
    public class StartRequest
    {
        private readonly string nodeId;
        private readonly string deploymentId;

        public StartRequest(string nodeId, string deploymentId)
        {
            this.nodeId = nodeId;
            this.deploymentId = deploymentId;
        }

        public string NodeId
        {
            get { return nodeId; }
        }

        public string DeploymentId
        {
            get { return deploymentId; }
        }
    }
}
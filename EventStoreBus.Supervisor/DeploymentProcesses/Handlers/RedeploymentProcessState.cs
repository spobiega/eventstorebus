﻿using System.Collections.Generic;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers
{
    public class RedeploymentProcessState : AggregateState
    {
        public string LockId { get; private set; }
        public string Service { get; private set; }
        public string Version { get; private set; }
        public int PlanAttempts { get; private set; }

        private readonly List<PlannedDeployment> plannedDeployments = new List<PlannedDeployment>();
        private int deploymentIndex;
        public PlannedDeployment CurrentDeployment
        {
            get { return plannedDeployments[deploymentIndex]; }
        }
        public bool NoMoreDeployments
        {
            get { return deploymentIndex == plannedDeployments.Count; }
        }

        private readonly List<PlannedUndeployment> plannedUndeployments = new List<PlannedUndeployment>();
        private int undeploymentIndex;
        public PlannedUndeployment CurrentUndeployment
        {
            get { return plannedUndeployments[undeploymentIndex]; }
        }
        public bool NoMoreUndeployments
        {
            get { return undeploymentIndex == plannedUndeployments.Count; }
        }

        public void When(Created evnt)
        {
            Service = evnt.Service;
            Version = evnt.RequestedVersion;
        }

        public void When(LockRequested evnt)
        {
            LockId = evnt.LockId;
        }

        public void When(VersionDetermined evnt)
        {
            Version = evnt.Version;
        }

        public void When(DeploymentPlanRequested evnt)
        {
            PlanAttempts++;
        }

        public void When(DeploymentPlanned evnt)
        {
            plannedDeployments.Add(new PlannedDeployment(evnt.NodeId, evnt.Component));
        }
        
        public void When(UndeploymentPlanned evnt)
        {
            plannedUndeployments.Add(new PlannedUndeployment(evnt.NodeId, evnt.Component, evnt.ExistingDeploymentId));
        }

        public void When(DeploymentSucceeded evnt)
        {
            CurrentDeployment.On(evnt);
        }

        public void When(StartSucceeded evnt)
        {
            deploymentIndex++;
        }

        public void When(UndeploymentSucceeded evnt)
        {
            undeploymentIndex++;
        }
    }
}
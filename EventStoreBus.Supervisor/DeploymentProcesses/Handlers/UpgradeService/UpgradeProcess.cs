﻿using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class UpgradeProcess : Aggregate<UpgradeProcessState>
    {
        public void Start(string service, string targetVersion)
        {
            Apply(new ServiceUpgradeStarted(Id, service, targetVersion));
            /*
             * Deploy new event processors side by side                         - Undeploy
             * Disable gateways in all new event processors                     -
             * Start new event processors                                       - Stop
             * Wait till they are finished (up to date) (poll)                  -
             * Deploy all other (non-processors) components side by side        - Undeploy
             * Stop old event processors                                        - Start
             * Stop old receptors                                               - Start
             * Switch endpoints (in load balancer)                              - Switch back       - some new commands
             * Wait for old command processors to process all commands (poll)   - 
             * Stop old command handlers                                        - Start  
             * Enable gateways in new event processors                          -             * - 
             * Start new receptors                                              - Stop              - some new commands
             * Start new command processors                                     - Stop              - some new events       - no automatic rollback
             * Undeploy all old components                                      -              
             */

        }

        public void OnLocked(string lockId, string currentVersion, IDeploymentStateService deploymentStateService, IUniqueIdGenerator uniqueIdGenerator)
        {
            var state = deploymentStateService.CaptureState(State.Service);

            var points = state.ActualPoints.Select(
                    x => new DeploymentPoint(uniqueIdGenerator.Generate(), x.NodeId, x.DeploymentId, x.Component, x.ComponentType));

            Apply(new ServiceDeploymentStateCaptured(Id, currentVersion, points));
        }

        public void OnDeploymentSuccess(IEnumerable<DeploymentConfirmation> confirmations)
        {
            var deploymentConfirmations = confirmations.Select(x => new Events.DeploymentConfirmation(x.RequestId, x.DeploymentId));
            if (State.HaveEventProcessorsBeenDeployed)
            {
                Apply(new FullDeploymentSucceeded(Id, deploymentConfirmations));
            }
            else
            {
                Apply(new EventProcessorsDeployed(Id, deploymentConfirmations));
            }
        }

        public void OnManagementTaskSucceeded()
        {
            if (State.HaveGatewaysBeenDisabled)
            {
                Apply(new GatewaysEnabled(Id));
            }
            else
            {
                Apply(new GatewaysDisabled(Id));                
            }
        }

        public void OnStartSuccess()
        {
            if (State.HaveReceptorsBeenStarted && State.HaveEventProcessorsBeenStarted)
            {
                Apply(new CommandProcessorsStarted(Id));
            }
            else if (State.HaveEventProcessorsBeenStarted)
            {
                Apply(new ReceptorsStarted(Id));
            }
            else
            {
                Apply(new EventProcessorsStarted());                
            }
        }

        public void OnWaitCompleted()
        {
            if (State.HaveViewsBeenRebuilt)
            {
                Apply(new OldCommandsProcessed(Id));
            }
            else
            {
                Apply(new ViewsRebuilt(Id));                
            }
        }

        public void OnStopSuccess()
        {
            if (State.HaveOldReceptorsBeenStopped && State.HaveOldEventProcessorsBeenStopped)
            {
                Apply(new OldCommandProcessorsStopped(Id));
            }
            else if (State.HaveOldEventProcessorsBeenStopped)
            {
                Apply(new OldReceptorsStopped(Id));
            }
            else
            {
                Apply(new OldEventProcessorsStopped(Id));
            }
        }

        public void OnRouterConfigured()
        {
            Apply(new RouterConfigured(Id));
        }

        public void OnUndeploymentSuccess()
        {
            Apply(new ServiceUpgradeSucceeded(Id, State.Service, State.BaseVersion, State.TargetVersion));
        }
    }
}
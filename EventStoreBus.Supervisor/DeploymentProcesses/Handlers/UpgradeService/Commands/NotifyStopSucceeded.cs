﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Commands
{
    [DataContract]
    [Version(1)]
    public class NotifyStopSucceeded : Command
    {
        [DataMember]
        public string ProcessId { get; set; }

        public NotifyStopSucceeded(string commandId, string processId) : base(commandId)
        {
            ProcessId = processId;
        }

        public NotifyStopSucceeded()
        {
        }
    }
}
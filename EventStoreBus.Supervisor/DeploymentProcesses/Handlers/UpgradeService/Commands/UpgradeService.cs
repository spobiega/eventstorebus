﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Commands
{
    [DataContract]
    [Version(1)]
    public class UpgradeService : Command
    {
        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string TargetVersion { get; set; }

        public UpgradeService(string commandId, string service, string targetVersion) : base(commandId)
        {
            Service = service;
            TargetVersion = targetVersion;
        }

        public UpgradeService()
        {
        }
    }
}
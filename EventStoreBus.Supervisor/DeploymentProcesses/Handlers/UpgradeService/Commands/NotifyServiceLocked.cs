﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Commands
{
    [DataContract]
    [Version(1)]
    public class NotifyServiceLocked : Command
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string LockId { get; set; }

        [DataMember]
        public string CurrentVersion { get; set; }

        public NotifyServiceLocked(string commandId, string processId, string lockId, string currentVersion) : base(commandId)
        {
            ProcessId = processId;
            LockId = lockId;
            CurrentVersion = currentVersion;
        }

        public NotifyServiceLocked()
        {
        }
    }
}
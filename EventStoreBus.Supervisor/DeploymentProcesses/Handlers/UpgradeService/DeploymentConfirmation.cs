﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class DeploymentConfirmation
    {
        private readonly string requestId;
        private readonly string deploymentId;

        public DeploymentConfirmation(string requestId, string deploymentId)
        {
            this.requestId = requestId;
            this.deploymentId = deploymentId;
        }

        public string RequestId
        {
            get { return requestId; }
        }

        public string DeploymentId
        {
            get { return deploymentId; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.ConfigureRouter;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.ConfigureRouter.Commands;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.ConfigureRouter.Events;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy.Commands;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Manage;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Manage.Commands;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start.Commands;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop.Commands;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy.Commands;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Wait;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Wait.Commands;
using EventStoreBus.Views.Api;
using System.Linq;
using DeploymentRequest = EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy.Commands.DeploymentRequest;
using StartRequest = EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Start.Commands.StartRequest;
using StopRequest = EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop.Commands.StopRequest;
using UndeploymentRequest = EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy.Commands.UndeploymentRequest;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class UpgradeProcessGateway
    {
        private readonly IViewReader<UpgradeProcessView> viewReader;
        private readonly ICommandSender commandSender;

        public UpgradeProcessGateway(IViewReader<UpgradeProcessView> viewReader, ICommandSender commandSender)
        {
            this.viewReader = viewReader;
            this.commandSender = commandSender;
        }

        public void When(ServiceDeploymentStateCaptured evnt, Guid eventId)
        {
            DeployEventProcessors(evnt, eventId);
        }

        private void DeployEventProcessors(ServiceDeploymentStateCaptured evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.EventProcessors.Select(
                x => new DeploymentRequest(x.Id, x.NodeId, processData.Service, x.Component, processData.TargetVersion));
            var command = new BeginDeploymentSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<DeploymentProcessHandler>(command);
        }

        public void When(EventProcessorsDeployed evnt, Guid eventId)
        {
            DisableGateways(evnt, eventId);
        }

        private void DisableGateways(EventProcessorsDeployed evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.EventProcessors.Select(x => new ManageRequest(x.Id, x.NodeId, x.NewDeploymentId, "EnableGateways", false));
            var command = new BeginManagementSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<ManageProcessHandler>(command);
        }

        public void When(GatewaysDisabled evnt, Guid eventId)
        {
            StartEventProcessors(evnt, eventId);
        }

        private void StartEventProcessors(GatewaysDisabled evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.EventProcessors.Select(x => new StartRequest(x.NodeId, x.NewDeploymentId));
            var command = new BeginStartSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<StartProcessHandler>(command);
        }

        public void When(EventProcessorsStarted evnt, Guid eventId)
        {
            WaitForViewsToRebuild(evnt, eventId);
        }

        private void WaitForViewsToRebuild(EventProcessorsStarted evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.EventProcessors.Select(x => new WaitRequest(x.Id, x.NodeId, x.NewDeploymentId, "Idle", true));
            var command = new Wait.Commands.Wait(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<WaitProcessHandler>(command);
        }

        public void When(ViewsRebuilt evnt, Guid eventId)
        {
            DeployAllComponents(evnt, eventId);
        }

        private void DeployAllComponents(ViewsRebuilt evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.NonEventProcessors.Select(
                x => new DeploymentRequest(x.Id, x.NodeId, processData.Service, x.Component, processData.TargetVersion));
            var command = new BeginDeploymentSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<DeploymentProcessHandler>(command);
        }

        public void When(FullDeploymentSucceeded evnt, Guid eventId)
        {
            StopOldEventProcessors(evnt, eventId);
        }

        private void StopOldEventProcessors(FullDeploymentSucceeded evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.EventProcessors.Select(x => new StopRequest(x.NodeId, x.OldDeploymentId));
            var command = new BeginStopSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<StopProcessHandler>(command);
        }

        public void When(OldEventProcessorsStopped evnt, Guid eventId)
        {
            StopOldReceptors(evnt, eventId);
        }

        private void StopOldReceptors(OldEventProcessorsStopped evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.Receptors.Select(x => new StopRequest(x.NodeId, x.OldDeploymentId));
            var command = new BeginStopSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<StopProcessHandler>(command);
        }

        public void When(OldReceptorsStopped evnt, Guid eventId)
        {
            SwitchLoadBalancer(evnt, eventId);
        }

        private void SwitchLoadBalancer(OldReceptorsStopped evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);    
            
            var command = new ConfigureEndpoint(eventId.ToString(), evnt.ProcessId, processData.Service, null /*TODO: Where to find URLs?*/ );
            commandSender.SendTo<ConfigureRouterProcessHandler>(command);
        }

        public void When(EndpointConfigured evnt, Guid eventId)
        {
            WaitUntillAllCommandsAreProcessed(evnt, eventId);
        }

        private void WaitUntillAllCommandsAreProcessed(EndpointConfigured evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.CommandProcessors.Select(x => new WaitRequest(x.Id, x.NodeId, x.OldDeploymentId, "Idle", true));
            var command = new Wait.Commands.Wait(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<WaitProcessHandler>(command);
        }

        public void When(OldCommandsProcessed evnt, Guid eventId)
        {
            StopOldCommandProcessors(evnt, eventId);
        }

        private void StopOldCommandProcessors(OldCommandsProcessed evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.CommandProcessors.Select(x => new StopRequest(x.NodeId, x.OldDeploymentId));
            var command = new BeginStopSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<StopProcessHandler>(command);
        }

        public void When(GatewaysEnabled evnt, Guid eventId)
        {
            StartNewReceptors(evnt, eventId);
        }

        private void StartNewReceptors(GatewaysEnabled evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.Receptors.Select(x => new StartRequest(x.NodeId, x.NewDeploymentId));
            var command = new BeginStartSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<StartProcessHandler>(command);
        }

        public void When(ReceptorsStarted evnt, Guid eventId)
        {
            StartNewCommandProcessors(evnt, eventId);
        }

        private void StartNewCommandProcessors(ReceptorsStarted evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.CommandProcessors.Select(x => new StartRequest(x.NodeId, x.NewDeploymentId));
            var command = new BeginStartSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<StartProcessHandler>(command);
        }

        public void When(OldCommandProcessorsStopped evnt, Guid eventId)
        {
            EnableGateways(evnt, eventId);
        }

        private void EnableGateways(OldCommandProcessorsStopped evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.EventProcessors.Select(x => new ManageRequest(x.Id, x.NodeId, x.NewDeploymentId, "EnableGateways", true));
            var command = new BeginManagementSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<ManageProcessHandler>(command);
        }

        public void When(CommandProcessorsStarted evnt, Guid eventId)
        {
            var processData = LoadProcessData(evnt.ProcessId);
            var requests = processData.DeploymentTasks.Select(x => new UndeploymentRequest(x.NodeId, x.OldDeploymentId));
            var command = new BeginUndeploymentSequence(eventId.ToString(), evnt.ProcessId, requests);
            commandSender.SendTo<UndeploymentProcessHandler>(command);
        }

        private UpgradeProcessView LoadProcessData(string processId)
        {
            UpgradeProcessView view;
            using (var session = viewReader.OpenSession())
            {
                view = session.Load(processId);
            }
            return view;
        }
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class OldCommandsProcessed : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }


        public OldCommandsProcessed(string processId)
        {
            ProcessId = processId;
        }

        public OldCommandsProcessed()
        {
        }
    }
}
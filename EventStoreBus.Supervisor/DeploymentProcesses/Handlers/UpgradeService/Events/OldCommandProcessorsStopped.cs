﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class OldCommandProcessorsStopped : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }


        public OldCommandProcessorsStopped(string processId)
        {
            ProcessId = processId;
        }

        public OldCommandProcessorsStopped()
        {
        }
    }
}
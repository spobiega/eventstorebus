﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class ServiceUpgradeStarted : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string TargetVersion { get; set; }

        public ServiceUpgradeStarted(string processId, string service, string targetVersion)
        {
            ProcessId = processId;
            Service = service;
            TargetVersion = targetVersion;
        }

        public ServiceUpgradeStarted()
        {
        }
    }

}
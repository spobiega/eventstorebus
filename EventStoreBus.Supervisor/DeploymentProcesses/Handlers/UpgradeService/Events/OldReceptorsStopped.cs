﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class OldReceptorsStopped : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }


        public OldReceptorsStopped(string processId)
        {
            ProcessId = processId;
        }

        public OldReceptorsStopped()
        {
        }
    }
}
﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    public class DeploymentConfirmation
    {
        [DataMember]
        public string RequestId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        public DeploymentConfirmation(string requestId, string deploymentId)
        {
            RequestId = requestId;
            DeploymentId = deploymentId;
        }

        public DeploymentConfirmation()
        {
        }
    }
}
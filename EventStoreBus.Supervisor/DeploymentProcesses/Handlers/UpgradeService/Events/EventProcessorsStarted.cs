﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class EventProcessorsStarted : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }


        public EventProcessorsStarted(string processId)
        {
            ProcessId = processId;
        }

        public EventProcessorsStarted()
        {
        }
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class GatewaysEnabled : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }


        public GatewaysEnabled(string processId)
        {
            ProcessId = processId;
        }

        public GatewaysEnabled()
        {
        }
    }
}
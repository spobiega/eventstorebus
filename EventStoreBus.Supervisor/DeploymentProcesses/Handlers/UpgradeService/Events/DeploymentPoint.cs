using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService.Events
{
    [DataContract]
    [Version(1)]
    public class DeploymentPoint
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string DeploymentId { get; set; }
        [DataMember]
        public string Component { get; set; }
        [DataMember]
        public ComponentType ComponentType { get; set; }

        public DeploymentPoint(string id, string nodeId, string deploymentId, string component, ComponentType componentType)
        {
            Id = id;
            NodeId = nodeId;
            DeploymentId = deploymentId;
            Component = component;
            ComponentType = componentType;
        }

        public DeploymentPoint()
        {
        }
    }
}
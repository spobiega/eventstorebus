﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public enum ComponentType
    {
        Receptor,
        CommandProcessor,
        EventProcessor,
        Endpoint
    }
}
﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class PlannedDeploymentPoint
    {
        private readonly string nodeId;
        private readonly string component;
        private readonly ComponentType componentType;

        public PlannedDeploymentPoint(string nodeId, string component, ComponentType componentType)
        {
            this.nodeId = nodeId;
            this.component = component;
            this.componentType = componentType;
        }

        public string NodeId
        {
            get { return nodeId; }
        }

        public string Component
        {
            get { return component; }
        }

        public ComponentType ComponentType
        {
            get { return componentType; }
        }
    }
}
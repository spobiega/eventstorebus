﻿using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.UpgdadeService
{
    public class UpgradeProcessView : ConcurrentViewBase
    {
        private readonly List<UpgradeTask> tasks = new List<UpgradeTask>();

        public string Service { get; private set; }
        public string TargetVersion { get; private set; }

        public IEnumerable<UpgradeTask> DeploymentTasks
        {
            get { return tasks; }
        }

        public IEnumerable<UpgradeTask> EventProcessors
        {
            get { return tasks.Where(x => x.ComponentType == ComponentType.EventProcessor); }
        }
        
        public IEnumerable<UpgradeTask> CommandProcessors
        {
            get { return tasks.Where(x => x.ComponentType == ComponentType.CommandProcessor); }
        }
        
        public IEnumerable<UpgradeTask> Receptors
        {
            get { return tasks.Where(x => x.ComponentType == ComponentType.Receptor); }
        }
        
        public IEnumerable<UpgradeTask> Endpoints
        {
            get { return tasks.Where(x => x.ComponentType == ComponentType.Endpoint); }
        }
        
        public IEnumerable<UpgradeTask> NonEventProcessors
        {
            get { return tasks.Where(x => x.ComponentType != ComponentType.EventProcessor); }
        }

        public void AddTask(string id, string nodeId, string deploymentId, ComponentType componentType, string component)
        {
            tasks.Add(new UpgradeTask(id, nodeId, component, componentType, component));
        }


        public void ConfirmNewVersionDeployed(string requestId, string deploymentId)
        {
            var point = tasks.FirstOrDefault(x => x.Id == requestId);
            if (point != null)
            {
                point.NewVersionDeployed(deploymentId);
            }
        }
    }
}
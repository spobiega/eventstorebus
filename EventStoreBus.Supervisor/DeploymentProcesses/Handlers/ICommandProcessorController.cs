﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers
{
    public interface ICommandProcessorController
    {
        bool HasEmptyQueue(string deploymentId, string commandProcessorName);
    }
}
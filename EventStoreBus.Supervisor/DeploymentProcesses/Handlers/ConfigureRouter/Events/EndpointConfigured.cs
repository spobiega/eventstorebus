﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.ConfigureRouter.Events
{
    [DataContract]
    [Version(1)]
    public class EndpointConfigured
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public string EndpointName { get; set; } 
    }
}
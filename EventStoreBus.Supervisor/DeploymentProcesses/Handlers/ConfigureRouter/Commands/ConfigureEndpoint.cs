﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.ConfigureRouter.Commands
{
    [DataContract]
    [Version(1)]
    public class ConfigureEndpoint : Command
    {
        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public string EndpointName { get; set; }

        [DataMember]
        public List<string> WorkerUrls { get; set; }

        public ConfigureEndpoint(string commandId, string parentProcessId, string endpointName, IEnumerable<string> workerUrls) 
            : base(commandId)
        {
            ParentProcessId = parentProcessId;
            EndpointName = endpointName;
            WorkerUrls = workerUrls.ToList();
        }

        public ConfigureEndpoint()
        {
        }
    }
}
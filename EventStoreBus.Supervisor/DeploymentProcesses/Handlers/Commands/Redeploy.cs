﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands
{
    [DataContract]
    [Version(1)]
    public class Redeploy : Command
    {
        [DataMember]
        public string Service { get; set; }
        
        [DataMember(IsRequired = false)]
        public string Version { get; set; }
    }
}
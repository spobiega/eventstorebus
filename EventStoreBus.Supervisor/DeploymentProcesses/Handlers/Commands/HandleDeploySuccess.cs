﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands
{
    [DataContract]
    [Version(1)]
    public class HandleDeploySuccess : Command
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string OperationId { get; set; }
        
        [DataMember]
        public string DeploymentId { get; set; }
    }
}
﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop
{
    public class StopTask
    {
        private readonly int maxAttempts;
        private readonly StopRequest request;
        private int failedAttempts;
        private bool succeeded;
        private bool completed;

        public StopTask(StopRequest request, int maxAttempts)
        {
            this.request = request;
            this.maxAttempts = maxAttempts;
        }

        public bool Succeeded
        {
            get { return succeeded; }
        }

        public StopRequest Request
        {
            get { return request; }
        }

        public bool Completed
        {
            get { return completed; }
        }

        public bool CanBeRetried()
        {
            return failedAttempts + 1 < maxAttempts;
        }

        public void When(StopSucceeded evnt)
        {
            succeeded = true;
            completed = true;
        }

        public void When(StopFailed evnt)
        {
            failedAttempts++;
            if (!evnt.RetryPending)
            {
                completed = true;
            }
        }
    }
}
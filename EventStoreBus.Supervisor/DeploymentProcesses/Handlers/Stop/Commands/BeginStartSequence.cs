﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop.Commands
{
    [DataContract]
    [Version(1)]
    public class BeginStopSequence : Command
    {
        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<StopRequest> Requests { get; set; }

        public BeginStopSequence(string commandId, string parentProcessId, IEnumerable<StopRequest> requests)
            : base(commandId)
        {
            ParentProcessId = parentProcessId;
            Requests = requests.ToList();
        }

        public BeginStopSequence()
        {
        } 
    }
}
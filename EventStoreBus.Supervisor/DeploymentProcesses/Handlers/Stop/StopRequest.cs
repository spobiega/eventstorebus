﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop
{
    public class StopRequest
    {
        private readonly string nodeId;
        private readonly string deploymentId;

        public StopRequest(string nodeId, string deploymentId)
        {
            this.nodeId = nodeId;
            this.deploymentId = deploymentId;
        }

        public string NodeId
        {
            get { return nodeId; }
        }

        public string DeploymentId
        {
            get { return deploymentId; }
        }
    }
}
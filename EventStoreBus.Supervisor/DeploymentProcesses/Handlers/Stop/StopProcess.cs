﻿using System.Collections.Generic;
using EventStoreBus.Aggregates.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop
{
    public class StopProcess : Aggregate<StopProcessState>
    {
        private readonly int maxAttempts;

        public StopProcess(int maxAttempts)
        {
            this.maxAttempts = maxAttempts;
        }

        public StopProcess()
            : this(5)
        {
        }

        public void Start(string parentProcessId, IEnumerable<StopRequest> requests)
        {
            Apply(new StopSequenceStarted(Id, parentProcessId));
            foreach (var request in requests)
            {
                Apply(new StopRequested(Id, request.DeploymentId, request.NodeId, maxAttempts));
            }
        }

        public void HandleStopSuccess(string deploymentId)
        {
            Apply(new StopSucceeded(Id, deploymentId));
            TryComplete();
        }

        public void HandleStopFailure(string deploymentId)
        {
            var task = State.GetTaskByDeploymentId(deploymentId);
            var retry = task.CanBeRetried();

            Apply(new StopFailed(Id, deploymentId, retry));

            if (retry)
            {
                Apply(new StopRequested(Id, task.Request.DeploymentId, task.Request.NodeId, maxAttempts));
            }
            else
            {
                TryComplete();
            }
        }

        private void TryComplete()
        {
            if (!State.HaveAllTasksCompleted)
            {
                return;
            }
            var failures = State.FailedStops;
            if (failures.Any())
            {
                Apply(new StopSequenceFailed(Id, State.ParentProcessId, failures));
            }
            else
            {
                Apply(new StopSequenceSucceeded(Id, State.ParentProcessId));
            }
        }
    }
}
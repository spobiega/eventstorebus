﻿using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop
{
    public class StopProcessState : AggregateState
    {
        private readonly Dictionary<string, StopTask> tasks = new Dictionary<string, StopTask>();

        public string ParentProcessId { get; private set; }

        public bool HaveAllTasksCompleted
        {
            get { return tasks.Values.All(x => x.Completed); }
        }

        public IList<string> FailedStops
        {
            get { return tasks.Values.Where(x => x.Completed && !x.Succeeded).Select(x => x.Request.DeploymentId).ToList(); }
        }

        public StopTask GetTaskByDeploymentId(string taskId)
        {
            return tasks[taskId];
        }

        public void When(StopSequenceStarted evnt)
        {
            ParentProcessId = evnt.ParentProcessId;
        }

        public void When(StopRequested evnt)
        {
            if (!tasks.ContainsKey(evnt.DeploymentId))
            {
                tasks[evnt.DeploymentId] = new StopTask(new StopRequest(evnt.NodeId, evnt.DeploymentId), evnt.MaxAttempts);
            }
        }

        public void When(StopSucceeded evnt)
        {
            tasks[evnt.DeploymentId].When(evnt);
        }
        
        public void When(StopFailed evnt)
        {
            tasks[evnt.DeploymentId].When(evnt);
        }
    }
}
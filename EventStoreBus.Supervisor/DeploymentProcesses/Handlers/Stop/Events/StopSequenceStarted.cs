﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop
{
    [DataContract]
    [Version(1)]
    public class StopSequenceStarted : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        public StopSequenceStarted(string processId, string parentProcessId)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
        }

        public StopSequenceStarted()
        {
        }
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop
{
    [DataContract]
    [Version(1)]
    public class StopFailed : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        [DataMember]
        public bool RetryPending { get; set; }

        public StopFailed(string processId, string deploymentId, bool retryPending)
        {
            ProcessId = processId;
            DeploymentId = deploymentId;
            RetryPending = retryPending;
        }

        public StopFailed()
        {
        }
    }
}
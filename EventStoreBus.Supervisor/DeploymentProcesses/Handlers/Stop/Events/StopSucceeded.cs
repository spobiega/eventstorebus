﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Stop
{
    [DataContract]
    [Version(1)]
    public class StopSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        public StopSucceeded(string processId, string deploymentId)
        {
            ProcessId = processId;
            DeploymentId = deploymentId;
        }

        public StopSucceeded()
        {
        }
    }
}
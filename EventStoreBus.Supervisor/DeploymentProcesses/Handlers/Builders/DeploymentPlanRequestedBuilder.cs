namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Builders
{
    public class DeploymentPlanRequestedBuilder
    {
        public static DeploymentPlanRequested Build(string id, RedeploymentProcessState state)
        {
            return new DeploymentPlanRequested()
                       {
                           LockId = state.LockId,
                           ProcessId = id,
                           Service = state.Service
                       };
        }
    }
}
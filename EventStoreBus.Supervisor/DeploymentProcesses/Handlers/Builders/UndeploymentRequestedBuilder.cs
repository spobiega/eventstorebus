﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Builders
{
    public class UndeploymentRequestedBuilder
    {
        public static UndeploymentRequested Build(string id, RedeploymentProcessState state)
        {
            return new UndeploymentRequested
                       {
                           DeploymentId = state.CurrentUndeployment.ExistingDeploymentId,
                           NodeId = state.CurrentUndeployment.NodeId,
                           ProcessId = id,
                       };
        }
    }
}
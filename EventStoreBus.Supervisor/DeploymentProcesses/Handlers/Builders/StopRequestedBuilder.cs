﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Builders
{
    public class StopRequestedBuilder
    {
        public static StopRequested Build(string id, RedeploymentProcessState state)
        {
            return new StopRequested
                       {
                           DeploymentId = state.CurrentUndeployment.ExistingDeploymentId,
                           NodeId = state.CurrentUndeployment.NodeId,
                           ProcessId = id,
                       };
        }
    }
}
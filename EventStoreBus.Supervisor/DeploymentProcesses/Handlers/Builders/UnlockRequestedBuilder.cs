﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Builders
{
    public class UnlockRequestedBuilder
    {
        public static UnlockRequested Build(string id, RedeploymentProcessState state)
        {
            return new UnlockRequested()
                       {
                           ProcessId   = id,
                           LockId = state.LockId,
                           Service = state.Service
                       };
        }
    }
}
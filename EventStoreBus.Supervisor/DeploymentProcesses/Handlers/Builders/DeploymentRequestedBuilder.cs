namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Builders
{
    public class DeploymentRequestedBuilder
    {
        public static DeploymentRequested Build(string id, RedeploymentProcessState state)
        {
            return new DeploymentRequested()
                       {
                           Service = state.Service,
                           Component = state.CurrentDeployment.Component,
                           Version = state.Version,
                           NodeId = state.CurrentDeployment.NodeId,
                           ProcessId = id
                       };
        }
    }
}
﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Wait.Commands
{
    [DataContract]
    [KnownType(typeof(string))]
    [KnownType(typeof(int))]
    [KnownType(typeof(bool))]
    public class WaitRequest
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string DeploymentId { get; set; }
        [DataMember]
        public string Attribute { get; set; }
        [DataMember]
        public object AwaitedValue { get; set; }

        public WaitRequest(string id, string nodeId, string deploymentId, string attribute, object awaitedValue)
        {
            Id = id;
            NodeId = nodeId;
            DeploymentId = deploymentId;
            Attribute = attribute;
            AwaitedValue = awaitedValue;
        }

        public WaitRequest()
        {
        }
    }
}
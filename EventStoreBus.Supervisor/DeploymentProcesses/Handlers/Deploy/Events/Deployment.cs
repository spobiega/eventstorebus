﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    [DataContract]
    public class Deployment
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string TaskId { get; set; }

        public Deployment(string id, string taskId)
        {
            Id = id;
            TaskId = taskId;
        }

        public Deployment()
        {
        }
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    [DataContract]
    [Version(1)]
    public class DeploymentRequested : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string TaskId { get; set; }

        [DataMember]
        public string NodeId { get; set; }

        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string Component { get; set; }

        [DataMember]
        public int MaxAttempts { get; set; }

        [DataMember(IsRequired = false)]
        public string DeploymentId { get; set; }

        [DataMember(IsRequired = false)]
        public string DeploymentUrl { get; set; }

        public DeploymentRequested(string processId, string taskId, string nodeId, string service, string version, string component, int maxAttempts)
        {
            ProcessId = processId;
            TaskId = taskId;
            NodeId = nodeId;
            Service = service;
            Version = version;
            Component = component;
            MaxAttempts = maxAttempts;
        }

        public DeploymentRequested()
        {
        }
    }
}
﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy
{
    public class DeploymentTask
    {
        private readonly int maxAttempts;
        private readonly DeploymentRequest request;
        private int failedAttempts;
        private string deploymentId;
        private bool succeeded;
        private bool completed;

        public DeploymentTask(DeploymentRequest request, int maxAttempts)
        {
            this.request = request;
            this.maxAttempts = maxAttempts;
        }

        public bool Succeeded
        {
            get { return succeeded; }
        }

        public DeploymentRequest Request
        {
            get { return request; }
        }

        public bool Completed
        {
            get { return completed; }
        }

        public string DeploymentId
        {
            get { return deploymentId; }
        }

        public bool CanBeRetried()
        {
            return failedAttempts + 1 < maxAttempts;
        }

        public void When(DeploymentSucceeded evnt)
        {
            succeeded = true;
            completed = true;
            deploymentId = evnt.DeploymentId;
        }

        public void When(DeploymentFailed evnt)
        {
            deploymentId = evnt.DeploymentId;
            failedAttempts++;
            if (!evnt.RetryPending)
            {
                completed = true;
            }
        }
    }
}
﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using EventStoreBus.Api;
using System.Linq;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy.Commands
{
    [DataContract]
    [Version(1)]
    public class BeginDeploymentSequence : Command
    {
        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public List<DeploymentRequest> Requests { get; set; }

        public BeginDeploymentSequence(string commandId, string parentProcessId, IEnumerable<DeploymentRequest> requests)
            : base(commandId)
        {
            ParentProcessId = parentProcessId;
            Requests = requests.ToList();
        }

        public BeginDeploymentSequence()
        {
        }
    }
}
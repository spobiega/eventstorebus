﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Deploy.Commands
{
    [DataContract]
    public class DeploymentRequest
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Component { get; set; }
        [DataMember]
        public string Version { get; set; }

        public DeploymentRequest(string id, string nodeId, string service, string component, string version)
        {
            Id = id;
            NodeId = nodeId;
            Service = service;
            Component = component;
            Version = version;
        }

        public DeploymentRequest()
        {
        }
    }
}
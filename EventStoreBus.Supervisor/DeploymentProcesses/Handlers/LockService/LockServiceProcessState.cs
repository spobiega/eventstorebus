using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.LockService
{
    public class LockServiceProcessState : AggregateState
    {
        public string ParentProcessId { get; private set; }
        public string Service { get; private set; }
        public string LockId { get; private set; }

        public void When(LockRequested evnt)
        {
            ParentProcessId = evnt.ParentProcessId;
            Service = evnt.Service;
            LockId = evnt.LockId;
        }
    }
}
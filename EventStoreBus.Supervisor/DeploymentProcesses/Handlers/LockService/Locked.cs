﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.LockService
{
    [DataContract]
    [Version(1)]
    public class Locked : IEvent
    {
        [DataMember]
        public string LockId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public string Version { get; set; }
    }
}
﻿using System;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.LockService
{
    public class LockServiceProcessHandler
    {
        private readonly IRepository<LockServiceProcess> processes;
        private readonly IUniqueIdGenerator uniqueIdGenerator;

        public LockServiceProcessHandler(IRepository<LockServiceProcess> processes, IUniqueIdGenerator uniqueIdGenerator)
        {
            this.processes = processes;
            this.uniqueIdGenerator = uniqueIdGenerator;
        }

        public void When(Lock command)
        {
            processes.Invoke(command.Id, command.Id,
                                        process => process.RequestLock(command.ParentProcessId, command.Service, uniqueIdGenerator));
        }

        public void When(HandleLockSuccess command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process => process.HandleLockSuccess());
        }

        public void When(HandleLockFailure command)
        {
            processes.InvokeEnsuringExists(command.ProcessId, command.Id,
                                         process => process.HandleLockFailure());
        }

        public class Factory : IAggregateComponentFactory
        {
            public object Create(Type componentImplementation, IRepositories repositories, IFactories factories)
            {
                return
                    new LockServiceProcessHandler(repositories.Get<LockServiceProcess>(), DefaultUniqueIdGenerator.Instance);
            }

            public void Release(object component)
            {
            }
        }
    }
}
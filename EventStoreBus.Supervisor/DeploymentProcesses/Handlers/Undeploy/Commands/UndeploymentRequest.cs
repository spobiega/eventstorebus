﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy.Commands
{
    [DataContract]
    public class UndeploymentRequest
    {
        [DataMember]
        public string NodeId { get; set; }
        [DataMember]
        public string DeploymentId { get; set; }

        public UndeploymentRequest(string nodeId, string deploymentId)
        {
            NodeId = nodeId;
            DeploymentId = deploymentId;
        }

        public UndeploymentRequest()
        {
        }
    }
}
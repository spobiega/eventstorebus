﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy
{
    public class UndeploymentRequest
    {
        private readonly string nodeId;
        private readonly string deploymentId;

        public UndeploymentRequest(string nodeId, string deploymentId)
        {
            this.nodeId = nodeId;
            this.deploymentId = deploymentId;
        }

        public string NodeId
        {
            get { return nodeId; }
        }

        public string DeploymentId
        {
            get { return deploymentId; }
        }
    }
}
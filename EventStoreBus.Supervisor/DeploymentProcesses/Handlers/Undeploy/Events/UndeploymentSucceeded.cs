﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy
{
    [DataContract]
    [Version(1)]
    public class UndeploymentSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string ParentProcessId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        public UndeploymentSucceeded(string processId, string parentProcessId, string deploymentId)
        {
            ProcessId = processId;
            ParentProcessId = parentProcessId;
            DeploymentId = deploymentId;
        }

        public UndeploymentSucceeded()
        {
        }
    }
}
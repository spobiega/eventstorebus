﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy
{
    [DataContract]
    [Version(1)]
    public class UndeploymentFailed : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        public UndeploymentFailed(string processId, string deploymentId)
        {
            ProcessId = processId;
            DeploymentId = deploymentId;
        }

        public UndeploymentFailed()
        {
        }
    }
}
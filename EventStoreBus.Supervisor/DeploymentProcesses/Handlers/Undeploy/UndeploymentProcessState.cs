﻿using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy
{
    public class UndeploymentProcessState : AggregateState
    {
        private readonly Dictionary<string, UndeploymentTask> tasks = new Dictionary<string, UndeploymentTask>();

        public string ParentProcessId { get; private set; }

        public bool HaveAllUndeploymentsCompleted
        {
            get { return tasks.Values.All(x => x.Completed); }
        }

        public IEnumerable<string> FailedUndeployments
        {
            get { return tasks.Values.Where(x => x.Completed && !x.Succeeded).Select(x => x.Request.DeploymentId); }
        }
        
        public IEnumerable<string> SucceededUndeployments
        {
            get { return tasks.Values.Where(x => x.Completed && x.Succeeded).Select(x => x.Request.DeploymentId); }
        }

        public UndeploymentTask GetTaskByDeploymentId(string taskId)
        {
            return tasks[taskId];
        }

        public void When(UndeploymentSequenceStarted evnt)
        {
            ParentProcessId = evnt.ParentProcessId;
        }

        public void When(UndeploymentRequested evnt)
        {
            if (!tasks.ContainsKey(evnt.DeploymentId))
            {
                tasks[evnt.DeploymentId] = new UndeploymentTask(new UndeploymentRequest(evnt.NodeId, evnt.DeploymentId), evnt.MaxAttempts);
            }
        }

        public void When(UndeploymentSucceeded evnt)
        {
            tasks[evnt.DeploymentId].When(evnt);
        }
        
        public void When(UndeploymentFailed evnt)
        {
            tasks[evnt.DeploymentId].When(evnt);
        }
    }
}
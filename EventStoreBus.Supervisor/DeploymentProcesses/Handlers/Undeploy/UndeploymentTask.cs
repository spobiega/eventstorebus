﻿namespace EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Undeploy
{
    public class UndeploymentTask
    {
        private readonly int maxAttempts;
        private readonly UndeploymentRequest request;
        private int failedAttempts;
        private bool succeeded;
        private bool completed;

        public UndeploymentTask(UndeploymentRequest request, int maxAttempts)
        {
            this.request = request;
            this.maxAttempts = maxAttempts;
        }

        public bool Succeeded
        {
            get { return succeeded; }
        }

        public UndeploymentRequest Request
        {
            get { return request; }
        }

        public bool Completed
        {
            get { return completed; }
        }

        public bool CanBeRetried()
        {
            return failedAttempts < maxAttempts;
        }

        public void When(UndeploymentSucceeded evnt)
        {
            succeeded = true;
            completed = true;
        }

        public void When(UndeploymentFailed evnt)
        {
            failedAttempts++;
            if (!CanBeRetried())
            {
                completed = true;
            }
        }
    }
}
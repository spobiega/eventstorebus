﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses
{
    [DataContract]
    [Version(1)]
    public class UndeploymentSucceeded : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; }

        [DataMember]
        public string OperationId { get; set; }
    }
}
﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses
{
    [Version(1)]
    [DataContract]
    public class StartRequested : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }

        [DataMember]
        public string DeploymentId { get; set; } 
        
        [DataMember]
        public string NodeId { get; set; }
    }
}
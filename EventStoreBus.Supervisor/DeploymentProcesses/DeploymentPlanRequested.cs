﻿using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses
{
    [DataContract]
    [Version(1)]
    public class DeploymentPlanRequested : IEvent
    {
        [DataMember]
        public string ProcessId { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string LockId { get; set; }
    }
}
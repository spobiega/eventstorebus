﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers;
using EventStoreBus.Supervisor.DeploymentProcesses.Handlers.Commands;
using EventStoreBus.Supervisor.Nodes.Handlers;
using EventStoreBus.Supervisor.Nodes.Handlers.Commands;
using EventStoreBus.Supervisor.Nodes.Views;
using EventStoreBus.Supervisor.Services.Handlers;
using EventStoreBus.Supervisor.Services.Views;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Gateways
{
    public class RedeploymentGateway
    {
        private readonly ICommandSender commandSender;
        private readonly IDeploymentPlanService deploymentPlanService;

        public RedeploymentGateway(ICommandSender commandSender, IDeploymentPlanService deploymentPlanService)
        {
            this.commandSender = commandSender;
            this.deploymentPlanService = deploymentPlanService;
        }

        public void When(LockRequested evnt, Guid eventId)
        {
            commandSender.SendTo<ServicesHandler>(new Lock()
                                                      {
                                                          LockId = evnt.LockId,
                                                          Service = evnt.Service,
                                                          Id = eventId.ToString()
                                                      });
        }
        
        public void When(UnlockRequested evnt, Guid eventId)
        {
            commandSender.SendTo<ServicesHandler>(new Unlock()
                                                      {
                                                          LockId = evnt.LockId,
                                                          Service = evnt.Service,
                                                          Id = eventId.ToString()
                                                      });
        }
        
        public void When(Succeeded evnt, Guid eventId)
        {
            commandSender.SendTo<ServicesHandler>(new SetCurrentVersion()
                                                      {
                                                          Service = evnt.Service,
                                                          Version = evnt.Version,
                                                          Id = eventId.ToString()
                                                      });
        }

        public void When(DeploymentRequested evnt, Guid eventId)
        {
            commandSender.SendTo<NodesHandler>(new DeployComponent()
                                                   {
                                                       Service = evnt.Service,
                                                       Component = evnt.Component,
                                                       Version = evnt.Version,
                                                       NodeId = evnt.NodeId,
                                                       Id = eventId.ToString()
                                                   });
        }

        public void When(StartRequested evnt, Guid eventId)
        {
            commandSender.SendTo<NodesHandler>(new StartComponent()
                                                   {
                                                       DeploymentId = evnt.DeploymentId,
                                                       NodeId = evnt.NodeId,
                                                       Id = eventId.ToString()
                                                   });
        }
        
        public void When(StopRequested evnt, Guid eventId)
        {
            commandSender.SendTo<NodesHandler>(new StopComponent()
                                                   {
                                                       DeploymentId = evnt.DeploymentId,
                                                       NodeId = evnt.NodeId,
                                                       Id = eventId.ToString()
                                                   });
        }
        
        public void When(UndeploymentRequested evnt, Guid eventId)
        {
            commandSender.SendTo<NodesHandler>(new UndeployComponent()
                                                   {
                                                       DeploymentId = evnt.DeploymentId,
                                                       NodeId = evnt.NodeId,
                                                       Id = eventId.ToString()
                                                   });
        }

        public void When(DeploymentPlanRequested evnt, Guid eventId)
        {
            var plan = deploymentPlanService.BuildRedeploymentPlan(evnt.Service, evnt.LockId);
            commandSender.SendTo<RedeploymentHandler>(new LoadPlan
                                                          {
                                                              Id = eventId.ToString(),
                                                              Plan = plan,
                                                              ProcessId = evnt.ProcessId
                                                          });
        }

        public class Factory : IGatewayFactory
        {
            public object Create(Type receptorImplementation, ICommandSender commandSender, IViewReaders viewReaders)
            {
                var planService = new DeploymentPlanService(
                    viewReaders.GetReader<NodeView, NodeViewProjection.Storage>(),
                    viewReaders.GetReader<ServiceView, ServiceViewProjection.Storage>());

                return new RedeploymentGateway(commandSender, planService);
            }

            public void Release(object receptor)
            {
            }
        }
    }
}
using EventStoreBus.Views.Api;

namespace EventStoreBus.Supervisor.DeploymentProcesses.Views
{
    public class CorrelationView : ConcurrentViewBase
    {
        public string ProcessId { get; set; }
        public string CorrelationId { get; set; }
    }
}
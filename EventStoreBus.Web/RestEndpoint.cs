﻿using System.IO;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.SelfHost;
using DurableSubscriber.Diagnostics;
using EventStoreBus.Api;
using EventStoreBus.Logging;
using EventStoreBus.Views.Api;
using EventStoreBus.Web.Api;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace EventStoreBus.Web
{
    public class RestEndpoint : IEndpointListener
    {
        private HttpSelfHostServer server;
        private readonly string endpointUrl;
        private readonly IWebFactory webFactory;
        private readonly string name;
        private readonly IViewEngine viewEngine;
        private readonly ICommandSender commandSender;
        private readonly ILogger logger;

        public RestEndpoint(string name, string endpointUrl, IWebFactory webFactory, IViewEngine viewEngine, ICommandSender commandSender)
        {
            this.endpointUrl = endpointUrl;
            this.webFactory = webFactory;
            this.name = name;
            this.viewEngine = viewEngine;
            this.commandSender = commandSender;
            logger = LogManager.GetLogger(GetType().FullName + "(" + ")");
        }

        public void Start()
        {
            if (server != null)
            {
                return;
            }
            logger.Info("Starting.");
            viewEngine.Start();
            var config = new HttpSelfHostConfiguration(endpointUrl);
            Configure(config);
            server = new HttpSelfHostServer(config);
            server.OpenAsync().Wait();
            logger.Info("Started.");
        }

        private void Configure(HttpConfiguration config)
        {
            config.Services.Replace(typeof(IHttpControllerActivator), 
                new ReleasingControllerActivator(webFactory, endpointUrl, viewEngine, commandSender));
            config.Filters.Add(new UnhandledExceptionFilterAttribute());
            config.Formatters.Add(new HtmlFormatter());
            webFactory.Configure(config);
            ConfigureRazor();
        }

        private void ConfigureRazor()
        {
            var templateConfig = new TemplateServiceConfiguration
                                     {
                                         Resolver = new DelegateTemplateResolver(LoadViewTemplate)
                                     };
            Razor.SetTemplateService(new TemplateService(templateConfig));
        }

        private string LoadViewTemplate(string viewModelName)
        {
            var viewModelNameWithoutNamespace = viewModelName.Substring(viewModelName.IndexOf(".") + 1);
            var fileName = viewModelNameWithoutNamespace.Replace(".", @"\") + ".cshtml";
            Stream stream;
            if (File.Exists(fileName))
            {
                stream = File.OpenRead(fileName);
                logger.Debug("Using template from file {0}", fileName);
            }
            else
            {
                var templateName = viewModelName + ".cshtml";
                stream = webFactory.GetType().Assembly.GetManifestResourceStream(templateName);
                if (stream == null)
                {
                    logger.Error("Template {0} not found in the assembly resources.", templateName);
                }
            }
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public void Stop()
        {
            if (server == null)
            {
                return;
            }
            logger.Info("Stopping.");
            server.CloseAsync().Wait();
            server = null;
            viewEngine.Stop();
            logger.Info("Stopped.");
        }

        public State State
        {
            get { return new State("Rest endpoint " + name, GetType()); }
        }
    }
}
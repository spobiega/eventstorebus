﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;
using EventStoreBus.Web.Api;

namespace EventStoreBus.Web
{
    public class RestEndpointFactory<T> : IListenerFactory
        where T : IWebFactory, new()
    {
        private static readonly IWebFactory webFactory = new T();

        public IEndpointListener Create(Type gatewayImplementation, string name, string endpointAddress, ICommandSender commandSender, IViewEngine viewEngine)
        {
            return new RestEndpoint(name, endpointAddress, webFactory, viewEngine, commandSender);
        }
    }
}
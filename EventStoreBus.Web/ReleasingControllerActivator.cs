﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;
using EventStoreBus.Web.Api;

namespace EventStoreBus.Web
{
    public class ReleasingControllerActivator : IHttpControllerActivator
    {
        private readonly IWebFactory webFactory;
        private readonly string endpointUrl;
        private readonly IViewEngine viewEngine;
        private readonly ICommandSender commandSender;

        public ReleasingControllerActivator(IWebFactory webFactory, string endpointUrl, IViewEngine viewEngine, ICommandSender commandSender)
        {
            this.webFactory = webFactory;
            this.endpointUrl = endpointUrl;
            this.viewEngine = viewEngine;
            this.commandSender = commandSender;
        }

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            var controller = webFactory.Create(controllerType, endpointUrl, viewEngine.CreateReaders(), commandSender);
            request.RegisterForDispose(new Release(() => webFactory.Release(controller)));
            return controller;
        }

        private class Release : IDisposable
        {
            private readonly Action release;

            public Release(Action release)
            {
                this.release = release;
            }

            public void Dispose()
            {
                release();
            }
        }
    }
}
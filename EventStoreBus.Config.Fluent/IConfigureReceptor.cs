﻿using System;

namespace EventStoreBus.Config.Fluent
{
    public interface IConfigureReceptor
    {
        void SetFailureHandling(int retries, string store, string stream);
        void AddSubscription(string source, Action<IConfigureCheckpointing> checkpointingConfig);
    }

    public class ConfigureReceptor : IConfigureReceptor
    {
        private readonly Receptor receptor;

        public ConfigureReceptor(Receptor receptor)
        {
            this.receptor = receptor;
        }

        public void SetFailureHandling(int retries, string store, string stream)
        {
            receptor.FailureHandling = new FailureHandling()
                                           {
                                               Default = new DefaultFailureHandling()
                                                             {
                                                                 FailedStore = store,
                                                                 FailedStream = stream,
                                                                 Retries = retries
                                                             }
                                           };
        }

        public void AddSubscription(string source, Action<IConfigureCheckpointing> checkpointingConfig)
        {
            
            var checkpointing = new Checkpointing();
            var checkpointingConfigurator = new ConfigureCheckpointing(checkpointing);
            checkpointingConfig(checkpointingConfigurator);

            var subscription = new Subscription
                                   {
                SourceStore = source,
                Checkpointing = checkpointing
            };

            receptor.Subscriptions.Add(subscription);
        }
    }
}
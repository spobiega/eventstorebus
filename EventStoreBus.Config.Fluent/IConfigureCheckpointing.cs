﻿using System;

namespace EventStoreBus.Config.Fluent
{
    public interface IConfigureCheckpointing
    {
        void SetCheckpointing(string store, int batchSize);
    }

    public class ConfigureCheckpointing : IConfigureCheckpointing
    {
        private readonly Checkpointing checkpointing;

        public ConfigureCheckpointing(Checkpointing checkpointing)
        {
            this.checkpointing = checkpointing;
        }

        public void SetCheckpointing(string store, int batchSize)
        {
            checkpointing.BatchSize = batchSize;
            checkpointing.Store = store;
        }
    }
}
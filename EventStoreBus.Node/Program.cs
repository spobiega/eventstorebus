﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventStoreBus.Node.Api;
using NetMX;
using NetMX.Relation;
using NetMX.Remote.HttpAdaptor;
using NetMX.Server;
using NuGet;

namespace EventStoreBus.Node
{
    class Program
    {
        static void Main(string[] args)
        {
            const string baseUrl = "http://localhost:12345";

            //var remoteRepo = new DataServicePackageRepository(new Uri("http://localhost/NuGetServer/nuget"));
            var remoteRepo = new LocalPackageRepository(@"C:\NuGetServerTest");
            var packageStore = new FileSystemPackageStore(new PhysicalFileSystem("."), remoteRepo, baseUrl + "/hosts");

            var endpoint = new RestEndpoint(baseUrl+"/node", packageStore);
            endpoint.Start();
            Console.WriteLine("Node started. Press <enter> to exit.");
            Console.ReadLine();
            endpoint.Stop();
        }
    }
}

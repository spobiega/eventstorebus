﻿using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using EventStoreBus.Node.Api.Representations;

namespace EventStoreBus.Node
{
    public class PackageDeployment
    {
        public readonly PackageId PackageId;
        public readonly string Path;
        public readonly string Id;
        public readonly string Component;

        public PackageDeployment(string id, PackageId packageId, string component, string path)
        {
            Path = path;
            Component = component;
            Id = id;
            PackageId = packageId;
        }

        public InstanceState GetState()
        {
            var controller = ServiceController.GetServices().First(x => x.ServiceName == "EventStoreBus.Host$" + Id);
            return controller.Status == ServiceControllerStatus.Running
                       ? InstanceState.Running
                       : InstanceState.Stopped;
        }

        public void Install()
        {
            CallTopShelf("install", "--manual");
        }

        public void Start()
        {
            CallTopShelf("start");
        }

        public void Stop()
        {
            CallTopShelf("stop");
        }

        public void Uninstall()
        {
            CallTopShelf("uninstall");
        }

        private void CallTopShelf(string verb, string otherParams = "")
        {
            var execPath = System.IO.Path.Combine(Path, "EventStoreBus.Host.exe");
            var startInfo = new ProcessStartInfo(execPath)
            {
                WorkingDirectory = Path,
                Arguments = string.Format("{0} -instance:{1} {2}", verb, Id, otherParams)
            };
            var process = Process.Start(startInfo);
            process.WaitForExit();
        }

    }
}
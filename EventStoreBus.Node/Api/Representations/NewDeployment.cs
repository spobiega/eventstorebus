using System.Runtime.Serialization;

namespace EventStoreBus.Node.Api.Representations
{
    [DataContract(Namespace = "http://api.eventstorebus.com/node")]
    public class NewDeployment
    {
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Version { get; set; }
        [DataMember]
        public string Component { get; set; }
    }
}
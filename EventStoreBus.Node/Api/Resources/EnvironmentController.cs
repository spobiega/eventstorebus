﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using EventStoreBus.Node.Api.Representations;

namespace EventStoreBus.Node.Api.Resources
{
    public class EnvironmentController : BaseController
    {
        public EnvironmentController(IPackageStore packageStore, Uri baseUrl) : base(packageStore, baseUrl)
        {
        }

        public Task Put()
        {
            return Request.Content.ReadAsStreamAsync()
                .ContinueWith(x => PackageStore.UpdateEnvironment(x.Result))
                .ContinueWith(x =>
                                  {
                                      var runningDeployments = x.Result.Where(deployment => deployment.GetState() == InstanceState.Running);
                                      foreach (var deployment in runningDeployments)
                                      {
                                          deployment.Stop();
                                          deployment.Start();
                                      }
                                  });
        }
    }
}
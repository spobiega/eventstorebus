﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EventStoreBus.Node.Api.Representations;
using NuGet;

namespace EventStoreBus.Node.Api.Resources
{
    public class DeploymentController : BaseController
    {
        public DeploymentController(IPackageStore packageStore, Uri baseUrl) : base(packageStore, baseUrl)
        {
        }

        public HttpResponseMessage Post(string id, string verb)
        {
            if (verb.ToLowerInvariant() == "delete")
            {
                var originalResponse = Delete(id);
                if (!originalResponse.IsSuccessStatusCode)
                {
                    return originalResponse;
                }
                var response = Request.CreateResponse(HttpStatusCode.SeeOther);
                response.Headers.Location = new Uri(BaseUrl, Url.Route("node", new { }));
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        public HttpResponseMessage Put(NewDeployment newDeployment)
        {
            var deploymentId = (string)ControllerContext.RouteData.Values["id"];
            var existingDeployment = PackageStore.GetDeployment(deploymentId);
            if (existingDeployment != null)                
            {
                if (existingDeployment.PackageId.UniqueName != newDeployment.Service
                    || existingDeployment.PackageId.Version.ToString() != newDeployment.Version
                    || existingDeployment.Component != newDeployment.Component)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Conflict, "A different deployment with same ID has already been done.");
                }
                return Request.CreateResponse(HttpStatusCode.OK, new Deployment(existingDeployment, Url));
            }

            var packageId = new PackageId(newDeployment.Service, new SemanticVersion(newDeployment.Version));
            var deployment = PackageStore.DeployPackage(deploymentId, packageId, newDeployment.Component);
            deployment.Install();

            var representation = new Deployment(deployment, Url);
            var responseMessage = Request.CreateResponse(HttpStatusCode.OK, representation);
            return responseMessage;
        }

        public HttpResponseMessage Get(string id)
        {
            var deployment = PackageStore.GetDeployment(id);
            if (deployment == null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Deployment()
                                                                     {
                                                                         Id = id
                                                                     });
            }
            var responseMessage = Request.CreateResponse(HttpStatusCode.OK, new Deployment(deployment, Url));
            return responseMessage;
        }

        public HttpResponseMessage Delete(string id)
        {
            var deployment = PackageStore.GetDeployment(id);
            if (deployment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            if (deployment.GetState() == InstanceState.Running)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "The component cannot be undeployed because it's host is still running. Try stopping it first.");
            }
            deployment.Uninstall();
            PackageStore.UndeployPackage(deployment);
            var responseMessage = Request.CreateResponse(HttpStatusCode.OK);
            return responseMessage;
        }
    }
}
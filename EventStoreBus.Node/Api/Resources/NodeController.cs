using System;
using System.Linq;
using System.Net.Http;
using System.ServiceProcess;
using System.Web.Http;
using EventStoreBus.Node.Api.Representations;

namespace EventStoreBus.Node.Api.Resources
{
    public class NodeController : BaseController
    {
        public NodeController(IPackageStore packageStore, Uri baseUrl) : base(packageStore, baseUrl)
        {
        }

        public Representations.Node Get()
        {
            var newDeploymentId = Guid.NewGuid().ToString("N");

            var deployments = PackageStore.Deployments;

            return new Representations.Node()
                       {
                           Id = BaseUrl.ToString(),
                           Deploy = HyperLink.Put("deploy", Url, "deployment", new {id = newDeploymentId}),
                           UpdateEnvironment = HyperLink.Put("update-environment", Url, "environment", new { }),
                           Deployments = deployments.Select(x => new Deployment(x, Url)).ToList()
                       };
        }
    }
}
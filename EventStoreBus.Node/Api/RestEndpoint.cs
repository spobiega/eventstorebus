﻿using System;
using System.IO;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.SelfHost;
using EventStoreBus.Node.Api.Resources;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace EventStoreBus.Node.Api
{
    public class RestEndpoint
    {
        private HttpSelfHostServer server;
        private readonly string listenAddress;
        private readonly IPackageStore packageStore;

        public RestEndpoint(string listenAddress, IPackageStore packageStore)
        {
            this.listenAddress = listenAddress;
            this.packageStore = packageStore;
        }

        public void Start()
        {
            if (server != null)
            {
                throw new InvalidOperationException("Server is already started.");
            }
            var config = new HttpSelfHostConfiguration(listenAddress);
            Configure(config);
            server = new HttpSelfHostServer(config);
            server.OpenAsync().Wait();
        }

        private void Configure(HttpSelfHostConfiguration config)
        {
            config.Formatters.Add(new HtmlFormatter());

            config.Services.Replace(typeof(IHttpControllerActivator), new ControllerActivator(packageStore, new Uri(listenAddress)));

            config.Routes.MapHttpRoute<NodeController>("");
            config.Routes.MapHttpRoute<InstanceController>("deployments/{id}/instance");
            config.Routes.MapHttpRoute<DeploymentController>("deployments/{id}");
            config.Routes.MapHttpRoute<EnvironmentController>("environment");

            config.Filters.Add(new UnhandledExceptionFilterAttribute());

            ConfigureRazor();
        }

        private static void ConfigureRazor()
        {
            const string viewPathTemplate = "EventStoreBus.Node.Api.Representations.{0}";
            var templateConfig = new TemplateServiceConfiguration();
            templateConfig.Resolver = new DelegateTemplateResolver(name =>
                                                                       {
                                                                           string resourcePath = string.Format(viewPathTemplate, name);
                                                                           var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourcePath);
                                                                           using (var reader = new StreamReader(stream))
                                                                           {
                                                                               return reader.ReadToEnd();
                                                                           }
                                                                       });
            Razor.SetTemplateService(new TemplateService(templateConfig));
        }

        public void Stop()
        {
            if (server == null)
            {
                throw new InvalidOperationException("Server is already stopped.");
            }
            server.CloseAsync().Wait();
        }
    }
}
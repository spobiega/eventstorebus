﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace EventStoreBus.Node.Api
{
    public class ControllerActivator : IHttpControllerActivator
    {
        private readonly IPackageStore packageStore;
        private readonly Uri baseUrl;

        public ControllerActivator(IPackageStore packageStore, Uri baseUrl)
        {
            this.packageStore = packageStore;
            this.baseUrl = baseUrl;
        }

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            return (IHttpController)Activator.CreateInstance(controllerType, packageStore, baseUrl);
        }
    }
}
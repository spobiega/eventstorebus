﻿using System;
using System.Web.Http;
using System.Web.Http.Routing;

namespace EventStoreBus.Node.Api
{
    public static class RouteExtensions
    {
        public static string Route<T>(this UrlHelper urlHelper, object parameters)
        {
            var routeName = GetRouteName(typeof(T));
            return urlHelper.Route(routeName, parameters);
        }

        public static IHttpRoute MapHttpRoute<T>(this HttpRouteCollection routeCollection, string template)
        {
            var routeName = GetRouteName(typeof(T));
            var controllerName = GetControllerName(typeof(T));

            return routeCollection.MapHttpRoute(routeName, template, new { controller = controllerName});
        }

        private static string GetControllerName(Type resourceType)
        {
            return resourceType.Name.Replace("Controller", "");
        }

        private static string GetRouteName(Type resourceType)
        {
            return GetControllerName(resourceType).ToLowerInvariant();
        }
    }
}
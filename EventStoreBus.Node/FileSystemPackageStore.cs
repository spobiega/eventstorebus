﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using NuGet;

namespace EventStoreBus.Node
{
    public class FileSystemPackageStore : IPackageStore
    {
        private static readonly object syncRoot = new object();

        private const string PackagesDirectoryName = "Packages";
        private const string DeploymentsDirectoryName = "Deployments";
        private const string LogsDirectoryName = "Logs";
        private const string HostPackageName = "EventStoreBus.Host";

        private readonly IFileSystem fileSystem;
        private readonly IPackageRepository remoteRepository;
        private readonly string baseUrl;
        private readonly LocalPackageRepository repository;
        private static readonly XmlSerializer deploymentDescriptorSerializer = new XmlSerializer(typeof(Config.Deployment));

        public FileSystemPackageStore(IFileSystem fileSystem, IPackageRepository remoteRepository, string baseUrl)
        {
            repository = new LocalPackageRepository(fileSystem.GetFullPath(PackagesDirectoryName));
            this.fileSystem = fileSystem;
            this.remoteRepository = remoteRepository;
            this.baseUrl = baseUrl;
        }

        public PackageDeployment DeployPackage(string deploymentId, PackageId packageId, string component)
        {
            lock (syncRoot)
            {
                var package = GetPackage(packageId);
                var deploymentPath = GetDeploymentPath(deploymentId);

                var hostPackage = remoteRepository.FindPackage(HostPackageName);
                InstallPackage(hostPackage, deploymentPath);
                InstallPackage(package, deploymentPath);
                SaveManifestFile(deploymentPath, package);
                SaveDeploymentDescriptor(packageId, deploymentId, deploymentPath, component);

                return new PackageDeployment(deploymentId, packageId, component, deploymentPath);
            }
        }

        private void SaveDeploymentDescriptor(PackageId packageId, string deploymentId, string deploymentPath, string component)
        {
            var logDirectory = Path.Combine(Environment.CurrentDirectory, LogsDirectoryName, packageId.UniqueName);
            var deploymentDescriptor = new Config.Deployment
                                           {
                                               Id = deploymentId,
                                               BaseUrl = baseUrl,
                                               Service = packageId.UniqueName,
                                               Version = packageId.Version.ToString(),
                                               LogDirectory = logDirectory
                                           };
            deploymentDescriptor.Components.Add(component);
            var deploymentDescriptorPath = GetDeploymentDescriptorPath(deploymentPath);
            using (var deploymentDescriptorStream = File.OpenWrite(deploymentDescriptorPath))
            {
                deploymentDescriptorSerializer.Serialize(deploymentDescriptorStream, deploymentDescriptor);
                deploymentDescriptorStream.Flush();
            }
        }

        private static Config.Deployment ReadDeploymentDescriptor(string deploymentPath)
        {
            var deploymentDescriptorPath = GetDeploymentDescriptorPath(deploymentPath);
            using (var deploymentDescriptorStream = File.OpenRead(deploymentDescriptorPath))
            {
                return (Config.Deployment) deploymentDescriptorSerializer.Deserialize(deploymentDescriptorStream);
            }
        }

        private static void SaveManifestFile(string deploymentPath, IPackage package)
        {
            var manifest = Manifest.Create(package);
            var manifestPath = GetManifestPath(deploymentPath);
            using (var manifestFileStream = File.OpenWrite(manifestPath))
            {
                manifest.Save(manifestFileStream);
            }
        }

        private IPackage GetPackage(PackageId packageId)
        {
            var package = repository.FindPackage(packageId.UniqueName, packageId.Version);
            if (package == null)
            {
                package = remoteRepository.FindPackage(packageId.UniqueName, packageId.Version);
                repository.AddPackage(package);
            }
            return package;
        }

        private void InstallPackage(IPackage package, string deploymentPath)
        {
            foreach (var file in package.GetLibFiles())
            {
                var fileName = Path.GetFileName(file.Path);
                fileSystem.AddFile(Path.Combine(deploymentPath, fileName), file.GetStream());                
            }
        }

        private static string GetDeploymentDescriptorPath(string deploymentPath)
        {
            return Path.Combine(deploymentPath, "deployment.xml");
        }

        private static string GetManifestPath(string deploymentPath)
        {
            return Path.Combine(deploymentPath, "manifest.xml");
        }
        
        public PackageDeployment GetDeployment(string deploymentId)
        {
            lock (syncRoot)
            {
                var deploymentPath = GetDeploymentPath(deploymentId);
                if (!Directory.Exists(deploymentPath))
                {
                    return null;
                }
                var manifestPath = GetManifestPath(deploymentPath);
                Manifest manifest;
                using (var manifestFileStream = File.OpenRead(manifestPath))
                {
                    manifest = Manifest.ReadFrom(manifestFileStream);
                }
                var deploymentDescriptor = ReadDeploymentDescriptor(deploymentPath);
                return new PackageDeployment(deploymentId,
                                             new PackageId(manifest.Metadata.Id,
                                                           new SemanticVersion(manifest.Metadata.Version)),
                                             deploymentDescriptor.Components[0],
                                             deploymentPath);
            }
        }

        public IEnumerable<PackageDeployment> Deployments
        {
            get
            {
                lock (syncRoot)
                {
                    if (Directory.Exists(DeploymentsDirectoryName))
                    {
                        var deploymentDirectories = Directory.GetDirectories(DeploymentsDirectoryName);
                        foreach (var deploymentPath in deploymentDirectories)
                        {
                            var deploymentId = Path.GetFileName(deploymentPath.TrimEnd(Path.DirectorySeparatorChar));
                            var deployment = GetDeployment(deploymentId);
                            if (deployment != null)
                            {
                                yield return deployment;
                            }
                        }
                    }
                }
            }
        }

        public void UndeployPackage(PackageDeployment packageDeployment)
        {
            lock (syncRoot)
            {
                foreach (var file in fileSystem.GetFiles(packageDeployment.Path, "*.*", true))
                {
                    fileSystem.DeleteFile(file);
                }
                fileSystem.DeleteDirectory(packageDeployment.Path, true);
                var package = repository.FindPackage(packageDeployment.PackageId.UniqueName,
                                                     packageDeployment.PackageId.Version);
                if (package != null)
                {
                    repository.RemovePackage(package);
                }
            }
        }

        public IEnumerable<PackageDeployment> UpdateEnvironment(Stream newEnvironmentData)
        {
            lock (syncRoot)
            {
                var environmentFilePath = Path.Combine(DeploymentsDirectoryName, "environment.xml");
                var oldEnvironment = new Config.Environment();
                if (File.Exists(environmentFilePath))
                {
                    oldEnvironment = ReadEnvironment(environmentFilePath);
                    File.Delete(environmentFilePath);
                }
                using (var environmentFile = File.OpenWrite(environmentFilePath))
                {
                    newEnvironmentData.CopyTo(environmentFile);
                    environmentFile.Flush();
                }
                var newEnvironment = ReadEnvironment(environmentFilePath);

                var added =
                    newEnvironment.Deployments.Where(
                        x => !oldEnvironment.Deployments.Any(o => o.DeploymentId == x.DeploymentId));
                var removed =
                    oldEnvironment.Deployments.Where(
                        x => !newEnvironment.Deployments.Any(o => o.DeploymentId == x.DeploymentId));

                var affectedServices = added.Concat(removed).Select(x => x.Service).Distinct();
                return Deployments.Where(x => affectedServices.Contains(x.PackageId.UniqueName));
            }
        }

        private static Config.Environment ReadEnvironment(string environmentFilePath)
        {
            using (var environmentFile = File.OpenRead(environmentFilePath))
            {
                return (Config.Environment)new XmlSerializer(typeof (Config.Environment)).Deserialize(environmentFile);
            }
        }

        private static string GetDeploymentPath(string deploymentId)
        {
            return Path.Combine(Environment.CurrentDirectory, DeploymentsDirectoryName, deploymentId);
        }
    }
}
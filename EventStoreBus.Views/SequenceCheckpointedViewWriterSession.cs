﻿using EventStoreBus.Api;
using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus.Views
{
    public class SequenceCheckpointedViewWriterSession<TView, TViewStorage> : ViewWriterSession<TView, TViewStorage>
        where TView : class 
        where TViewStorage : IViewStorage<TView>, new()
    {
        public SequenceCheckpointedViewWriterSession(IDocumentSession documentSession) : base(documentSession)
        {
        }

        protected override void OnHandled(object view, IEnvelope envelope)
        {
            ((SequenceCheckpointedViewBase)view).OnHandled(envelope.Store, envelope.StreamId, envelope.Sequence);
        }

        protected override bool ShouldHandle(object view, IEnvelope envelope)
        {
            return ((SequenceCheckpointedViewBase)view).ShouldHandle(envelope.Store, envelope.StreamId, envelope.Sequence);
        }
    }
}
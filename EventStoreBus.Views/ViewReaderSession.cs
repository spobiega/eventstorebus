﻿using System.Linq;
using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus.Views
{
    public class ViewReaderSession<TView, TViewStorage> : IViewReaderSession<TView>
        where TView : class 
        where TViewStorage : IViewStorage<TView>, new()
    {
        private static readonly TViewStorage storage = new TViewStorage();

        private readonly IDocumentSession session;

        public ViewReaderSession(IDocumentSession session)
        {
            this.session = session;
        }

        public TView Load(object viewId)
        {
            var documentId = GetDocumentId(viewId);
            return session.Load<TView>(documentId);
        }

        private static string GetDocumentId(object viewId)
        {
            return string.Format("{0}/{1}", storage.CollectionName, viewId);
        }

        public IQueryable<TView> Query()
        {
            return session.Query<TView>();
        }

        public void Dispose()
        {
            session.Dispose();
        }

    }
}
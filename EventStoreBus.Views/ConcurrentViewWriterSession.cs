﻿using EventStoreBus.Api;
using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus.Views
{
    public class ConcurrentViewWriterSession<TView, TViewStorage> : ViewWriterSession<TView, TViewStorage>
        where TView : class 
        where TViewStorage : IViewStorage<TView>, new()
    {
        public ConcurrentViewWriterSession(IDocumentSession documentSession) : base(documentSession)
        {
        }

        protected override void OnHandled(object view, IEnvelope envelope)
        {
            ((ConcurrentViewBase)view).OnHandled(envelope.EventId);
        }

        protected override bool ShouldHandle(object view, IEnvelope envelope)
        {
            return ((ConcurrentViewBase)view).ShouldHandle(envelope.EventId);
        }
    }
}
﻿using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus.Views
{
    public class ViewWriters : IViewWriters
    {
        private readonly IDocumentStore documentStore;

        public ViewWriters(IDocumentStore documentStore)
        {
            this.documentStore = documentStore;
        }

        public IViewWriter<TView> GetWriter<TView, TViewStorage>()
            where TView : class 
            where TViewStorage : IViewStorage<TView>, new()
        {
            return new ConcurrentViewWriter<TView, TViewStorage>(documentStore);
        }
    }
}
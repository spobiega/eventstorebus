﻿using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus.Views
{
    public class SequenceCheckpointedViewWriter<TView, TViewStorage> : IViewWriter<TView>
        where TView : class 
        where TViewStorage : IViewStorage<TView>, new()
    {
        private readonly IDocumentStore documentStore;

        public SequenceCheckpointedViewWriter(IDocumentStore documentStore)
        {
            this.documentStore = documentStore;
        }

        public IViewWriterSession<TView> OpenSession()
        {
            return new SequenceCheckpointedViewWriterSession<TView, TViewStorage>(documentStore.OpenSession());
        }
    }
}
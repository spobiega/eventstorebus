﻿using System;
using EventStoreBus.Views.Api;
using Raven.Client;

namespace EventStoreBus.Views
{
    public class ViewReaders : IViewReaders
    {
        private readonly IDocumentStore documentStore;

        public ViewReaders(IDocumentStore documentStore)
        {
            this.documentStore = documentStore;
        }

        public IViewReader<TView> GetReader<TView, TViewStorage>() 
            where TView : class 
            where TViewStorage : IViewStorage<TView>, new()
        {
            return new ViewReader<TView, TViewStorage>(documentStore);
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using DurableSubscriber.Diagnostics;
using EventStoreBus.Aggregates;
using EventStoreBus.Config;
using EventStoreBus.Logging;
using EventStoreBus.Views;
using EventStoreBus.Web;
using NetMX;
using NetMX.Relation;
using NetMX.Remote;
using NetMX.Remote.HttpAdaptor;
using NetMX.Remote.Jsr262;
using Environment = System.Environment;

namespace EventStoreBus.Host
{
    public class Host
    {
        private SelfHostingHttpAdaptor httpManagementServerAdaptor;
        private INetMXConnectorServer wsmanConnectorServer;
        private IMBeanServer managementServer;
        private IStartable serviceModule;

        private static readonly ILogger logger = LogManager.GetLoggerFor<Host>();

        public void Start()
        {
            try
            {
                logger.Info("Starting...");

                var deploymentDescriptor = ReadDeploymentDescriptor();

                var service = ReadServiceDescription();
                var environment = ReadServiceEnvironmentDescription();

                var extensions = new IExtension[]
                    {
                        new WebExtension(), 
                        new AggregatesExtension(),
                        new RavenViewsExtension()
                    };

                var serviceBuilder = new ServiceBuilder(deploymentDescriptor.Id, deploymentDescriptor.Components, service, deploymentDescriptor.Version, environment, extensions);
                serviceModule = serviceBuilder.BuildService();
                var rootState = serviceModule.State;

                StartManagementServer(deploymentDescriptor, rootState);
                serviceModule.Start();

                logger.Info("Started");
            }
            catch (Exception ex)
            {
                logger.FatalException(ex, "Start failed");
                throw;
            }
        }

        public static Deployment ReadDeploymentDescriptor()
        {
            var deploymentDescriptorSerializer = new XmlSerializer(typeof(Deployment));
            using (var deploymentDescriptorStream = File.OpenRead("deployment.xml"))
            {
                return (Deployment)deploymentDescriptorSerializer.Deserialize(deploymentDescriptorStream);
            }
        }

        private void StartManagementServer(Deployment deploymentDescriptor, State rootState)
        {
            var webManagementUrl = string.Format("{0}/{1}/rest", deploymentDescriptor.BaseUrl.TrimEnd('/'), deploymentDescriptor.Id);
            var wsManagementUrl = string.Format("{0}/{1}/ws", deploymentDescriptor.BaseUrl.TrimEnd('/'), deploymentDescriptor.Id);

            managementServer = MBeanServerFactory.CreateMBeanServer(deploymentDescriptor.Id);
            managementServer.RegisterMBean(new RelationService(), RelationService.ObjectName);

            httpManagementServerAdaptor = new SelfHostingHttpAdaptor(managementServer, webManagementUrl);
            Console.WriteLine("Staring management http adaptor at {0}", webManagementUrl);
            httpManagementServerAdaptor.Start();

            var connectorServerFactory = new Jsr262ConnectorServerFactory();
            wsmanConnectorServer = connectorServerFactory.NewNetMXConnectorServer(new Uri(wsManagementUrl), managementServer);
            Console.WriteLine("Staring management ws connector at {0}", wsManagementUrl);
            wsmanConnectorServer.Start();

            StateToBeanConverter.RegisterStateBeansRecursive(managementServer, rootState);
        }

        private static Config.Environment ReadServiceEnvironmentDescription()
        {
            var deploymentSerializer = CreateSerializer(typeof(Config.Environment));
            Config.Environment environment;

            var currentPath = new DirectoryInfo(Directory.GetCurrentDirectory());
            while (true)
            {
                if (currentPath == null) //drive root
                {
                    throw new InvalidOperationException("Could not fine environment.xml file.");
                }
                if (File.Exists(Path.Combine(currentPath.FullName, "environment.xml")))
                {
                    break;
                }
                currentPath = currentPath.Parent;
            }

            using (var stream = new FileStream(Path.Combine(currentPath.FullName, "environment.xml"), FileMode.Open))
            {
                environment = (Config.Environment)deploymentSerializer.Deserialize(stream);
            }
            return environment;
        }        
        private static Service ReadServiceDescription()
        {
            using (var stream = new FileStream("service.xml", FileMode.Open))
            {
                return ConfigurationFacade.ReadServiceDescriptionFromStream(stream);
            }
        }

        private static XmlSerializer CreateSerializer(Type type)
        {
            return new XmlSerializer(type);
        }

        public void Stop()
        {
            serviceModule.Stop();
            httpManagementServerAdaptor.Stop();
            wsmanConnectorServer.Dispose();

            Console.WriteLine("Stopped...");
        }
    }
}
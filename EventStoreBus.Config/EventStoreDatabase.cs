﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "database", Namespace = Const.Namespace)]
    public class EventStoreDatabase
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}
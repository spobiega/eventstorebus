﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "commandProcessor", Namespace = Const.Namespace)]
    [XmlRoot(ElementName = "commandProcessor", Namespace = Const.Namespace)]
    public class CommandProcessor : Component
    {       
        [XmlAttribute(AttributeName = "implementation")]
        public string Implementation { get; set; }

        [XmlAttribute(AttributeName = "factory")]
        public string Factory { get; set; }

        [XmlElement(ElementName = "failureHandling", Namespace = Const.Namespace)]
        public FailureHandling FailureHandling { get; set; }

        [XmlElement(ElementName = "checkpointing", Namespace = Const.Namespace)]
        public Checkpointing Checkpointing { get; set; }
    }
}
﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    public class NoSkipFailureHandling
    {
        [XmlAttribute(AttributeName = "retries", Namespace = Const.Namespace)]
        public int Retries { get; set; }
    }
}
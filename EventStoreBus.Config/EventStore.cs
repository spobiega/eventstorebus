﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "eventStore", Namespace = Const.Namespace)]
    public class EventStore
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
       
        [XmlAttribute(AttributeName = "serializerImplementation")]
        public string SerializerImplementation { get; set; }

        [XmlArray(ElementName = "events", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "assembly", Namespace = Const.Namespace)]
        public List<Assembly> Events { get; set; }

        public EventStore()
        {
            Events = new List<Assembly>();
        }
    }
}
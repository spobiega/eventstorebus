﻿using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "componentDeployment", Namespace = Const.Namespace)]
    public class ComponentDeployment
    {
        [XmlAttribute(AttributeName = "nodeId")]
        public string NodeId { get; set; }
        [XmlAttribute(AttributeName = "deploymentId")]
        public string DeploymentId { get; set; }
        [XmlAttribute(AttributeName = "service")]
        public string Service { get; set; }
        [XmlAttribute(AttributeName = "component")]
        public string Component { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }        
    }
}
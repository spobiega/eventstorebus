﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace EventStoreBus.Config
{
    [XmlType(TypeName = "receptor", Namespace = Const.Namespace)]
    [XmlRoot(ElementName = "receptor", Namespace = Const.Namespace)]
    public class Receptor : Component
    {
        [XmlAttribute(AttributeName = "implementation")]
        public string Implementation { get; set; }

        [XmlAttribute(AttributeName = "factory")]
        public string Factory { get; set; }
        
        [XmlElement(ElementName = "failureHandling", Namespace = Const.Namespace)]
        public FailureHandling FailureHandling { get; set; }

        [XmlArray(ElementName = "subscriptions", Namespace = Const.Namespace)]
        [XmlArrayItem(ElementName = "subscription", Namespace = Const.Namespace)]
        public List<Subscription> Subscriptions { get; set; }

        [XmlAttribute(AttributeName = "viewStore")]
        public string ViewStore { get; set; }

        public Receptor()
        {
            Subscriptions = new List<Subscription>();
        }
    }
}
﻿namespace EventStoreBus.Config.Conventions
{
    public interface IConvention
    {
        void Apply(Service service);
    }
}
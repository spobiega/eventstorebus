using System.Linq;

namespace EventStoreBus.Config.Conventions
{
    public class CommandProcessorDefaultCheckpointingConvention : IConvention
    {
        private const int BatchSize = 5;

        public void Apply(Service service)
        {
            foreach (var component in service.Modules.SelectMany(x => x.Components).OfType<CommandProcessor>())
            {
                if (component.Checkpointing == null)
                {
                    component.Checkpointing = new Checkpointing()
                        {
                            BatchSize = BatchSize
                        };
                }
            }
        }
    }
}
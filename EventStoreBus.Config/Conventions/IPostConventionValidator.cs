﻿namespace EventStoreBus.Config.Conventions
{
    public interface IPostConventionValidator
    {
        void Validate(Service service);
    }
}
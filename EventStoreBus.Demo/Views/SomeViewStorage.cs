﻿using System;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Demo.Views
{
    public class SomeViewStorage : IViewStorage<SomeView>
    {
        public string CollectionName
        {
            get { return "someView"; }
        }
    }
}
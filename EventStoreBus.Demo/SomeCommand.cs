﻿using System;
using System.Runtime.Serialization;
using EventStoreBus.Api;

namespace EventStoreBus.Demo
{
    [DataContract]
    [Version(1)]
    public class SomeCommand : Command
    {
        [DataMember]
        public string AggregateId { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public DateTime InitialTimestamp { get; set; }
        [DataMember]
        public bool PrintTime { get; set; }
    }
}
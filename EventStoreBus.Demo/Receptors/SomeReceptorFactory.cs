﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Receptors.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Demo.Receptors
{
    public class SomeReceptorFactory : IReceptorFactory
    {
        public object Create(Type receptorImplementation, ICommandSender commandSender, IViewReaders viewReaders)
        {
            return new SomeReceptor(commandSender);
        }

        public void Release(object receptor)
        {
        }
    }
}
﻿using System;
using EventStoreBus.Api;
using EventStoreBus.Receptors.Api;

namespace EventStoreBus.Demo.Receptors
{
    public class SomeReceptor
    {
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger(); 

        private readonly ICommandSender commandSender;

        public SomeReceptor(ICommandSender commandSender)
        {
            this.commandSender = commandSender;
        }

        public void When(SomeExternalEvent someExternalEvent, Guid eventId)
        {
            if (someExternalEvent.Value == "bad event")
            {
                logger.Error("Bad event");
                throw new InvalidOperationException(someExternalEvent.Value);
            }
            //Console.WriteLine("Received event with value: {0}. Sending command...", someExternalEvent.Value);
            commandSender.SendTo("SomeComponent", new SomeCommand
                                              {
                                                  Id = eventId.ToString(),
                                                  AggregateId = someExternalEvent.Id,
                                                  Value = someExternalEvent.Value,
                                                  InitialTimestamp = someExternalEvent.InitialTimestamp,
                                                  PrintTime = someExternalEvent.PrintTime
                                              });
            if (someExternalEvent.PrintTime)
            {
                var diff = DateTime.Now - someExternalEvent.InitialTimestamp;
                logger.Info("Receptors processed batch in {0} ms", diff.TotalMilliseconds);
            }
        }
    }
}
﻿using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Demo.Aggregates
{
    public class SomeAggregateState : AggregateState
    {
        public int Count { get; private set; }

        public void When(SomeEvent evnt)
        {
            Count += evnt.Value.Length;
        }
    }
}
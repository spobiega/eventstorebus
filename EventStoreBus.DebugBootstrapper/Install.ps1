﻿param($installPath, $toolsPath, $package, $project)

function Set-PropertyValue {
    param(
		$Project,
		[string]$Name,
		[string]$Value
    )    
    $prop = $Project.Xml.Properties | Where-Object { $_.Name -eq $Name } | Select-Object -First 1
	if ($prop -eq $null) {
		$p.Xml.AddProperty($Name, $Value)
	} else {
		$prop.Value = $Value
	}
}

function Add-Import {
    param(
		$Project,
		[string]$File
    )    
    $imp = $Project.Xml.Imports | Where-Object { $_.Project -eq $File } | Select-Object -First 1
	if ($imp -eq $null) {
		$p.Xml.AddImport($File)
	} else {
		$imp.Project = $File
	}
}

$p = Get-MSBuildProject
Set-PropertyValue $p "StartAction" 'Program'
Set-PropertyValue $p "StartProgram" '$(MSBuildProjectDirectory)\bin\$(Configuration)\EventStoreBus.DebugBootstrapper.exe'
Set-PropertyValue $p "OctoPackNuSpecFileName" 'Service.nuspec'
Add-Import $p '$(SolutionDir)\.packaging\Packaging.targets'
$p.Save()

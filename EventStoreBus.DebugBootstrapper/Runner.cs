﻿using System;

namespace EventStoreBus.DebugBootstrapper
{
    public class Runner : MarshalByRefObject
    {
        private Host.Host host;

        public void Run()
        {
            //var programType = Type.GetType("EventStoreBus.Host.Program", true);
            //var hostType = Type.GetType("EventStoreBus.Host.Host", true);
            Host.Program.TryInitLogging();
            host = new Host.Host();
            host.Start();
        }

        public void Stop()
        {
            host.Stop();
        }
    }
}
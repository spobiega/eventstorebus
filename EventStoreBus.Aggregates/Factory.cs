﻿using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Aggregates
{
    public class Factory<TAggregate> : IFactory<TAggregate>
        where TAggregate : IAggregate, new()
    {
        public IAggregateReference<TAggregate> Create(string id)
        {
            var instance = new TAggregate();
            instance.SetId(id);

            return new NewAggregateReference<TAggregate>(id, instance);
        }
    }
}
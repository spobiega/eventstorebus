﻿using System;
using DurableSubscriber;
using EventStoreBus.Aggregates.Api;
using EventStoreBus.Api;

namespace EventStoreBus.Aggregates
{
    public class AggregateComponentFactory<TComponentFactory> : IAutonomousComponentFactory
        where TComponentFactory : IAggregateComponentFactory, new()
    {
        private static readonly IAggregateComponentFactory factoryImpl = new TComponentFactory();

        public object Create(Type componentImplementation, IEventStoreConnectionAccessor domainStoreConnection, ISerializer domainStoreEventSerializer, ICommandSender commandSender)
        {
            var factories = new Factories();
            return factoryImpl.Create(componentImplementation, 
                new Repositories(domainStoreConnection, domainStoreEventSerializer, factories),
                factories);
        }

        public void Release(object component)
        {
            factoryImpl.Release(component);
        }

        private class Factories : IFactories
        {
            public IFactory<TAggregate> Get<TAggregate>() 
                where TAggregate : IAggregate, new()
            {
                return new Factory<TAggregate>();
            }
        }

        private class Repositories : IRepositories
        {
            private readonly IEventStoreConnectionAccessor domainStoreConnection;
            private readonly ISerializer domainStoreEventSerializer;
            private readonly Factories factories;

            public Repositories(IEventStoreConnectionAccessor domainStoreConnection, ISerializer domainStoreEventSerializer, Factories factories)
            {
                this.domainStoreConnection = domainStoreConnection;
                this.domainStoreEventSerializer = domainStoreEventSerializer;
                this.factories = factories;
            }

            public IRepository<TAggregate> Get<TAggregate, TStorage>()
                where TAggregate : IAggregate, new()
                where TStorage : IAggregateStorage<TAggregate>, new()
            {
                return new Repository<TAggregate, TStorage>(factories.Get<TAggregate>(), domainStoreConnection, domainStoreEventSerializer);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventStoreBus.Aggregates.Api;

namespace EventStoreBus.Aggregates
{
    public class NewAggregateReference<T> : IAggregateReference<T>
        where T : IAggregate
    {
        private readonly T instance;
        private readonly string aggregateId;

        public NewAggregateReference(string aggregateId, T instance)
        {
            this.instance = instance;
            this.aggregateId = aggregateId;
        }

        public bool HasCommandAlreadyBeenProcessed(string commandId)
        {
            return false;
        }

        public string AggregateId
        {
            get { return aggregateId; }
        }

        public int? LoadedVersion
        {
            get { return null; }
        }

        public T Instance
        {
            get { return instance; }
        }
    }
}
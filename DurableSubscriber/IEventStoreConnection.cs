﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventStore.ClientAPI;

namespace DurableSubscriber
{
    public interface IEventStoreConnection
    {
        void AppendToStream(string stream, int expectedVersion, params EventData[] events);
        void AppendToStream(string stream, int expectedVersion, IEnumerable<EventData> events);
        Task AppendToStreamAsync(string stream, int expectedVersion, params EventData[] events);
        Task AppendToStreamAsync(string stream, int expectedVersion, IEnumerable<EventData> events);
        StreamEventsSlice ReadStreamEventsForward(string stream, int start, int count, bool resolveLinkTos);
        Task<StreamEventsSlice> ReadStreamEventsForwardAsync(string stream, int start, int count, bool resolveLinkTos);
        StreamEventsSlice ReadStreamEventsBackward(string stream, int start, int count, bool resolveLinkTos);
        Task<StreamEventsSlice> ReadStreamEventsBackwardAsync(string stream, int start, int count, bool resolveLinkTos);
        AllEventsSlice ReadAllEventsForward(Position position, int maxCount, bool resolveLinkTos);
        Task<AllEventsSlice> ReadAllEventsForwardAsync(Position position, int maxCount, bool resolveLinkTos);

        EventStoreSubscription SubscribeToStream(string stream, bool resolveLinkTos,
                                                       Action<ResolvedEvent> eventAppeared,
                                                       Action subscriptionDropped = null);

        EventStoreSubscription SubscribeToAll(bool resolveLinkTos, Action<ResolvedEvent> eventAppeared,
                                                           Action subscriptionDropped = null);
    }
}
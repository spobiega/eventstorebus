﻿using System;
using System.Collections.Generic;
using System.Linq;
using DurableSubscriber.Diagnostics;
using EventStore.ClientAPI;

namespace DurableSubscriber
{
    public class AllEventStreamsPullSource : IPullSource<Position>
    {
        private const int SliceSize = 100;
        private readonly IEventStoreConnectionAccessor connection;
        private readonly ICheckpointingStrategy<Position> checkpointingStrategy;
        private Position currentPosition;

        public AllEventStreamsPullSource(IEventStoreConnectionAccessor connection, ICheckpointingStrategy<Position> checkpointingStrategy)
        {
            this.connection = connection;
            this.checkpointingStrategy = checkpointingStrategy;
        }

        public void Seek(Position position)
        {
            currentPosition = position;
        }

        public void Reset()
        {
            currentPosition = Position.Start;
        }

        public IEnumerable<Tuple<object, Position>> ReadSlice()
        {
            if (currentPosition == Position.Start)
            {
                currentPosition = checkpointingStrategy.LoadState();
            }
            AllEventsSlice slice;
            if (!connection.TryInvoke(x => x.ReadAllEventsForward(currentPosition, SliceSize, true), out slice))
            {
                throw new EventStoreConnectionException();
            }
            var previousPosition = currentPosition;
            currentPosition = slice.NextPosition;
            return slice.Events.Select(x => Tuple.Create((object)x.Event, previousPosition));
        }

        public State State
        {
            get 
            {
                return new State("All streams pull source", GetType())
                    .WithAttribute("CommitPostion", () => currentPosition == Position.Start 
                                                              ? "Unknown" 
                                                              : currentPosition.CommitPosition.ToString())
                    .WithAttribute("PreparePosition", () => currentPosition == Position.Start 
                                                                ? "Unknown" 
                                                                : currentPosition.PreparePosition.ToString());
            }
        }
    }
}
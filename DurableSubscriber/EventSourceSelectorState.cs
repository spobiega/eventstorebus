﻿namespace DurableSubscriber
{
    public enum EventSourceSelectorState
    {
        NotStarted,
        Pulling,
        Synchronizing,
        Receiving
    }
}
﻿using System;
using System.IO;
using EventStore.ClientAPI;

namespace DurableSubscriber
{
    public class AllStreamsEventStoreCheckpointingStrategy : EventStoreCheckpointingStrategy<Position>
    {
        public AllStreamsEventStoreCheckpointingStrategy(IEventStoreConnectionAccessor connection, string streamName) : base(connection, streamName)
        {
        }

        protected override byte[] SerializeState(Position state)
        {
            var preparePart = BitConverter.GetBytes(state.PreparePosition);
            var commitPart = BitConverter.GetBytes(state.CommitPosition);
            var result = new byte[16];
            preparePart.CopyTo(result,0);
            commitPart.CopyTo(result,8);
            return result;
        }

        protected override Position DeserializeState(byte[] data)
        {
            var preparePart = BitConverter.ToInt64(data, 0);
            var commitPart = BitConverter.ToInt64(data, 8);
            return new Position(commitPart, preparePart);
        }

        protected override Position GetBeginState()
        {
            return Position.Start;
        }
    }
}
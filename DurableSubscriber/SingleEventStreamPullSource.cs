﻿using System;
using System.Collections.Generic;
using System.Linq;
using DurableSubscriber.Diagnostics;
using EventStore.ClientAPI;

namespace DurableSubscriber
{
    public class SingleEventStreamPullSource : IPullSource<int>
    {
        private const int SliceSize = 100;
        private readonly IEventStoreConnectionAccessor connection;
        private int currentPosition = int.MinValue;
        private readonly string streamName;
        private readonly ICheckpointingStrategy<int> checkpointingStrategy;

        public SingleEventStreamPullSource(IEventStoreConnectionAccessor connection, string streamName, ICheckpointingStrategy<int> checkpointingStrategy)
        {
            this.connection = connection;
            this.streamName = streamName;
            this.checkpointingStrategy = checkpointingStrategy;
        }

        public virtual IEnumerable<Tuple<object, int>> ReadSlice()
        {
            if (currentPosition == int.MinValue)
            {
                currentPosition = checkpointingStrategy.LoadState()+1;
            }
            StreamEventsSlice slice;
            if (!connection.TryInvoke(x => x.ReadStreamEventsForward(streamName, currentPosition, SliceSize, true), out slice))
            {
                throw new EventStoreConnectionException();
            }
            if (slice.Status == SliceReadStatus.Success)
            {
                currentPosition = slice.NextEventNumber;
            }
            return slice.Events.Select(x => new Tuple<object, int>(x.Event, x.OriginalEventNumber));
        }

        public void Seek(int position)
        {
            currentPosition = position;
        }

        public void Reset()
        {
            currentPosition = int.MinValue;
        }

        public State State
        {
            get
            {
                return new State("Pull source for " + streamName, GetType())
                    .WithProperty("Source", streamName)
                    .WithAttribute("Postion", () => currentPosition == int.MinValue ? "Unknown" : currentPosition.ToString())
                    .WithAttribute("SliceSize", () => SliceSize);
            }
        }
    }
}
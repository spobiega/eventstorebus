﻿using System;

namespace DurableSubscriber
{
    public class BatchEventStoreCheckpointingStrategy<T> : ICheckpointingStrategy<T>
    {
        private readonly ICheckpointingStrategy<T> implementation;
        private readonly int threshold;
        private int counter;

        public BatchEventStoreCheckpointingStrategy(ICheckpointingStrategy<T> implementation, int threshold)
        {
            this.implementation = implementation;
            this.threshold = threshold;
        }

        public void NotifyEventsProcessed(T until)
        {
            if (counter < threshold)
            {
                counter++;
            }
            else
            {
                counter = 0;
                implementation.NotifyEventsProcessed(until);
            }
        }

        public T LoadState()
        {
            return implementation.LoadState();
        }
    }
}
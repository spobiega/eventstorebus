﻿using System;
using EventStore.ClientAPI;

namespace DurableSubscriber
{
    public abstract class EventStoreCheckpointingStrategy<T> : ICheckpointingStrategy<T>
    {
        private readonly IEventStoreConnectionAccessor connection;
        private readonly string checkpointingStreamName;

        protected EventStoreCheckpointingStrategy(IEventStoreConnectionAccessor connection, string streamName)
        {
            this.connection = connection;
            checkpointingStreamName = streamName + "-checkpointing";
        }

        protected abstract byte[] SerializeState(T state);
        protected abstract T DeserializeState(byte[] data);
        protected abstract T GetBeginState();

        public void NotifyEventsProcessed(T until)
        {
            var evnt = new EventData(Guid.NewGuid(),"esb-checkpoint",false, SerializeState(until), new byte[0]);
            connection.TryInvoke(x => x.AppendToStreamAsync(checkpointingStreamName, ExpectedVersion.Any, evnt));
        }

        public T LoadState()
        {
            StreamEventsSlice slice;
            if (!connection.TryInvoke(x => x.ReadStreamEventsBackward(checkpointingStreamName, StreamPosition.End, 1, false), out slice))
            {
                throw new EventStoreConnectionException();
            }
            if (slice.Status == SliceReadStatus.Success && slice.Events.Length > 0)
            {
                return DeserializeState(slice.Events[0].Event.Data);
            }
            return GetBeginState();
        }
    }
}
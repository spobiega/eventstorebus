﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventStore.ClientAPI;

namespace DurableSubscriber
{
    public class DefualtEventStoreConnection : IEventStoreConnection
    {
        private readonly EventStoreConnection connection;

        public DefualtEventStoreConnection(EventStoreConnection connection)
        {
            this.connection = connection;
        }

        public void AppendToStream(string stream, int expectedVersion, params EventData[] events)
        {
            connection.AppendToStream(stream, expectedVersion, events);
        }

        public void AppendToStream(string stream, int expectedVersion, IEnumerable<EventData> events)
        {
            connection.AppendToStream(stream, expectedVersion, events);
        }

        public Task AppendToStreamAsync(string stream, int expectedVersion, params EventData[] events)
        {
            return connection.AppendToStreamAsync(stream, expectedVersion, events);
        }

        public Task AppendToStreamAsync(string stream, int expectedVersion, IEnumerable<EventData> events)
        {
            return connection.AppendToStreamAsync(stream, expectedVersion, events);
        }

        public StreamEventsSlice ReadStreamEventsForward(string stream, int start, int count, bool resolveLinkTos)
        {
            return connection.ReadStreamEventsForward(stream, start, count, resolveLinkTos);
        }

        public Task<StreamEventsSlice> ReadStreamEventsForwardAsync(string stream, int start, int count, bool resolveLinkTos)
        {
            return connection.ReadStreamEventsForwardAsync(stream, start, count, resolveLinkTos);
        }

        public StreamEventsSlice ReadStreamEventsBackward(string stream, int start, int count, bool resolveLinkTos)
        {
            return connection.ReadStreamEventsBackward(stream, start, count, resolveLinkTos);
        }

        public Task<StreamEventsSlice> ReadStreamEventsBackwardAsync(string stream, int start, int count, bool resolveLinkTos)
        {
            return connection.ReadStreamEventsBackwardAsync(stream, start, count, resolveLinkTos);
        }

        public AllEventsSlice ReadAllEventsForward(Position position, int maxCount, bool resolveLinkTos)
        {
            return connection.ReadAllEventsForward(position, maxCount, resolveLinkTos);
        }

        public Task<AllEventsSlice> ReadAllEventsForwardAsync(Position position, int maxCount, bool resolveLinkTos)
        {
            return connection.ReadAllEventsForwardAsync(position, maxCount, resolveLinkTos);
        }

        public EventStoreSubscription SubscribeToStream(string stream, bool resolveLinkTos, Action<ResolvedEvent> eventAppeared, Action subscriptionDropped)
        {
            return connection.SubscribeToStream(stream, resolveLinkTos, eventAppeared, subscriptionDropped).Result;
        }

        public EventStoreSubscription SubscribeToAll(bool resolveLinkTos, Action<ResolvedEvent> eventAppeared, Action subscriptionDropped)
        {
            return connection.SubscribeToAll(resolveLinkTos, eventAppeared, subscriptionDropped).Result;
        }
    }
}
﻿using System;
using DurableSubscriber.Diagnostics;
using EventStore.ClientAPI;

namespace DurableSubscriber
{
    public class AllEventStreamsPushSource : IPushSource<Position>
    {
        private readonly IEventStoreConnectionAccessor connection;
        private Position lastRecordedPosition = Position.Start;
        private bool running;
        private EventStoreSubscription subscription;

        public AllEventStreamsPushSource(IEventStoreConnectionAccessor connection)
        {
            this.connection = connection;
        }

        public event EventHandler<RecordedEventEventArgs<Position>> Event;
        public event EventHandler<SubscriptionDroppedEventArgs<Position>> SubscriptionDropped;

        public void Start()
        {
            if (!connection.TryInvoke(x => x.SubscribeToAll(true, OnEvent, OnDropped), out subscription))
            {
                OnDropped();
            }
            else
            {
                running = true;
            }            
        }

        private void OnDropped()
        {
            if (SubscriptionDropped != null)
            {
                SubscriptionDropped(this, new SubscriptionDroppedEventArgs<Position>(lastRecordedPosition));
            }
        }

        private void OnEvent(ResolvedEvent resolvedEvent)
        {
            lastRecordedPosition = resolvedEvent.OriginalPosition.Value;
            if (Event != null)
            {
                Event(this, new RecordedEventEventArgs<Position>(resolvedEvent.Event, lastRecordedPosition));
            }
        }

        public void Stop()
        {
            if (subscription != null)
            {
                subscription.Unsubscribe();
            }
            running = false;
        }

        public State State
        {
            get
            {
                return new State("All streams push source", GetType())
                    .WithAttribute("Running", () => running)
                    .WithAttribute("CommitPostion", () => lastRecordedPosition.CommitPosition)
                    .WithAttribute("PreparePosition", () => lastRecordedPosition.PreparePosition);
            }
        }
    }
}
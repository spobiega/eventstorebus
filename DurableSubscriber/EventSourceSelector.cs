using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DurableSubscriber.Diagnostics;

namespace DurableSubscriber
{
    public class EventSourceSelector<T> : IInstrumentable
    {
        private CancellationTokenSource cancellationTokenSource;
        private readonly IPullSource<T> pullSource;
        private readonly IPushSource<T> pushSource;
        private readonly EventBuffer<T> buffer;
        private Task processingTask;
        private EventSourceSelectorState state;

        public EventSourceSelector(IPullSource<T> pullSource, IPushSource<T> pushSource, IEventIdentityExtractor eventIdentityExtractor)
        {
            this.pullSource = pullSource;
            this.pushSource = pushSource;
            pushSource.Event += OnEvent;
            pushSource.SubscriptionDropped += OnSubscriptionDropped;
            buffer = new EventBuffer<T>(eventIdentityExtractor);
        }

        private void OnSubscriptionDropped(object sender, SubscriptionDroppedEventArgs<T> e)
        {
            Stop();            
            pullSource.Seek(e.LastRecordedPosition);
            Start();
        }

        private void OnEvent(object sender, RecordedEventEventArgs<T> e)
        {
            buffer.Add(new Tuple<object, T>(e.Event, e.Position));
        }

        public event EventHandler<RecordedEventEventArgs<T>> Event;

        public void Start()
        {
            pullSource.Reset();
            cancellationTokenSource = new CancellationTokenSource();
            buffer.Reset();
            processingTask = Task.Factory
                .StartNew(ReadAllFromPullSource)
                .ContinueWith(x => StartPushSource(), cancellationTokenSource.Token)
                .ContinueWith(x => ReadOnceFromPullSource(), cancellationTokenSource.Token)
                .ContinueWith(x => StartProcessingFromPushSource(x.Result), cancellationTokenSource.Token);
        }

        private void StartProcessingFromPushSource(IEnumerable<Tuple<object, T>> lastPulledSlice)
        {
            state = EventSourceSelectorState.Receiving;
            var processorTask = buffer.StartProcessing(lastPulledSlice, ProcessEvent);
            processorTask.Wait();
        }

        private IEnumerable<Tuple<object, T>> ReadOnceFromPullSource()
        {
            var slice = pullSource.ReadSlice();
            ProcessEvents(slice);
            return slice;
        }
        
        private void StartPushSource()
        {
            state = EventSourceSelectorState.Synchronizing;
            pushSource.Start();
        }

        private void ReadAllFromPullSource()
        {
            state = EventSourceSelectorState.Pulling;
            while (!cancellationTokenSource.Token.IsCancellationRequested)
            {
                try
                {
                    var slice = pullSource.ReadSlice();
                    if (!slice.Any())
                    {
                        break;
                    }
                    ProcessEvents(slice);
                }
                catch (EventStoreConnectionException)
                {
                    //Still no connection, sleeping
                    Thread.Sleep(1000);
                }
            }
        }

        private void ProcessEvents(IEnumerable<Tuple<object , T>> events)
        {
            foreach (var evnt in events)
            {
                ProcessEvent(evnt);
            }
        }

        private void ProcessEvent(Tuple<object, T> evnt)
        {
            if (Event != null)
            {
                Event(this, new RecordedEventEventArgs<T>(evnt.Item1, evnt.Item2));
            }
        }

        public void Stop()
        {
            cancellationTokenSource.Cancel();
            pushSource.Stop();
            buffer.Complete();
            try
            {
                processingTask.Wait();
            }
            catch (AggregateException ex)
            {
                //Swallow OperationCancelledException
            }
            state = EventSourceSelectorState.NotStarted;
        }

        public State State
        {
            get { return new State("Source selector", GetType())
                .WithAttribute("State", () => state.ToString())            
                .WithChild(pullSource.State)
                .WithChild(pushSource.State)
                .WithChild(buffer.State);}
        }
    }
}
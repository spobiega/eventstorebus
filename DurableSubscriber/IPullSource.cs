﻿using System;
using System.Collections.Generic;
using DurableSubscriber.Diagnostics;

namespace DurableSubscriber
{
    public interface IPullSource<T> : IInstrumentable
    {
        IEnumerable<Tuple<object,T>> ReadSlice();
        void Seek(T position);
        void Reset();
    }
}
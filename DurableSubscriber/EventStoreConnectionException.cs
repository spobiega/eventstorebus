﻿using System;
using System.Runtime.Serialization;

namespace DurableSubscriber
{
    [Serializable]
    public class EventStoreConnectionException : Exception
    {
        public EventStoreConnectionException()
        {
        }

        public EventStoreConnectionException(string message) : base(message)
        {
        }

        public EventStoreConnectionException(string message, Exception inner) : base(message, inner)
        {
        }

        protected EventStoreConnectionException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
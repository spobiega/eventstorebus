﻿using System;

namespace EventStoreBus.Aggregates.Api
{
    public static class RepositoryExtensions
    {
        public static void Invoke<T>(this IRepository<T> repository, string aggregateId, string commandId, Action<T> action)
            where T : IAggregate
        {
            var reference = repository.Find(aggregateId) ?? repository.Factory.Create(aggregateId);

            if (!reference.HasCommandAlreadyBeenProcessed(commandId))
            {
                action(reference.Instance);
                repository.SaveChanges(reference, commandId);
            }
            
        }

        public static void InvokeEnsuringNew<T>(this IRepository<T> repository, string newAggregateId, string commandId, Action<T> action)
            where T : IAggregate
        {
            var existing = repository.Find(newAggregateId);
            if (existing != null)
            {
                if (existing.HasCommandAlreadyBeenProcessed(commandId))
                {
                    return; //We already processed this command.
                }
                throw new InvalidOperationException(string.Format("Aggregate {0} already exists and was not created by this command.", newAggregateId));
            }
            var reference = repository.Factory.Create(newAggregateId);

            action(reference.Instance);
            repository.SaveChanges(reference, commandId);
        }
        
        public static void InvokeEnsuringExists<T>(this IRepository<T> repository, string aggregateId, string commandId, Action<T> action)
            where T : IAggregate
        {
            var reference = repository.Find(aggregateId);
            if (reference == null)
            {
                throw new InvalidOperationException(string.Format("Aggregate {0} not found.", aggregateId));
            }

            if (!reference.HasCommandAlreadyBeenProcessed(commandId))
            {
                action(reference.Instance);
                repository.SaveChanges(reference, commandId);
            }
        }
    }
}
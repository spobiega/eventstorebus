﻿namespace EventStoreBus.Aggregates.Api
{
    public interface IRepositories
    {
        IRepository<TAggregate> Get<TAggregate, TStorage>()
            where TStorage : IAggregateStorage<TAggregate>, new()
            where TAggregate : IAggregate, new();
    }

    public static class RepositoriesExtensions
    {
        public static IRepository<TAggregate> Get<TAggregate>(this IRepositories repositories)
            where TAggregate : IAggregate, new()
        {
            return repositories.Get<TAggregate, DefaultAggregateStorage<TAggregate>>();
        }
    }
}
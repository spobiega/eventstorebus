﻿using System.Collections.Generic;
using EventStoreBus.Api;

namespace EventStoreBus.Aggregates.Api
{
    public interface IAggregate
    {
        void SetId(string id);
        void LoadState(IEnumerable<IEvent> events);
        IEnumerable<IEvent> Changes { get; }
        object State { get; }
    }
}
﻿namespace EventStoreBus.Aggregates.Api
{
    public class DefaultAggregateStorage<T> : IAggregateStorage<T> where T 
        : IAggregate
    {
        public static IAggregateStorage<T> Instance = new DefaultAggregateStorage<T>();

        private static readonly string typeName = typeof(T).Name;

        public string MakeStreamId(string aggregateId)
        {
            return string.Format("{0}[{1}]", typeName, aggregateId);
        }
    }
}
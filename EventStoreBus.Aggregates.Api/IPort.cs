﻿using EventStoreBus.Api;

namespace EventStoreBus.Aggregates.Api
{
    public interface IPort
    {
        void LoadState(object state);
        void Handle(IEvent evnt);
    }
}
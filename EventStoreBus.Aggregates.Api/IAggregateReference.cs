﻿namespace EventStoreBus.Aggregates.Api
{
    public interface IAggregateReference<out T>
        where T : IAggregate
    {
        bool HasCommandAlreadyBeenProcessed(string commandId);
        string AggregateId { get; }
        int? LoadedVersion { get; }
        T Instance { get; }
    }
}
﻿namespace EventStoreBus.Aggregates.Api
{
    public interface IFactory<T>
        where T : IAggregate
    {
        IAggregateReference<T> Create(string id);
    }
}
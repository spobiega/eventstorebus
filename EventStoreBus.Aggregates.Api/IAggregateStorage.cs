﻿using System.Collections.Generic;
using EventStoreBus.Api;

namespace EventStoreBus.Aggregates.Api
{
    public interface IAggregateStorage<out T>
        where T: IAggregate
    {
        string MakeStreamId(string aggregateId);
    }
}
﻿using System.Collections.Generic;
using EventStoreBus.Api;

namespace EventStoreBus.Aggregates.Api
{
    public interface IAggregateFactory<T>
        where T : IAggregate
    {
        //T CreateRoot();
        //void ReleaseRoot(T root);

        IEnumerable<IPort> CreatePorts(ICommandSender commandSender);
        void ReleasePort(IPort port);
    }
}
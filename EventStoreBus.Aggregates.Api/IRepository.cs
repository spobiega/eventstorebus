﻿using System;

namespace EventStoreBus.Aggregates.Api
{
    public interface IRepository<T>
        where T : IAggregate
    {
        IAggregateReference<T> Find(string aggregateId);
        IFactory<T> Factory { get; }
        void SaveChanges(IAggregateReference<T> aggregateReference, string commandId);

        void Invoke(string aggregateId, string commandId, Action<T> action);
        void InvokeEnsuringNew(string newAggregateId, string commandId, Action<T> action);
        void InvokeEnsuringExists(string aggregateId, string commandId, Action<T> action);
    }
}
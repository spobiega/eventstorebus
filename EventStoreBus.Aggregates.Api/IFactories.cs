﻿namespace EventStoreBus.Aggregates.Api
{
    public interface IFactories
    {
        IFactory<TAggregate> Get<TAggregate>()
            where TAggregate : IAggregate, new();
    }
}
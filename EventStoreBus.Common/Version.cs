﻿using System;

namespace EventStoreBus.Common
{
    public struct Version
    {
        private readonly int major;
        private readonly int minor;
        private readonly int patch;

        public Version(int major, int minor, int patch)
        {
            this.major = major;
            this.patch = patch;
            this.minor = minor;
        }

        public static Version Parse(string versionNumber)
        {
            var parts = versionNumber.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            var major = int.Parse(parts[0]);
            var minor = parts.Length > 1
                            ? int.Parse(parts[1])
                            : 0;
            var patch = parts.Length > 2
                            ? int.Parse(parts[2])
                            : 0;
            return new Version(major, minor, patch);
        }

        public bool IsCompatible(Version other)
        {
            return major == other.major
                   && minor == other.minor;
        }

        public int Major
        {
            get { return major; }
        }

        public int Minor
        {
            get { return minor; }
        }

        public int Patch
        {
            get { return patch; }
        }

        public bool Equals(Version other)
        {
            return other.major == major && other.minor == minor && other.patch == patch;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (obj.GetType() != typeof (Version)) return false;
            return Equals((Version) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = major;
                result = (result*397) ^ minor;
                result = (result*397) ^ patch;
                return result;
            }
        }

        public static implicit operator Version(string versionNumber)
        {
            return Parse(versionNumber);
        }

        public static bool operator <(Version left, Version right)
        {
            if (left.major == right.major)
            {
                if (left.minor == right.minor)
                {
                    return left.patch < right.patch;
                }
                return left.minor < right.minor;
            }
            return left.major < right.major;
        }
        public static bool operator <=(Version left, Version right)
        {
            if (left.major == right.major)
            {
                if (left.minor == right.minor)
                {
                    return left.patch <= right.patch;
                }
                return left.minor <= right.minor;
            }
            return left.major <= right.major;
        }

        public static bool operator >(Version left, Version right)
        {
            if (left.major == right.major)
            {
                if (left.minor == right.minor)
                {
                    return left.patch > right.patch;
                }
                return left.minor > right.minor;
            }
            return left.major > right.major;
        }

        public static bool operator >=(Version left, Version right)
        {
            if (left.major == right.major)
            {
                if (left.minor == right.minor)
                {
                    return left.patch >= right.patch;
                }
                return left.minor >= right.minor;
            }
            return left.major >= right.major;
        }

        public static bool operator ==(Version left, Version right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Version left, Version right)
        {
            return !left.Equals(right);
        }

        public override string ToString()
        {
            return string.Format("{0}.{1}.{2}", major, minor, patch);
        }
    }
}

// Copyright (c) 2012, Event Store LLP
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// 
// Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// Neither the name of the Event Store LLP nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
using System;
using NLog.Config;

namespace EventStoreBus.Logging
{
    public static class LogManager
    {
        private static bool initialized;
        public static ILogger GlobalLogger = GetLogger("GLOBAL");

        public static ILogger GetLoggerFor(Type type)
        {
            return GetLogger(type.Name);
        }

        public static ILogger GetLoggerFor<T>()
        {
            return GetLogger(typeof(T).Name);
        }

        public static ILogger GetLogger(string logName)
        {
            return new LazyLogger(() => new NLogger(logName));
        }

        public static void Init(string componentName, string logsDirectory)
        {
            if (componentName == null)
            {
                throw new ArgumentNullException("componentName");
            }
            if (initialized)
                throw new InvalidOperationException("Cannot initialize twice");

            initialized = true;

            var config = new LoggingConfiguration();

            var coloredConsoleTarget = new NLog.Targets.ColoredConsoleTarget
                                           {
                                               Layout =
                                                   @"[${pad:padCharacter=0:padding=5:inner=${processid}},${pad:padCharacter=0:padding=2:inner=${threadid}},${date:universalTime=true:format=HH\:mm\:ss\.fff}] ${message}${onexception:${newline}EXCEPTION OCCURED:${newline}${exception:format=message}}"
                                           };
            var fileTarget = new NLog.Targets.FileTarget()
                                 {
                                     Layout = @"[PID:${pad:padCharacter=0:padding=2:inner=${processid}} ${date:universalTime=true:format=yyyy\.MM\.dd HH\:mm\:ss\.fff} ${level} ${logger} ${threadid}] ${message}${onexception:${newline}EXCEPTION OCCURED:${newline}${exception:format=tostring:innerFormat=tostring:maxInnerExceptionLevel=20}}",
                                     FileName = string.Format(@"{0}\{1}-${{shortdate:universalTime=true}}.txt", logsDirectory, componentName)
                                 };

            config.AddTarget("colored-console",coloredConsoleTarget);
            config.AddTarget("file",fileTarget);

            config.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Debug, coloredConsoleTarget));
            config.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Debug, fileTarget));

            NLog.LogManager.Configuration = config;
            RegisterGlobalExceptionHandler();
        }

        private static void RegisterGlobalExceptionHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += (s, e) =>
            {
                var exc = e.ExceptionObject as Exception;
                if (exc != null)
                    GlobalLogger.FatalException(exc, "Global Unhandled Exception occurred.");
                else
                    GlobalLogger.Fatal("Global Unhandled Exception object: {0}.", e.ExceptionObject);
                GlobalLogger.Flush(TimeSpan.FromMilliseconds(500));
            };
        }

        public static void Finish()
        {
            NLogger.FlushLog();
            NLog.LogManager.Configuration = null;
        }
    }
}

﻿using System;

namespace EventStoreBus.Logging
{
    public static class LoggerExtensions
    {
        public static void Debug(this ILogger logger, string message, Action<IFormat> formatArguments)
        {
            var format = new Format();
            formatArguments(format);
            logger.Debug(string.Format(message, format.Arguments));
        }
        
        public static void Trace(this ILogger logger, string message, Action<IFormat> formatArguments)
        {
            var format = new Format();
            formatArguments(format);
            logger.Trace(string.Format(message, format.Arguments));
        }
        
        private class Format : IFormat
        {
            private object[] arguments;

            public object[] Arguments
            {
                get { return arguments; }
            }

            public void Args(params object[] formatArguments)
            {
                this.arguments = formatArguments;
            }
        }
    }
}
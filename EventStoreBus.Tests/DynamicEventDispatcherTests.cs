using System;
using NUnit.Framework;

// ReSharper disable InconsistentNaming
namespace EventStoreBus.Tests
{
    [TestFixture]
    public class DynamicEventDispatcherTests
    {
        [Test]
        public void It_calls_appropriate_method()
        {
            var target = new Target();
            var dispatcher = new DynamicDispatcher(typeof(Target));

            dispatcher.Handle(target, 7);

            Assert.AreEqual(7,target.LastArgument);
        }

        public class Target
        {
            public int LastArgument { get; set; }

            public void When(int argument)
            {
                LastArgument = argument;
            }
        }
    }
}
// ReSharper restore InconsistentNaming

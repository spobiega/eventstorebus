﻿using System;
using DurableSubscriber;
using EventStoreBus.Api;
using EventStoreBus.Receptors.Api;

namespace EventStoreBus.Receptors
{
    public class StatelessReceptorFactory<T> : IReceptorFactory
        where T : IStatelessReceptorFactory, new()
    {
        private static readonly IStatelessReceptorFactory factoryImpl = new T();

        public object Create(Type receptorImplementation, ICommandSender commandSender)
        {
            return factoryImpl.Create(receptorImplementation, commandSender);
        }

        public void Release(object receptor)
        {
        }
    }
}

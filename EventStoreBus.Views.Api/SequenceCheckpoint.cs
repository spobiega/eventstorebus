﻿namespace EventStoreBus.Views.Api
{
    public class SequenceCheckpoint
    {
        public string StoreId { get; set; }
        public string StreamId { get; set; }
        public int Sequence { get; set; }
    }
}
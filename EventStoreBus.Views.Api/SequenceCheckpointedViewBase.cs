﻿using System.Collections.Generic;
using System.Linq;

namespace EventStoreBus.Views.Api
{
    public abstract class SequenceCheckpointedViewBase
    {
        public List<SequenceCheckpoint> Checkpoints = new List<SequenceCheckpoint>();

        public bool ShouldHandle(string storeId, string streamId, int sequence)
        {
            var checkpoint = Checkpoints.FirstOrDefault(x => x.StreamId == streamId && x.StoreId == storeId);
            return checkpoint == null || checkpoint.Sequence < sequence;
        }

        public void OnHandled(string storeId, string streamId, int sequence)
        {
            var checkpoint = Checkpoints.FirstOrDefault(x => x.StreamId == streamId && x.StoreId == storeId);
            if (checkpoint == null)
            {
                Checkpoints.Add(new SequenceCheckpoint()
                                    {
                                        Sequence = sequence,
                                        StreamId = streamId,
                                        StoreId = storeId
                                    });
            }
            else
            {
                checkpoint.Sequence = sequence;
            }
        }
    }
}
﻿using System;

namespace EventStoreBus.Api
{
    public interface IStatelessComponentFactory
    {
        object Create(Type componentImplementation, IEventPublisher eventPublisher);
        void Release(object component);
    }
}
﻿using System.Collections;

namespace EventStoreBus.Views.Api
{
    public interface IViewWriter<TView>
        where TView : class 
    {
        IViewWriterSession<TView> OpenSession();
    }
}
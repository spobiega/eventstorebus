﻿namespace EventStoreBus.Api
{
    public interface IEventPublisher
    {
        void Publish(string streamId, IEvent evnt, string commandId);
    }
}
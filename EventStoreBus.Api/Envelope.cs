﻿using System;

namespace EventStoreBus.Api
{
    public class Envelope<T> : IEnvelope
        where T : IEvent
    {
        private readonly string streamId;
        private readonly Guid eventId;
        private readonly int sequence;
        private readonly T payload;
        private readonly string store;

        public Envelope(T payload, string store, string streamId, int sequence, Guid eventId)
        {
            this.streamId = streamId;
            this.payload = payload;
            this.store = store;
            this.sequence = sequence;
            this.eventId = eventId;
        }

        public string StreamId
        {
            get { return streamId; }
        }

        public Guid EventId
        {
            get { return eventId; }
        }

        public int Sequence
        {
            get { return sequence; }
        }

        public T Payload
        {
            get { return payload; }
        }

        public string Store
        {
            get { return store; }
        }
    }
}
﻿namespace EventStoreBus.Views.Api
{
    public static class ViewReaderSessionExtensions
    {
        public static TView LoadSingle<TView>(this IViewReaderSession<TView> session)
            where TView : class, new()
        {
            return session.Load("single") ?? new TView();
        }
    }
}
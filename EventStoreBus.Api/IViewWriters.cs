﻿namespace EventStoreBus.Views.Api
{
    public interface IViewWriters
    {
        IViewWriter<TView> GetWriter<TView, TViewStorage>()
            where TView : class 
            where TViewStorage : IViewStorage<TView>, new();
    }
}
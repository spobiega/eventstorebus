﻿using System;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Api
{
    public interface IViewProjectionFactory
    {
        object Create(Type viewProjectionImplementation, IViewWriters viewWriters);
        void Release(object projection);
    }
}
﻿using System;
using System.Runtime.Serialization;

namespace EventStoreBus.Api
{
    [Serializable]
    [DataContract]
    public abstract class Command : ICommand
    {
        [DataMember]
        public string Id { get; set; }

        protected Command(string commandId)
        {
            Id = commandId;
        }

        protected Command()
        {
        }
    }
}
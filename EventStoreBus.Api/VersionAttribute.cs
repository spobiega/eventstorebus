﻿using System;

namespace EventStoreBus.Api
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class VersionAttribute : Attribute
    {
        private readonly int versionNumber;

        public VersionAttribute(int versionNumber)
        {
            this.versionNumber = versionNumber;
        }

        public int VersionNumber
        {
            get { return versionNumber; }
        }
    }
}
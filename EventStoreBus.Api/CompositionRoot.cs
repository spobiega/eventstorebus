using System;

namespace EventStoreBus.Api
{
    public class CompositionRoot
    {
        public virtual void Starting()
        {
        }

        public virtual void Started()
        {
        }

        public virtual void Stopping()
        {
        }

        public virtual void Stopped()
        {
        }

        public virtual object CreateInstance(Type type, params object[] arguments)
        {
            return Activator.CreateInstance(type, arguments);
        }
    }
}
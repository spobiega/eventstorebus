﻿namespace EventStoreBus.Views.Api
{
    public interface IViewReaders
    {
        IViewReader<TView> GetReader<TView, TViewStorage>()
            where TView : class
            where TViewStorage : IViewStorage<TView>, new();   
    }
}
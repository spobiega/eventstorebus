﻿using System;

namespace EventStoreBus.Views.Api
{
    public interface IViewReader<out TView>
        where TView : class 
    {
        IViewReaderSession<TView> OpenSession();
    }

}
﻿using System.Linq;
using System.Management.Automation;
using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Get, "EventStore", SupportsShouldProcess = false)]
    public class GetEventStoreCommand : BaseCommand
    {
        protected override void ProcessRecordWithSupervisorClient(SupervisorClient client)
        {
            var references = client.GetEventStoreCollectionAsync().Result;
            var stores = references.Stores.Select(x => client.GetEventStoreInfoAsync(x.Name).Result);

            foreach (var eventStore in stores)
            {
                WriteObject(new EventStoreInfo(eventStore));
            }
        }
    }
}
﻿using System.Linq;
using System.Management.Automation;
using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Get, "ProcessingNode", SupportsShouldProcess = false)]
    public class GetProcessingNodeCommand : BaseCommand
    {
        protected override void ProcessRecordWithSupervisorClient(SupervisorClient client)
        {
            var references = client.GetNodeCollectionAsync().Result;
            var nodes = references.Nodes.Select(x => client.GetNodeInfoAsync(x.Id).Result);

            foreach (var node in nodes)
            {
                WriteObject(new NodeInfo(node));
            }
        }
    }
}
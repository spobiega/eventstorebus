﻿using System.Linq;
using System.Management.Automation;
using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Get, "ViewStore", SupportsShouldProcess = false)]
    public class GetViewStoreCommand : BaseCommand
    {
        protected override void ProcessRecordWithSupervisorClient(SupervisorClient client)
        {
            var references = client.GetViewStoreCollectionAsync().Result;
            var stores = references.Stores.Select(x => client.GetViewStoreInfoAsync(x.Name).Result);

            foreach (var viewStore in stores)
            {
                WriteObject(new ViewStoreInfo(viewStore));
            }
        }
    }
}
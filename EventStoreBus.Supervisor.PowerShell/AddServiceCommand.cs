﻿using System;
using System.Management.Automation;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Add, "Service", SupportsShouldProcess = false)]
    public class AddServiceCommand : CommandWithTimeout
    {
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string Service { get; set; }

        protected override void ProcessRecordWithSupervisorClient(Client.SupervisorClient client)
        {
            client.RegisterServiceAsync(Service).Wait();
            WriteVerbose(string.Format("Requested registration of service {0}.", Service));
            DoWaitForCompletion(() => client.GetServiceInfoAsync(Service).Result != null, string.Format("service {0}", Service));
        }
    }
}
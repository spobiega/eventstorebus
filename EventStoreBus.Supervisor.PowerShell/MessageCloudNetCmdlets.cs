﻿using System.ComponentModel;
using System.Management.Automation;

namespace EventStoreBus.Supervisor.PowerShell
{
    [RunInstaller(true)]
    public class MessageCloudNetCmdlets : PSSnapIn
    {
        public override string Description
        {
            get { return "MessageCloud.NET Cmdlets"; }
        }

        public override string Name
        {
            get { return "MessageCloudNetCmdlets"; }
        }

        public override string Vendor
        {
            get { return "MessageCloud.NET"; }
        }
    }
}
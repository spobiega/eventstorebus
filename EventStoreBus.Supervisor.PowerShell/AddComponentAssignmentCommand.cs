﻿using System;
using System.Linq;
using System.Management.Automation;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Add, "ComponentAssignment", SupportsShouldProcess = false)]
    public class AddComponentAssignmentCommand : CommandWithTimeout
    {
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string Node { get; set; }

        [Parameter(Mandatory = true, Position = 3, ValueFromPipelineByPropertyName = true)]
        public string Service { get; set; }
        
        [Parameter(Mandatory = true, Position = 4, ValueFromPipelineByPropertyName = true)]
        public string Component { get; set; }

        protected override void ProcessRecordWithSupervisorClient(Client.SupervisorClient client)
        {
            var assignmentId = client.AssignNodeAsync(Node, Service, Component).Result;
            WriteVerbose(string.Format("Component {0} of service {1} assigned to node {2}.", Component, Service, Node));
            DoWaitForCompletion(() => client.GetNodeInfoAsync(Node).Result.Assignments.Any(x => x.Id == assignmentId), 
                string.Format("assignmenr of component {0} of service {1} to node {2}", Component, Service, Node));
        }
    }
}
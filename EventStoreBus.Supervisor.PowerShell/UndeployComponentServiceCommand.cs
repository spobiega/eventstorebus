﻿using System.Management.Automation;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet("Undeploy", "Component", SupportsShouldProcess = false)]
    public class UndeployComponentServiceCommand : BaseCommand
    {
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string Node { get; set; }

        [Parameter(Mandatory = true, Position = 3, ValueFromPipelineByPropertyName = true)]
        public string Deployment { get; set; }
        
        protected override void ProcessRecordWithSupervisorClient(Client.SupervisorClient client)
        {
            var deploymentId = client.FindDeploymentIdByPrefix(Node, Deployment);
            client.UndeployAsync(Node, deploymentId).Wait();
            WriteVerbose(string.Format("Removed deployment {0} from node {1}.", Deployment, Node));
        }
    }
}
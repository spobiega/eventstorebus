﻿using System;
using System.Management.Automation;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Add, "ViewStore", SupportsShouldProcess = false)]
    public class AddViewStoreNodeCommand : CommandWithTimeout
    {
        [Alias("Name")]
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }

        [Parameter(Mandatory = true, Position = 3, ValueFromPipelineByPropertyName = true)]
        public string ConnectionString { get; set; }

        protected override void ProcessRecordWithSupervisorClient(Client.SupervisorClient client)
        {
            client.AddViewStoreAsync(Id, ConnectionString).Wait();
            WriteVerbose(string.Format("Requested attaching view store node {0} at {1}.", Id, ConnectionString));
            DoWaitForCompletion(() => client.GetEventStoreInfoAsync(Id).Result != null, string.Format("view store {0}", Id));
        }
    }
}
﻿using System;
using System.Management.Automation;
using System.Threading;

namespace EventStoreBus.Supervisor.PowerShell
{
    public abstract class CommandWithTimeout : BaseCommand
    {
        private static readonly TimeSpan defaultTimeout = TimeSpan.FromSeconds(10);

        [Parameter(Mandatory = false, ValueFromPipelineByPropertyName = false)]
        public SwitchParameter WaitForCompletion { get; set; }

        [Parameter(Mandatory = false, ValueFromPipelineByPropertyName = false)]
        public TimeSpan WaitForCompletionTimeout { get; set; }

        protected void DoWaitForCompletion(Func<bool> checkExistsCallback, string objectName)
        {
            if (!WaitForCompletion)
            {
                return;
            }
            var timeout = WaitForCompletionTimeout != TimeSpan.Zero
                              ? WaitForCompletionTimeout
                              : defaultTimeout;
            var start = DateTime.Now;

            while (DateTime.Now < start + timeout)
            {
                if (checkExistsCallback())
                {
                    WriteVerbose(string.Format("Successfuly registered {0}.", objectName));
                    return;
                }
                Thread.Sleep(1000);
            }
            WriteWarning(string.Format("Timed out waiting for registration of {0}.", objectName));
        }
    }
}
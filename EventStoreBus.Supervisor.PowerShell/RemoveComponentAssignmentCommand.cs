﻿using System.Management.Automation;

namespace EventStoreBus.Supervisor.PowerShell
{
    [Cmdlet(VerbsCommon.Remove, "ComponentAssignment", SupportsShouldProcess = false)]
    public class RemoveComponentAssignmentCommand : BaseCommand
    {
        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string Node { get; set; }

        [Parameter(Mandatory = true, Position = 3, ValueFromPipelineByPropertyName = true)]
        public string AssignmentId { get; set; }
        
        protected override void ProcessRecordWithSupervisorClient(Client.SupervisorClient client)
        {
            client.UnassignNodeAsync(Node, AssignmentId).Wait();
            WriteVerbose(string.Format("Assignment {0} removed from node {1}.", AssignmentId, Node));
        }
    }
}
﻿using System;
using System.Linq;
using EventStoreBus.Supervisor.Client;

namespace EventStoreBus.Supervisor.PowerShell
{
    public static class SupervisorClientExtensions
    {
        public static string FindDeploymentIdByPrefix(this SupervisorClient client, string nodeId, string prefix)
        {
            var node = client.GetNodeInfoAsync(nodeId).Result;
            var matchingIds = node.Deployments.Where(x => x.Id.StartsWith(prefix)).ToList();
            if (matchingIds.Count > 1)
            {
                throw new ArgumentException("Ambiguous deployment id prefix. Which id did you mean? {0}" + String.Join(" ", matchingIds));
            }
            if (matchingIds.Count == 0)
            {
                throw new ArgumentException("No deployment id starts with " + prefix);
            }
            return matchingIds.First().Id;
        }
    }
}
﻿using System;
using System.Web.Http;
using System.Web.Http.Controllers;
using EventStoreBus.Api;
using EventStoreBus.Views.Api;

namespace EventStoreBus.Web.Api
{
    public interface IWebFactory
    {
        IHttpController Create(Type controllerType, string endpointUrl, IViewReaders viewStoreReadersReaders, ICommandSender commandSender);
        void Release(IHttpController instance);
        void Configure(HttpConfiguration config);
    }
}
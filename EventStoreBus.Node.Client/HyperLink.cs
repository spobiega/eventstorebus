﻿using System.Runtime.Serialization;

namespace EventStoreBus.Node.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/node")]
    public class HyperLink
    {
        [DataMember]
        public string Relation { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Verb { get; set; }

    }
}
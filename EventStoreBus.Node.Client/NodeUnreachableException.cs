﻿using System;
using System.Runtime.Serialization;

namespace EventStoreBus.Node.Client
{
    [Serializable]
    public class NodeUnreachableException : Exception
    {
        public NodeUnreachableException(string nodeUrl) 
            : base("Node is unreachable: "+nodeUrl)
        {
        }

        protected NodeUnreachableException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
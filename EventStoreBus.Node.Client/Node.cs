﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EventStoreBus.Node.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/node")]
    public class Node
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public HyperLink Manage { get; set; }
        [DataMember]
        public HyperLink Deploy { get; set; }
        [DataMember]
        public HyperLink UpdateEnvironment { get; set; }
        [DataMember]
        public List<Deployment> Deployments { get; set; }
    }
}
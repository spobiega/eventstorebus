﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Environment = EventStoreBus.Config.Environment;

namespace EventStoreBus.Node.Client
{
    public class NodeController
    {
        private readonly string nodeUrl;
        private readonly HttpClient client;

        public NodeController(string nodeUrl)
        {
            this.nodeUrl = nodeUrl;
            client = new HttpClient
                         {
                             BaseAddress = new Uri(nodeUrl)
                         };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
        }

        public Task UpdateEnvironmentAsync(Environment environment)
        {
            var serializer = new XmlSerializer(typeof (Environment));
            var memoryStream = new MemoryStream();
            serializer.Serialize(memoryStream, environment);
            memoryStream.Flush();
            memoryStream.Seek(0, SeekOrigin.Begin);

            return GetNodeInfoAsync()
                .ContinueWith(x => client.PutAsync(x.Result.UpdateEnvironment.Url, new StreamContent(memoryStream)), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x => memoryStream.Dispose(), TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public Task<Tuple<string, string>> ClaimDeploymentSlotAsync()
        {
            string requestUri = null;
            return GetNodeInfoAsync()
                .ContinueWith(x =>
                                  {
                                      requestUri = x.Result.Deploy.Url;
                                      return client.GetAsync(requestUri);
                                  }, TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x => x.Result.Content.ReadAsAsync<Deployment>(), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x => Tuple.Create(requestUri, x.Result.Id));
        }

        public Task<Node> GetNodeInfoAsync()
        {
            return client.GetAsync("")
                .ContinueWith(x => EnsureNodeReachable(x))
                .ContinueWith(x => x.Result.Content.ReadAsAsync<Node>())
                .Unwrap();
        }

        private HttpResponseMessage EnsureNodeReachable(Task<HttpResponseMessage> x)
        {
            if (x.IsFaulted || x.Result.StatusCode != HttpStatusCode.OK)
            {
                throw new NodeUnreachableException(nodeUrl);
            }
            return x.Result;
        }

        public Task<string> DeployAsync(string service, string version, string componentName)
        {
            return GetNodeInfoAsync()
                .ContinueWith(x => DeployAsync(x.Result.Deploy.Url, service, version, componentName), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x => x.Result.Id, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public Task StartAsync(string deploymentId)
        {
            return GetNodeInfoAsync()
                .ContinueWith(x => client.PutAsync(x.Result.Deployments.First(d => d.Id == deploymentId).Start.Url, new StringContent("")), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }

        public Task StopAsync(string deploymentId)
        {
            return GetNodeInfoAsync()
                .ContinueWith(x => client.DeleteAsync(x.Result.Deployments.First(d => d.Id == deploymentId).Stop.Url), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }

        public Task UndeployAsync(string deploymentId)
        {
            return GetNodeInfoAsync()
                .ContinueWith(x => client.DeleteAsync(x.Result.Deployments.First(d => d.Id == deploymentId).Undeploy.Url), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }

        public Task<Deployment> DeployAsync(string deployUrl, string service, string version, string component)
        {
            var newDeployment = new NewDeployment
                                    {
                                        Component = component,
                                        Service = service,
                                        Version = version
                                    };

            return client
                .PutAsXmlAsync(deployUrl, newDeployment)
                .ContinueWith(x => x.Result.Content.ReadAsAsync<Deployment>(), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }
    }
}
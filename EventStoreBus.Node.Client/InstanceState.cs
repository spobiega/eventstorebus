﻿namespace EventStoreBus.Node.Client
{
    public enum InstanceState
    {
        Stopped,
        Running
    }
}
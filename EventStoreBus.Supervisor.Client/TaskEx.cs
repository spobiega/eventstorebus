﻿using System.Threading.Tasks;

namespace EventStoreBus.Supervisor.Client
{
    public static class TaskEx
    {
        public static Task<T> FromResult<T>(T result)
        {
            var taskSource = new TaskCompletionSource<T>();
            taskSource.SetResult(result);
            return taskSource.Task;
        }
    }
}
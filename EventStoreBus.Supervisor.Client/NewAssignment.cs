using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NewAssignment
    {
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Component { get; set; }
    }
}
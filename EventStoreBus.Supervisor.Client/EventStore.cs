﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class EventStore
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public int TcpPort { get; set; }
        [DataMember]
        public int HttpPort { get; set; }
        [DataMember]
        public List<EventStoreDatabase> Databases { get; set; }
        [DataMember]
        public HyperLink Remove { get; set; }
        [DataMember]
        public HyperLink Manage { get; set; }
        [DataMember]
        public HyperLink CreateDatabase { get; set; }
    }
}
﻿using System;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [Serializable]
    public class ServiceUnreachableException : Exception
    {
        public ServiceUnreachableException(string serviceUrl) 
            : base("Service is unreachable: "+serviceUrl)
        {
        }

        protected ServiceUnreachableException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
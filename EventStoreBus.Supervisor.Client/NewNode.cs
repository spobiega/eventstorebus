﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NewNode
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Url { get; set; }

        public NewNode(string id, string url)
        {
            Id = id;
            Url = url;
        }
    }
}
﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class ViewStore
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ConnectionString { get; set; }
        [DataMember]
        public HyperLink Remove { get; set; }
    }
}
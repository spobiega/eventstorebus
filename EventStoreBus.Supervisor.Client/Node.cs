﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Node
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public HyperLink Manage { get; set; }
        [DataMember]
        public HyperLink Deploy { get; set; }
        [DataMember]
        public HyperLink Assign { get; set; }   
        [DataMember]
        public List<Deployment> Deployments { get; set; }
        [DataMember]
        public List<Assignment> Assignments { get; set; }
    }
}
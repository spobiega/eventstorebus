﻿using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class CurrentVersion
    {
        [DataMember]
        public string Version { get; set; }

        public CurrentVersion(string version)
        {
            Version = version;
        }
    }
}
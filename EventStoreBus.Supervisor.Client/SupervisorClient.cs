﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace EventStoreBus.Supervisor.Client
{
    public class SupervisorClient
    {
        private readonly string serviceUrl;
        private readonly HttpClient client;

        public SupervisorClient(string serviceUrl)
        {
            this.serviceUrl = serviceUrl;
            client = new HttpClient
                         {
                             BaseAddress = new Uri(serviceUrl)
                         };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
        }
        
        public Task<Node> GetNodeInfoAsync(string nodeId)
        {
            return GetNodeCollectionAsync()
                .ContinueWith(x => GetNodeByReferenceAsync(x.Result, nodeId), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }
        
        public Task<Instance> GetInstanceInfoAsync(string nodeId, string deploymentId)
        {
            return GetNodeInfoAsync(nodeId)
                .ContinueWith(x => DoGetInstanceInfoAsync(x.Result, deploymentId))
                .Unwrap();
        }

        public Task<Instance> DoGetInstanceInfoAsync(Node node, string deploymentId)
        {
            var deployment = node.Deployments.FirstOrDefault(x => x.Id == deploymentId);
            return client.GetAsync(deployment.Status.Url)
                .ContinueWith(x => EnsureReachable(x))
                .ContinueWith(x => x.Result.Content.ReadAsAsync<Instance>())
                .Unwrap();
        }

        public Task<EventStore> GetEventStoreInfoAsync(string storeName)
        {
            return GetEventStoreCollectionAsync()
                .ContinueWith(x => GetEventStoreByReferenceAsync(x.Result, storeName), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }

        public Task<HttpStatusCode> CreateEventStoreDatabaseAsync(string storeName, string databaseName)
        {
            return GetEventStoreInfoAsync(storeName)
                .ContinueWith(x => client.PutAsXmlAsync(x.Result.CreateDatabase.Url, new NewDatabase
                    {
                        DatabaseName = databaseName
                    }))
                .Unwrap()
                .ContinueWith(x => x.Result.StatusCode);
        }
        
        public Task<ViewStore> GetViewStoreInfoAsync(string storeName)
        {
            return GetViewStoreCollectionAsync()
                .ContinueWith(x => GetViewStoreByReferenceAsync(x.Result, storeName), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }
        
        public Task<Service> GetServiceInfoAsync(string serviceName)
        {
            return GetServiceRepositoryAsync()
                .ContinueWith(x => GetServiceByReferenceAsync(x.Result, serviceName), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }

        public Task<ServiceRepository> GetServiceRepositoryAsync()
        {
            return GetEnvironmentInfoAsync()
                .ContinueWith(x => client.GetAsync(x.Result.Services.Url))
                .Unwrap()
                .ContinueWith(x => EnsureReachable(x))
                .ContinueWith(x => x.Result.Content.ReadAsAsync<ServiceRepository>())
                .Unwrap();
        }

        public Task<HttpStatusCode> RedeployAsync(string service)
        {
            return GetServiceInfoAsync(service)
                .ContinueWith(x => client.PutAsync(x.Result.Redeploy.Url, new StringContent("")))
                .Unwrap()
                .ContinueWith(x => x.Result.StatusCode);
        }

        private Task<Service> GetServiceByReferenceAsync(ServiceRepository repository, string serviceName)
        {
            var serviceReference = repository.Services.FirstOrDefault(s => s.Name == serviceName);
            if (serviceReference == null)
            {
                return TaskEx.FromResult<Service>(null);
            }
            return GetElementByReferenceAsync<Service>(serviceReference.Details.Url);
        }

        private Task<T> GetElementByReferenceAsync<T>(string url)
            where T : class 
        {
            return client.GetAsync(url)
                .ContinueWith(x => TryDeserializeContent<T>(x))
                .Unwrap();
        }

        private static Task<T> TryDeserializeContent<T>(Task<HttpResponseMessage> x)
            where T : class 
        {
            if (x.Result.StatusCode == HttpStatusCode.NotFound)
            {
                return TaskEx.FromResult<T>(null);
            }
            return x.Result.Content.ReadAsAsync<T>();
        }

        public Task<HttpStatusCode> RegisterServiceAsync(string service)
        {
            return GetServiceRepositoryAsync()
                .ContinueWith(x => DoRegisterServiceAsync(x.Result, service))
                .Unwrap()
                .ContinueWith(x => x.Result.StatusCode);
        }

        private Task<HttpResponseMessage> DoRegisterServiceAsync(ServiceRepository repository, string serviceName)
        {
            return client.PutAsXmlAsync(repository.Register.Url, new NewService
                                                                     {
                                                                         Name = serviceName
                                                                     });
        }
        
        public Task<HttpStatusCode> SetVersionAsync(string service, string version)
        {
            return GetServiceInfoAsync(service)
                .ContinueWith(x => client.PutAsXmlAsync(x.Result.SetVersion.Url, new CurrentVersion(version)))
                .Unwrap()
                .ContinueWith(x => x.Result.StatusCode);
        }

        public Task<Environment> GetEnvironmentInfoAsync()
        {
            return client.GetAsync("")
                .ContinueWith(x => EnsureReachable(x))
                .ContinueWith(x => x.Result.Content.ReadAsAsync<Environment>())
                .Unwrap();
        }

        public Task<NodeCollection> GetNodeCollectionAsync()
        {
            return GetEnvironmentInfoAsync()
                .ContinueWith(x => client.GetAsync(x.Result.Nodes.Url))
                .Unwrap()
                .ContinueWith(x => x.Result.Content.ReadAsAsync<NodeCollection>())
                .Unwrap();
        }

        private Task<Node> GetNodeByReferenceAsync(NodeCollection nodeCollection, string nodeId)
        {
            var nodeReference = nodeCollection.Nodes.FirstOrDefault(s => s.Id == nodeId);
            if (nodeReference == null)
            {
                return TaskEx.FromResult<Node>(null);
            }
            return GetElementByReferenceAsync<Node>(nodeReference.Details.Url);
        }
        
        public Task<EventStoreCollection> GetEventStoreCollectionAsync()
        {
            return GetEnvironmentInfoAsync()
                .ContinueWith(x => client.GetAsync(x.Result.EventStores.Url))
                .Unwrap()
                .ContinueWith(x => x.Result.Content.ReadAsAsync<EventStoreCollection>())
                .Unwrap();
        }

        private Task<EventStore> GetEventStoreByReferenceAsync(EventStoreCollection collection, string storeName)
        {
            var storeReference = collection.Stores.FirstOrDefault(s => s.Name == storeName);
            if (storeReference == null)
            {
                return TaskEx.FromResult<EventStore>(null);
            }
            return GetElementByReferenceAsync<EventStore>(storeReference.Details.Url);
        }
        
        public Task<ViewStoreCollection> GetViewStoreCollectionAsync()
        {
            return GetEnvironmentInfoAsync()
                .ContinueWith(x => client.GetAsync(x.Result.ViewStores.Url))
                .Unwrap()
                .ContinueWith(x => x.Result.Content.ReadAsAsync<ViewStoreCollection>())
                .Unwrap();
        }

        private Task<ViewStore> GetViewStoreByReferenceAsync(ViewStoreCollection collection, string storeName)
        {
            var storeReference = collection.Stores.FirstOrDefault(s => s.Name == storeName);
            if (storeReference == null)
            {
                return TaskEx.FromResult<ViewStore>(null);
            }
            return GetElementByReferenceAsync<ViewStore>(storeReference.Details.Url);
        }

        public Task<HttpStatusCode> AddNodeAsync(string nodeId, string nodeUrl)
        {
            return GetNodeCollectionAsync()
                .ContinueWith(x => DoAddNodeAsync(x.Result.Register.Url, nodeId, nodeUrl), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x => x.Result.StatusCode);
        }

        private Task<HttpResponseMessage> DoAddNodeAsync(string addUrl, string nodeId, string nodeUrl)
        {
            var newNode = new NewNode(nodeId, nodeUrl);
            return client.PutAsXmlAsync(addUrl, newNode);
        }
        
        public Task<string> AssignNodeAsync(string nodeId, string service, string component)
        {
            return GetNodeInfoAsync(nodeId)
                .ContinueWith(x => DoAssignNodeAsync(x.Result.Assign.Url, service, component), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }

        private Task<string > DoAssignNodeAsync(string assignUrl, string service, string component)
        {
            var newAssignment = new NewAssignment
                                    {
                                        Component = component,
                                        Service = service
                                    };
            return client.PutAsXmlAsync(assignUrl, newAssignment)
                .ContinueWith(x => x.Result.Content.ReadAsStringAsync())
                .Unwrap();
        }

        public Task<HttpStatusCode> UnassignNodeAsync(string nodeId, string assignmentId)
        {
            return GetNodeInfoAsync(nodeId)
                .ContinueWith(x => DoUnassignNodeAsync(x.Result, assignmentId), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }

        private Task<HttpStatusCode> DoUnassignNodeAsync(Node node, string assignmentId)
        {
            var assignment = node.Assignments.FirstOrDefault(x => x.Id == assignmentId);
            if (assignment == null)
            {
                return TaskEx.FromResult(HttpStatusCode.NotFound);
            }
            return client.DeleteAsync(assignment.Unassign.Url)
                .ContinueWith(x => x.Result.StatusCode);
        }
        
        public Task<HttpStatusCode> AddEventStoreAsync(string storeId, string address, int port)
        {
            return GetEventStoreCollectionAsync()
                .ContinueWith(x => DoAddEventStoreAsync(x.Result.Register.Url, storeId, address, port), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x => x.Result.StatusCode);
        }

        private Task<HttpResponseMessage> DoAddEventStoreAsync(string addUrl, string storeId, string address, int port)
        {
            var newStore = new NewEventStore(storeId, address, port);
            return client.PutAsXmlAsync(addUrl, newStore);
        }
        
        public Task<HttpStatusCode> AddViewStoreAsync(string storeId, string connectionString)
        {
            return GetViewStoreCollectionAsync()
                .ContinueWith(x => DoAddViewStoreAsync(x.Result.Register.Url, storeId, connectionString), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x => x.Result.StatusCode);
        }

        private Task<HttpResponseMessage> DoAddViewStoreAsync(string addUrl, string storeId, string connectionString)
        {
            var newStore = new NewViewStore()
                               {
                                   Name = storeId,
                                   ConnectionString = connectionString
                               };
            return client.PutAsXmlAsync(addUrl, newStore);
        }

       
        private HttpResponseMessage EnsureReachable(Task<HttpResponseMessage> x)
        {
            if (x.IsFaulted || x.Result.StatusCode != HttpStatusCode.OK)
            {
                throw new ServiceUnreachableException(serviceUrl);
            }
            return x.Result;
        }

        public Task<string> DeployAsync(string nodeId, string service, string version, string componentName)
        {
            return GetNodeInfoAsync(nodeId)
                .ContinueWith(x => DoDeployAsync(x.Result.Deploy.Url, service, version, componentName), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x => x.Result.Id, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public Task<HttpStatusCode> StartAsync(string nodeId, string deploymentId)
        {
            return GetNodeInfoAsync(nodeId)
                .ContinueWith(x => client.PutAsync(x.Result.Deployments.First(d => d.Id == deploymentId).Start.Url, new StringContent("")), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x => x.Result.StatusCode);
        }

        public Task<HttpStatusCode> StopAsync(string nodeId, string deploymentId)
        {
            return GetNodeInfoAsync(nodeId)
                .ContinueWith(x => client.DeleteAsync(x.Result.Deployments.First(d => d.Id == deploymentId).Stop.Url), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x => x.Result.StatusCode);
        }

        public Task<HttpStatusCode> UndeployAsync(string nodeId, string deploymentId)
        {
            return GetNodeInfoAsync(nodeId)
                .ContinueWith(x => client.DeleteAsync(x.Result.Deployments.First(d => d.Id == deploymentId).Undeploy.Url), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap()
                .ContinueWith(x=> x.Result.StatusCode);
        }

        public Task<Deployment> DoDeployAsync(string deployUrl, string service, string version, string component)
        {
            var newDeployment = new NewDeployment
                                    {
                                        Component = component,
                                        Service = service,
                                        Version = version
                                    };

            return client
                .PutAsXmlAsync(deployUrl, newDeployment)
                .ContinueWith(x => WaitForDeploymentToComplete(x.Result.Headers.Location), TaskContinuationOptions.OnlyOnRanToCompletion)
                .Unwrap();
        }

        private Task<Deployment> WaitForDeploymentToComplete(Uri location)
        {
            while (true)
            {
                var response = client.GetAsync(location).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return response.Content.ReadAsAsync<Deployment>();
                }
                if (response.StatusCode != HttpStatusCode.NotFound)
                {
                    throw new InvalidOperationException("Something wrong");
                }
                Thread.Sleep(1000);
            }
        }
    }
}
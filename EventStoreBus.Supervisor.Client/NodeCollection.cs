﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class NodeCollection
    {
        [DataMember]
        public List<NodeReference> Nodes { get; set; }

        [DataMember]
        public HyperLink Register { get; set; }
        
        [DataMember]
        public HyperLink Parent { get; set; }
    }
}
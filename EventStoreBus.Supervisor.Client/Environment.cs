﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EventStoreBus.Supervisor.Client
{
    [DataContract(Namespace = "http://api.eventstorebus.com/supervisor")]
    public class Environment
    {
        [DataMember]
        public HyperLink EventStores { get; set; }

        [DataMember]
        public HyperLink ViewStores { get; set; }

        [DataMember]
        public HyperLink Nodes { get; set; }

        [DataMember]
        public HyperLink Services { get; set; }
    }
}